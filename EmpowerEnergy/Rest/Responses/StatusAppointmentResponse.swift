//
//  StatusAppointmentResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 01/07/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class StatusAppointmentResponse: Decodable{
    var id: Int?
    var appointment_at: String?
    var appointment_end_at: String?
    var snooze_time: Int?
    var held: Bool?
    var description: String?
    var title: String?
    var created_at: String?
    var updated_at: String?
    var user: UsersResponse?
    var address: InteractionAddressData?
}
