//
//  AddressHistoryResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 28/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class AddressHistoryResponse: Decodable
{
    var appointments: [AddressAppointmentHistory]?
    var notes: [NotesResponse]?
    var interactions: [AddressAppointmentHistoryResponse]?
}
