//
//  File.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 28/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class AddressAppointmentHistory:Decodable
{
    var appointment_at:String?
    var appointment_end_at:String?
    var snooze_time: Int?
    var held:Int?
    var id: Int?
    var description: String?
    var title: String?
    var created_at: String?
    var updated_at: String?
    var user: UsersResponse
    var address: InteractionAddressData?
}
