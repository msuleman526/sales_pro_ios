//
//  File.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 02/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class MyAreaResponse: Decodable
{
    var id: Int?
    var name: String?
    var geography: String?
    var office_id: Int?
    var color: String?
    var created_at: String?
    var updated_at: String?
    var pivat: Pivat?
}

class Pivat: Decodable
{
    var user_id: Int?
    var area_id: Int?
}
