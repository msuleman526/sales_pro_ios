//
//  AddressAppointmentHistoryResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 28/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class AddressAppointmentHistoryResponse: Decodable{
    
    var created_at: String?
    var updated_at: String?
    var id: Int?
    var user: UsersResponse?
    var interaction: Interaction?
}
