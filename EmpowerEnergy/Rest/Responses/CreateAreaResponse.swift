//
//  CreateAreaResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 29/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class CreateAreaResponse: Decodable
{
    var id: Int?
    var name: String?
    var geography: String?
    var office_id: String?
    var color: String?
    var created_at: String?
    var updated_at: String?
    var pivat: Pivat?
}


