//
//  UsersResponse.swift
//  energysolutions
//
//  Created by Salman Sherin on 18/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class UsersResponse:Decodable {
    
    var message:String?;
    var id:Int?;
    var name:String?;
    var first_name:String?
    var last_name:String?
    var email:String?;
    var api_token:String?;
    var role:Role?;
    var office_id:Int?;
    var created_at:String?;
    var updated_at:String?;
    var password: String?
    var interactions:[Interaction]?;
    var parent_id:Int?;
    var phone:String?;
    var companies:[Office]?;
    
    init() {
        
    }
    
    init(id: Int,name: String,first_name: String,last_name: String,email: String,office_id: Int,role: Role,parent: Int,phone: String,password: String)
    {
        self.id = id
        self.name = name
        self.first_name = first_name
        self.last_name = last_name
        self.office_id = office_id
        self.email = email
        self.role = role
        self.parent_id = parent
        self.phone = phone
        self.password = password
    }
    
    init(id: Int,name: String,first_name: String,last_name: String,email: String,office_id: Int,role: Role,parent: Int,phone: String,password: String,created: String, updated: String,companies: [Office])
    {
        self.id = id
        self.name = name
        self.first_name = first_name
        self.last_name = last_name
        self.office_id = office_id
        self.email = email
        self.role = role
        self.parent_id = parent
        self.phone = phone
        self.password = password
        self.created_at = created
        self.updated_at = updated
        self.companies = companies
    }
    
    init(id: Int,name: String,first_name: String,last_name: String,email: String,office_id: Int,role: Role,parent: Int,phone: String,password: String,companies: [Office])
       {
           self.id = id
           self.name = name
           self.first_name = first_name
           self.last_name = last_name
           self.office_id = office_id
           self.email = email
           self.role = role
           self.parent_id = parent
           self.phone = phone
           self.password = password
           self.companies = companies
       }
    
    init(name: String,first_name: String,last_name: String,email: String,office_id: Int,role: Role,parent: Int,phone: String,password: String)
    {
        self.name = name
        self.first_name = first_name
        self.last_name = last_name
        self.office_id = office_id
        self.email = email
        self.role = role
        self.parent_id = parent
        self.phone = phone
        self.password = password
    }
    
}
