//
//  OfficesDataResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 24/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation


class OfficesDataResponse: Decodable
{
    var id: Int?
    var name: String?
    var created_at: String?
    var updated_at: String?
    var users: [UsersResponse]?
}
