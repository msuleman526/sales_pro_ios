//
//  RequestResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 09/07/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class RequestResponse: Decodable
{
    var status: String?
}
