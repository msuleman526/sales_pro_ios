//
//  UserResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 26/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class UserResponse: Decodable
{
    var message:String?;
    var id:Int?;
    var name:String?;
    var first_name:String?
    var last_name:String?
    var email:String?;
    var api_token:String?;
    var role:Role?;
    var office_id:Int?;
    var created_at:String?;
    var updated_at:String?;
    var password: String?
    var interactions:[Interaction]?;
    var parent_id:String?;
    var phone:String?;
    var companies:[Office]?;
    
    init() {
        
    }
    
    init(name: String,first_name: String,last_name: String,email: String,office_id: Int,role: Role,parent: String,phone: String,password: String)
    {
        self.name = name
        self.first_name = first_name
        self.last_name = last_name
        self.office_id = office_id
        self.email = email
        self.role = role
        self.parent_id = parent
        self.phone = phone
        self.password = password
    }
}
