//
//  InteractionResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 02/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class InteractionResponse:Decodable {
    
    var updated_at:String?;
    var created_at:String?;
    var user:UsersResponse?;
    var interaction: Interaction
}
