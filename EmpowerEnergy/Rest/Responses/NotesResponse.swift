//
//  NotesResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 28/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class NotesResponse: Decodable{
    
    var id: Int?
    var note: String?
    var address_id: Int?
    var created_at: String?
    var updated_at: String?
    var user: UsersResponse?
    
}
