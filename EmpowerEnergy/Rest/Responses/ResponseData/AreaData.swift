//
//  AreaData.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 04/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class AreaData: Decodable
{
    var id: Int?
    var name: String?
    var geography: String?
    var office_id: Int?
    var color: String?
    var created_by: Int?
    var created_at: String?
    var updated_at: String?
    var total_addresses: Int?
    var users: [UsersResponse]?
}
