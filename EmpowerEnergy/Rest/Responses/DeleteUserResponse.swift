//
//  DeleteUserResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 26/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class DeleteUserResponse: Decodable
{
    var status: String?
    var message: String?
}
