//
//  AllAppointmentResponse.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 21/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class AllAppointmentResponse: Decodable
{
    var id: Int?
    var appointment_at: String?
    var appointment_end_at: String?
    var snooze_time: Int?
    //var snooze_time: String?
    var held: Int?
    var description: String?
    var title: String?
    var created_at: String?
    var updated_at: String?
    var user: UsersResponse?
    var customer: InteractionAddressData?
}
