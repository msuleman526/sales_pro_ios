//
//  InteractionsUserResponse.swift
//  energysolutions
//
//  Created by Salman Sherin on 18/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation


class InteractionsUserResponse:Decodable
{
    var interactions:[Interaction]?;
    var users:[UsersResponse]?;
    var roles:[Role]?;
    var current_user: UsersResponse?;
    
}
