//
//  Utils.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 18/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation
import UIKit

class Utils
{
    //public static var requestURL = "http://crm.empowerenergy.co/";
    public static var requestURL = "https://sparkemp.io/"
    
    static func resizeImage(image: UIImage,sizeImage: CGSize)-> UIImage
    {
        let hasAlpha = true
        let scale: CGFloat = 0.0
        UIGraphicsBeginImageContextWithOptions(sizeImage, !hasAlpha, scale)
        image.draw(in: CGRect(origin: .zero, size: sizeImage))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return scaledImage!
    }
    
    static func resizeImageForMarker(image: UIImage,sizeImage: CGSize) -> UIImage {
        let size = image.size
        let container = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 2.0)
        UIGraphicsGetCurrentContext()!.interpolationQuality = .high
        image.draw(in: container)

        let topWidth = size.width / 1.0
        let topHeight = size.height / 1.0
        let topX = (size.width / 2.0) - (topWidth / 2.0)
        let topY = (size.height / 2.0) - (topHeight / 2.0)

        image.draw(in: CGRect(x: topX, y: topY, width: topWidth, height: topHeight), blendMode: .normal, alpha: 1.0)
        

        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    static func customImage(diameter: CGFloat,color: UIColor) -> UIImage
    {
        
        //let textColor = UIColor.white
        //let textFont = UIFont(name: "Helvetica Bold", size: 11)
        
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: diameter, height: diameter), false, 0)
        let ctx = UIGraphicsGetCurrentContext()!
        ctx.saveGState()
        
        let rect = CGRect(x: 0, y: 0, width: diameter, height: diameter)
        ctx.setFillColor(color.cgColor)
        ctx.fillEllipse(in: rect)
        
        ctx.restoreGState()
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsGetCurrentContext()
        
        return img;
    }
    
    //   static func textToImage(drawText text: NSString, inImage image: UIImage) -> UIImage {
    //        //draw image first
    //        UIGraphicsBeginImageContext(image.size)
    //        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
    //
    //        //text attributes
    //        let font=UIFont(name: "Helvetica-Bold", size: 14)!
    //        let text_style=NSMutableParagraphStyle()
    //        text_style.alignment=NSTextAlignment.center
    //        let text_color=UIColor.white
    //        let attributes=[NSAttributedString.Key.font:font, NSAttributedString.Key.paragraphStyle:text_style, NSAttributedString.Key.foregroundColor:text_color]
    //
    //        //vertically center (depending on font)
    //        let text_h=font.lineHeight
    //        let text_y=(image.size.height-text_h)/2
    //        let text_rect=CGRect(x: 0, y: text_y, width: image.size.width, height: text_h)
    //        text.draw(in: text_rect.integral, withAttributes: attributes)
    //        let result=UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //
    //        return result!
    //    }
    
    static func textToImage(drawText: NSString, inImage: UIImage) -> UIImage{
        
        // Setup the font specific variables
        let font=UIFont(name: "Helvetica-Bold", size: 13)!
        let text_style=NSMutableParagraphStyle()
        text_style.alignment=NSTextAlignment.center
        let text_color=UIColor.white
        let attributes=[NSAttributedString.Key.font:font, NSAttributedString.Key.paragraphStyle:text_style, NSAttributedString.Key.foregroundColor:text_color]
        
        // Setup the image context using the passed image
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(inImage.size, false, scale)
        // Put the image into a rectangle as large as the original image
        inImage.draw(in: CGRect(x: 0, y: 0, width: inImage.size.width, height: inImage.size.height))
        
        // Create a point within the space that is as bit as the image
        let text_h=font.lineHeight
        let text_y=(inImage.size.height-text_h)/2
        let rect = CGRect(x: 0, y: text_y, width: inImage.size.width, height: text_h)
        
        // Draw the text into an image
        drawText.draw(in: rect.integral, withAttributes: attributes)
        
        // Create a new image out of the images we have created
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        
        //Pass the image back up to the caller
        return newImage
        
    }
    
    static func textToImageForMarker(drawText: NSString, inImage: UIImage) -> UIImage{
        
        // Setup the font specific variables
        let font=UIFont(name: "Helvetica-Bold", size: 13)!
        let text_style=NSMutableParagraphStyle()
        text_style.alignment=NSTextAlignment.center
        let text_color=UIColor.white
        let attributes=[NSAttributedString.Key.font:font, NSAttributedString.Key.paragraphStyle:text_style, NSAttributedString.Key.foregroundColor:text_color]
        
        // Setup the image context using the passed image
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 23, height: 50), false, 0.0)
        // Put the image into a rectangle as large as the original image
        inImage.draw(at: CGPoint(x: (23 - inImage.size.width) / 2.0, y: (50 - inImage.size.height) / 2.0))
        //inImage.draw(in: CGRect(x: 0, y: 0, width: inImage.size.width, height: inImage.size.height + 10))
        // Create a point within the space that is as bit as the image
        let text_h=font.lineHeight
        let text_y=(inImage.size.height + 50)/2
        let rect = CGRect(x: 0, y: text_y, width: inImage.size.width, height: text_h)
        
        // Draw the text into an image
        drawText.draw(in: rect.integral, withAttributes: attributes)
        
        // Create a new image out of the images we have created
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        
        //Pass the image back up to the caller
        return newImage
        
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func drawText(text:NSString, inImage:UIImage) -> UIImage? {

        let size = inImage.size

        let font=UIFont(name: "Helvetica-Bold", size: 12)!
        let text_style=NSMutableParagraphStyle()
        text_style.alignment=NSTextAlignment.center
        let text_color=UIColor.black
        let attributes=[NSAttributedString.Key.font:font, NSAttributedString.Key.paragraphStyle:text_style, NSAttributedString.Key.foregroundColor:text_color]
               
               // Setup the image context using the passed image
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(inImage.size, false, scale)

        inImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))


        let textSize = text.size(withAttributes: attributes as [NSAttributedString.Key : Any])
        let rect = CGRect(x: 0, y: 0, width: inImage.size.width, height: inImage.size.height)
        
        let textRect = CGRect(x: (rect.size.width - textSize.width)/2, y: (rect.size.height - textSize.height)/2 - 2, width: textSize.width, height: textSize.height)
        
        text.draw(in: textRect.integral, withAttributes: attributes as [NSAttributedString.Key : Any])
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        resultImage?.withTintColor(UIColor.white, renderingMode: .alwaysTemplate)

        UIGraphicsEndImageContext()

        return resultImage
    }
    
    static func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}
