//
//  ViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 18/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class ViewController: UIViewController {
    
    var progressDialog: SVProgressHUD?
    static var interactionResponse: InteractionsUserResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        //if(SessionUtils.getName()=="" || SessionUtils.getEmail()=="" || SessionUtils.getToken()=="")
        //{
        getInteractionsWithUsers();
        //}
        
    }
    
    func getInteractionsWithUsers()
    {
        do{
            let API_TOKEN = SessionUtils.getToken();
            let header = [
                "Authorization": "bearer \(API_TOKEN)"
            ]
            let resourceString = "\(Utils.requestURL)api/interactions";
            Alamofire.request(resourceString, method: .get, parameters: nil, headers: header).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    if let httpURLResponse = response.response{
                        let response_code = httpURLResponse.statusCode;
                        if Constants.LOGGING_ENABLED{
                        print(response_code)
                        }
                        if response_code == 200 {
                            do{
                                if Constants.LOGGING_ENABLED{
                                print(value)
                                }
                                let json = JSON(value)
                                let str = String(describing: json);
                                let jsonData = str.data(using: .utf8)
                                let decoder = JSONDecoder();
                                let res = try decoder.decode(InteractionsUserResponse.self, from: jsonData!)
                                
                                ViewController.interactionResponse = res;
                                let loginViewController = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "Home") as? HomeViewController
                                self.navigationController?.pushViewController(loginViewController!, animated: false);
                                //self.present(loginViewController!,animated: true,completion: nil)
                                
                                //self.navigationController?.popToRootViewController(animated: false)
                                
                            }catch{
                                if Constants.LOGGING_ENABLED{
                                    print(error)}}
                            
                            
                        }else if response_code == 401{
                            Toast.show(message: "Unauthorized User", controller: self)
                            let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            self.navigationController?.pushViewController(viewController!, animated: false);
                            //self.present(viewController!,animated: true,completion: nil)
                            
                        }else{
                            if Constants.LOGGING_ENABLED{
                            print("Error")
                            }
                        }
                    }
                case .failure(let error):
                    if Constants.LOGGING_ENABLED{
                    print("Interaction Response Failure is: \(error)")
                    }
                    let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                    self.navigationController?.pushViewController(viewController!, animated: false);
                    // self.present(viewController!,animated: true,completion: nil)
                }
                
            })
            
            
            
        }
        
        
        
        
    }
    
}
