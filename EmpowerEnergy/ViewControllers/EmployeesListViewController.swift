//
//  ManageEmployeeViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 23/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EmployeesListViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var employeesTableView: UITableView!
    var employees: [Employee] = []
    var tempEmployeesList: [Employee] = []
    var alert: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Users"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.backItem?.title = "Back"
        
        //Add Employees
        let formator = DateFormatter()
        formator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        for i in (0..<ViewController.interactionResponse!.users!.count)
        {
            let id = ViewController.interactionResponse!.users![i].id!
            let firstName = ViewController.interactionResponse!.users![i].first_name ?? ""
            let lastName = ViewController.interactionResponse!.users![i].last_name ?? ""
            let email = ViewController.interactionResponse!.users![i].email!
            let created = formator.date(from: ViewController.interactionResponse!.users![i].created_at!)!
            let updated = formator.date(from: ViewController.interactionResponse!.users![i].updated_at!)
            let type = ViewController.interactionResponse!.users![i].role!
            let companies = ViewController.interactionResponse!.users![i].companies!
            let parent = ViewController.interactionResponse!.users![i].parent_id!
            let phone = ViewController.interactionResponse!.users![i].phone!
            
            employees.append(Employee(id: id, firstName: firstName,lastName: lastName, email: email, created: created, modified: updated!, type: type.name!,offices: companies,parent: parent,phone: phone))
        }
        tempEmployeesList = employees
        manageSorting()
        
        nameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        tempEmployeesList = []
        if(textField.text == "")
        {
            tempEmployeesList = employees
        }else{
            for i in (0..<employees.count)
            {
                var employeeName = "\(employees[i].firstName!)\(employees[i].lastName ?? "")"
                employeeName = employeeName.lowercased()
                
                if employeeName.contains(textField.text!.lowercased())
                {
                    tempEmployeesList.append(employees[i])
                }
            }
        }
        manageSorting()

    }
    
    func manageSorting()
    {
        //var sortedArray: [Employee] = []
        //sortedArray = employees.sorted(by: {$0.created!.compare($1.created!) == .orderedDescending})
        
        //employees = sortedArray
        employeesTableView.reloadData()
    }
    
    
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
   
    
    
    
    
    
}

extension EmployeesListViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempEmployeesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = employeesTableView.dequeueReusableCell(withIdentifier: "employeeitem", for: indexPath) as! EmployeeItemmCell
        
        cell.nameLbl.text = tempEmployeesList[indexPath.row].firstName!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let customerViewController = UIStoryboard.init(name: "Map", bundle: Bundle.main).instantiateViewController(withIdentifier: "MapViewViewController") as! MapViewViewController
        customerViewController.employeeName = tempEmployeesList[indexPath.row].firstName!
        customerViewController.employee = tempEmployeesList[indexPath.row]
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    
}


class EmployeeItemmCell: UITableViewCell
{
    
    @IBOutlet weak var nameLbl: UILabel!
    //@IBOutlet weak var nameLbll: UILabel!
}
