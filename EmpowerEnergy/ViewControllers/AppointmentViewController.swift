//
//  AppointmentViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 20/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SkeletonView

class AppointmentViewController: UIViewController {
    
    @IBOutlet weak var totalAppLabel: UILabel!
    
    
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    @IBOutlet weak var appProgressBar: UIProgressView!
    @IBOutlet weak var notCompletedLabel: UILabel!
    @IBOutlet weak var completedLabel: UILabel!
    var appointments: [Appointment] = []
    
    @IBOutlet weak var appointmentTableView: UITableView!
    
    var alert : UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Appointments"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.progressIndicator.isHidden = true
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        appointmentTableView.isSkeletonable = true
        appointmentTableView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .lightGray), animation: nil,  transition: .crossDissolve(0.25))
        //appointmentTableView.showAnimatedSkeleton(usingColor: .concrete, transition: .crossDissolve(0.25))
        
        getappointments()
        
        
    }
    
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func manageProgress()
    {
        if(appointments.count == 0)
        {
            appProgressBar.progress = 0.0
        }else
        {
            var count = 0
            for i in (0..<appointments.count)
            {
                if (appointments[i].held!)
                {
                    count = count + 1
                }
            }
            appProgressBar.progress = Float(count) / Float(appointments.count)
            
        }
        
        var completed: Int = 0
        var not_completed: Int = 0
        
        for i in (0..<appointments.count)
        {
            if(appointments[i].held!)
            {
                completed = completed + 1
            }
            else{
                not_completed = not_completed + 1
            }
        }
        
        completedLabel.text = "You completed \(completed) appointments"
        notCompletedLabel.text = "\(not_completed) appointments not completed"
        totalAppLabel.text = "\(appointments.count) Appointments"
    }
    
    
    //Get All Appointments
    func getappointments()
    {
        appointments = []
        progressIndicator.isHidden = false
        progressIndicator.startAnimating()
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/appointments";
        
        Alamofire.request(resourceString, method: .get, parameters: nil, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            self.appointments = []
                            self.progressIndicator.isHidden = true
                            self.progressIndicator.stopAnimating()
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode([AllAppointmentResponse].self, from: jsonData!)
                            
                            //Add Updated Status Interaction in List
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            if Constants.LOGGING_ENABLED{
                                print("Total Appointments: \(res.count)")}
                            for i in (0..<res.count)
                            {
                                
                                var held: Bool = false;
                                if res[i].held == 0
                                {
                                    held = false
                                }else
                                {
                                    held = true
                                }
                                let created_at = formatter.date(from: res[i].created_at! )
                                let updated_at = formatter.date(from: res[i].updated_at! )
                                let start = formatter.date(from: res[i].appointment_at!)
                                let end = formatter.date(from: res[i].appointment_end_at!)
                                if Constants.LOGGING_ENABLED{
                                    print(value)}
                                if(res[i].customer != nil){
                                    let address = res[i].customer!
                                    if Constants.LOGGING_ENABLED{
                                        print(res[i].appointment_at!)}
                                    self.appointments.append(Appointment(created: created_at!, end: end!, held: held, id: res[i].id!, modified: updated_at!, start: start!, title: res[i].title ?? "", content: res[i].description ?? "", user: res[i].user ?? UsersResponse(), address: address ))
                                }
                                
                            }
                            if Constants.LOGGING_ENABLED{
                                print("Total Appointments: \(self.appointments.count)")}
                            self.hideSkeleton()
                            var sortedArray: [Appointment] = []
                            sortedArray = self.appointments.sorted(by: {$0.start!.compare($1.start!) == .orderedDescending})
                            
                            self.appointments = sortedArray
                            
                            self.manageProgress()
                            self.appointmentTableView.reloadData()
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 401{
                       // print(response.result.value!)
                        self.hideSkeleton()
                        self.progressIndicator.isHidden = true
                        self.progressIndicator.stopAnimating()
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        //print(response.result.value)
                        self.hideSkeleton()
                        self.progressIndicator.isHidden = true
                        self.progressIndicator.stopAnimating()
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.hideSkeleton()
                self.progressIndicator.isHidden = true
                self.progressIndicator.stopAnimating()
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    
    //Delete Appointments
    func deleteAppointments(index: Int, id: Int)
    {
        present(alert!, animated: false, completion: nil)
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/appointments/\(id)";
        
        Alamofire.request(resourceString, method: .delete, parameters: nil, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        self.alert?.dismiss(animated: false, completion: nil)
                        if Constants.LOGGING_ENABLED{
                            print(value)}
//                        do{
//                            self.alert?.dismiss(animated: false, completion: nil)
//                            let json = JSON(value)
//                            let str = String(describing: json);
//                            let jsonData = str.data(using: .utf8)
//                            let decoder = JSONDecoder();
//                            let res = try decoder.decode(Bool.self, from: jsonData!)
                            
                            self.appointments.remove(at: index)
                            
                            var sortedArray: [Appointment] = []
                            sortedArray = self.appointments.sorted(by: {$0.start!.compare($1.start!) == .orderedDescending})
                            
                            self.appointments = sortedArray
                            
                            self.manageProgress()
                            self.appointmentTableView.reloadData()
                            
                            
                       // }catch{print(error)}
                        
                        
                    }
                    else if response_code == 401{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    //Update Status Appointments
    func updateStatusOfAppointments(index: Int, held: Int)
    {
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "held": held,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/appointments/\(appointments[index].id!)";
        
        Alamofire.request(resourceString, method: .put, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode(StatusAppointmentResponse.self, from: jsonData!)
                            
                            self.appointments[index].held = res.held!
                            
                            self.manageProgress()
                            self.appointmentTableView.reloadData()
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 401{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        self.alert?.dismiss(animated: false, completion: nil)
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    func hideSkeleton()
    {
        appointmentTableView.stopSkeletonAnimation()
        self.view.hideSkeleton()
    }
    
    @objc
    func deleteAppointment(tapGesture: UITapGestureRecognizer)
    {
        let imageView = tapGesture.view as! UIImageView
        let index = imageView.tag
        
        let refreshAlert = UIAlertController(title: "", message: "Are you sure to delete appointment?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
            self.deleteAppointments(index: index, id: self.appointments[index].id!)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @objc
    func editAppointment(tapGesture: UITapGestureRecognizer)
    {
        let imageView = tapGesture.view as! UIImageView
        let index = imageView.tag
        
        let customerViewController = UIStoryboard.init(name: "EditAppointment", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditAppointmentViewController") as! EditAppointmentViewController
        customerViewController.appointment = self.appointments[index]
        customerViewController.editAppointmentDelegate = self
        customerViewController.index = index
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    @objc
    func updateAppointment(tapGesture: UITapGestureRecognizer)
    {
        let imageView = tapGesture.view as! UIImageView
        let index = imageView.tag
        
        if appointments[index].held!
        {
            let refreshAlert = UIAlertController(title: "", message: "Are you sure to change appointment status to Not Complete?", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Change", style: .default, handler: { (action: UIAlertAction!) in
                refreshAlert.dismiss(animated: false, completion: nil)
                self.updateStatusOfAppointments(index: index, held: 0)
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                refreshAlert.dismiss(animated: false, completion: nil)
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        }else
        {
            let refreshAlert = UIAlertController(title: "", message: "Are you sure to change appointment status to Complete?", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Change", style: .default, handler: { (action: UIAlertAction!) in
                refreshAlert.dismiss(animated: false, completion: nil)
                self.updateStatusOfAppointments(index: index, held: 1)
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                refreshAlert.dismiss(animated: false, completion: nil)
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
    }
    
}

extension AppointmentViewController: SkeletonTableViewDataSource, SkeletonTableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.appointments.count
        
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "appointmentcell" as ReusableCellIdentifier
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = appointmentTableView.dequeueReusableCell(withIdentifier: "appointmentcell", for: indexPath) as! AppointmentCell
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let now = df.string(from: appointments[indexPath.row].start!)
        
        cell.appointmentLabel.text = now
        
        if appointments[indexPath.row].held!
        {
            cell.appointmentImage.image = UIImage(named: "appointment_completed")
        }
        else
        {
            cell.appointmentImage.image = UIImage(named: "appointment_not_completed")
        }
        
        cell.deleteImage.tag = indexPath.row
        cell.deleteImage.isUserInteractionEnabled = true
        let imageGesture = UITapGestureRecognizer(target: self, action: #selector(deleteAppointment(tapGesture:)))
        imageGesture.numberOfTapsRequired = 1
        cell.deleteImage.addGestureRecognizer(imageGesture)
        
        cell.editImage.tag = indexPath.row
        cell.editImage.isUserInteractionEnabled = true
        let imageGesture1 = UITapGestureRecognizer(target: self, action: #selector(editAppointment(tapGesture:)))
        imageGesture1.numberOfTapsRequired = 1
        cell.editImage.addGestureRecognizer(imageGesture1)
        
        cell.changeImage.tag = indexPath.row
        cell.changeImage.isUserInteractionEnabled = true
        let imageGesture2 = UITapGestureRecognizer(target: self, action: #selector(updateAppointment(tapGesture:)))
        imageGesture2.numberOfTapsRequired = 1
        cell.changeImage.addGestureRecognizer(imageGesture2)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let customerViewController = UIStoryboard.init(name: "EditAppointment", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditAppointmentViewController") as! EditAppointmentViewController
        customerViewController.appointment = self.appointments[indexPath.row]
        customerViewController.editAppointmentDelegate = self
        customerViewController.index = indexPath.row
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    
}

class AppointmentCell: UITableViewCell
{
    @IBOutlet weak var editImage: UIImageView!
    @IBOutlet weak var changeImage: UIImageView!
    @IBOutlet weak var deleteImage: UIImageView!
    
    @IBOutlet weak var appointmentImage: UIImageView!
    @IBOutlet weak var appointmentLabel: UILabel!
}


extension AppointmentViewController: EditAppointmentDelegate
{
    func editAppointment(appointmentResponsee: AppointmentResponse,index: Int) {
        self.navigationController?.popViewController(animated: true)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        
        let created_at = formatter.date(from: appointmentResponsee.created_at! )
        let updated_at = formatter.date(from: appointmentResponsee.updated_at! )
        let start = formatter.date(from: appointmentResponsee.appointment_at!)
        let end = formatter.date(from: appointmentResponsee.appointment_end_at!)
        
        self.appointments[index] = Appointment(created: created_at!, end: end!, held: appointmentResponsee.held!, id: appointmentResponsee.id!, modified: updated_at!, start: start!, title: appointmentResponsee.title ?? "", content: appointmentResponsee.description!, user: appointmentResponsee.user ?? UsersResponse(), address: appointmentResponsee.address! )
        
        var sortedArray: [Appointment] = []
        sortedArray = self.appointments.sorted(by: {$0.start!.compare($1.start!) == .orderedDescending})
        
        self.appointments = sortedArray
        
        self.manageProgress()
        self.appointmentTableView.reloadData()
        Toast.show(message: "Appointment updated Successfully", controller: self)
    }
    
    
}
