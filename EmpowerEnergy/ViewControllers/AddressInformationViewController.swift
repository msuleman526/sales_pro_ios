//
//  AddressInformationViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 27/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import  GoogleMaps
import GoogleMapsUtils
import MapKit

class AddressInformationViewController: UIViewController,CLLocationManagerDelegate{
    
    @IBOutlet weak var handleArea2: UIView!
    var filterIndex:Int = 0
    
    @IBOutlet weak var sendRequestView: UIView!
    
    @IBOutlet weak var addAppointmentView: UIView!
    @IBOutlet weak var historyProgress: UIActivityIndicatorView!
    
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var interactionImage: UIImageView!
    @IBOutlet weak var customerTableHeight: NSLayoutConstraint!
    @IBOutlet weak var customerTableView: UITableView!
    @IBOutlet weak var postalLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var routeOnMapView: UIView!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var handleArea: UIView!
    
    var historyList: [History] = [];
    var histories: [History] = [];
    @IBOutlet weak var interactionCollectionView: UICollectionView!
    static var interactions:[Interaction]?;
    static var customers: [Customer] = []
    var index: Int?
    var homeViewController:HomeViewController?
    var addressInteraction: InteractionAddress?
    //@IBOutlet weak var InteractionImage: UICollectionView!
    var alert: UIAlertController?
    @IBOutlet weak var createCustomerView: UIView!
    
    @IBOutlet weak var deleteAddressImage: UIImageView!
    var locationManager : CLLocationManager = CLLocationManager()
    @IBOutlet weak var addNoteView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        AddressInformationViewController.interactions = ViewController.interactionResponse?.interactions;
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        let createCustomerGesture = UITapGestureRecognizer(target: self, action: #selector(createCustomerClick))
        createCustomerView.addGestureRecognizer(createCustomerGesture)
        
        let mapGesture = UITapGestureRecognizer(target: self, action: #selector(openMapForPlace))
        routeOnMapView.addGestureRecognizer(mapGesture)
        
        let noteGesture = UITapGestureRecognizer(target: self, action: #selector(openAddNote))
        addNoteView.addGestureRecognizer(noteGesture)
        
        let appointmentGesture = UITapGestureRecognizer(target: self, action: #selector(openAddAppointment))
        addAppointmentView.addGestureRecognizer(appointmentGesture)
        
        let deleteGesture = UITapGestureRecognizer(target: self, action: #selector(deleteAddressInformation))
        deleteAddressImage.isUserInteractionEnabled = true
        deleteAddressImage.addGestureRecognizer(deleteGesture)
        
        
        let requestGesture = UITapGestureRecognizer(target: self, action: #selector(openSendRequest))
        sendRequestView.addGestureRecognizer(requestGesture)
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            //locationManager.location?.coordinate.latitude
            
        }
        
    }
    
    @objc func deleteAddressInformation()
    {
        let refreshAlert = UIAlertController(title: "", message: "Are you sure to delete address permanently?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
            self.deleteAddress(customerId: (self.addressInteraction?.address?.id!)!, clusterIndex: self.index!)
            //self.deleteCustomer(index: <#T##Int#>, customer: <#T##Customer#>, clusterIndex: <#T##Int#>)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.customerTableHeight.constant
            = self.customerTableView.contentSize.height
        
    }
    
    
    func loadInformation(addressInteraction: InteractionAddress,index: Int)
    {
        if(SessionUtils.getUserType() != "Admin" && SessionUtils.getUserType() != "Superadmin" && SessionUtils.getUserType() != "SuperAdmin")
           {
               deleteAddressImage.isHidden = true
        }else{
            deleteAddressImage.isHidden = false
        }
        self.index = index
        self.addressInteraction = addressInteraction
        AddressInformationViewController.customers = addressInteraction.customers!
        streetLabel.text = addressInteraction.address!.street!
        if(addressInteraction.address!.postal == "0")
        {
            postalLabel.text = "00000"
        }else
        {
            postalLabel.text = addressInteraction.address!.postal!
        }
        cityLabel.text = addressInteraction.address!.city ?? "New York"
        countryLabel.text = addressInteraction.address!.country ?? "USA"
        AddressInformationViewController.customers = addressInteraction.customers!
        customerTableView.reloadData()
        historyTableView.reloadData()
        loadInteractionImage()
        if(Constants.LOGGING_ENABLED){
            print(addressInteraction.address!.location!.latitude!)
            print(addressInteraction.address!.location!.longitude!)
            print(addressInteraction.address!.number!)
        }
    }
    
    @objc func openMapForPlace() {
        
        let coordinate = CLLocationCoordinate2DMake((addressInteraction?.address?.location?.latitude)!, (addressInteraction?.address?.location?.longitude)!)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = "Target location"
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        
        /*if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(String(describing: addressInteraction?.address?.location?.latitude)),\(String(describing: addressInteraction?.address?.location?.longitude))&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
        } else {
            NSLog("Can't use comgooglemaps://");
        }*/
        
        
    }
    
    func loadInteractionImage()
    {
        if(self.addressInteraction?.interaction?.name != "None")
        {
            let color = self.addressInteraction?.interaction?.color!
            let icon = self.addressInteraction?.interaction?.icon!
            let tmpImage = Utils.customImage(diameter: interactionImage.frame.width, color: Utils.hexStringToUIColor(hex: color!))
            self.interactionImage.image = Utils.textToImage(drawText: icon! as NSString, inImage: tmpImage)
        }else{
            self.interactionImage.image  = UIImage(named: "ic_no_interaction_48dp")
        }
    }
    
    @objc
    func openAddNote()
    {
        let customerViewController = UIStoryboard.init(name: "AddNote", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddNoteViewController") as! AddNoteViewController
        customerViewController.addNoteDelegate = self
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    @objc
    func openSendRequest()
    {
        let customerViewController = UIStoryboard.init(name: "Request", bundle: Bundle.main).instantiateViewController(withIdentifier: "RequestViewController") as! RequestViewController
        customerViewController.addressInteraction = self.addressInteraction!
        customerViewController.sendRequestDelegate = self
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    
    @objc
    func openAddAppointment()
    {
        let customerViewController = UIStoryboard.init(name: "AddAppointment", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddAppointmentViewController") as! AddAppointmentViewController
        customerViewController.locationStr = self.addressInteraction!.address!.street!
        customerViewController.address_id = self.addressInteraction!.address!.id!
        customerViewController.addAppointmentDelegate = self
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    
    @objc
    func createCustomerClick()
    {
        let customerViewController = UIStoryboard.init(name: "EditCustomer", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditCustomerViewController") as! EditCustomerViewController
        if((self.addressInteraction?.customers!.count)! <= 0)
        {
            customerViewController.customer = Customer(id: (self.addressInteraction?.address?.id!)!, address_id: (self.addressInteraction?.address?.id!)!)
        }else{
            customerViewController.customer = self.addressInteraction?.customers?[0]}
            customerViewController.clusterIndex = self.index
            customerViewController.index = 0;
            customerViewController.editCustomerDelegate = self
        self.navigationController?.pushViewController(customerViewController, animated: false);
        
    }
    
    func updateInteractionStatus(interaction: Interaction, addressId: Int, interactionId: Int)
    {
        
        if CLLocationManager.locationServicesEnabled() {
            let latitude = locationManager.location?.coordinate.latitude
            let longitude = locationManager.location?.coordinate.longitude
            
            if latitude == nil || longitude == nil || latitude == 0.0 || longitude == 0.0
            {
                Toast.show(message: "Please Enable location from settings", controller: self)
            }else
            {
                
                present(alert!, animated: false, completion: nil)
                let parameters =  [
                    "interaction_id": interactionId,
                    "lat": addressInteraction!.address!.location!.latitude!,
                    "long": addressInteraction!.address!.location!.longitude!,
                    "current_lat": latitude!,
                    "current_long": longitude!,
                    ] as [String : Any]
                
                let header = [
                    "Accept": "text/json",
                    "Authorization": "bearer \(SessionUtils.getToken())"
                ]
                
                let resourceString = "\(Utils.requestURL)api/address/interact/\(addressId)";
                print(resourceString)
                
                Alamofire.request(resourceString, method: .put, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        if let httpURLResponse = response.response{
                            let response_code = httpURLResponse.statusCode;
                            if response_code == 200{
                                do{
                                    
                                    self.alert?.dismiss(animated: false, completion: nil)
                                    let json = JSON(value)
                                    let str = String(describing: json);
                                    let jsonData = str.data(using: .utf8)
                                    let decoder = JSONDecoder();
                                    let res = try decoder.decode(InteractionResponse.self, from: jsonData!)
                                    self.addressInteraction?.interaction = res.interaction
                                    self.loadInteractionImage()
                                    self.homeViewController?.updateStatus(clusterIndex: self.index!, interaction: (self.addressInteraction?.interaction)!)
                                    
                                    //Add Updated Status Interaction in List
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    
                                    let created_at = formatter.date(from: res.created_at!)
                                    let updated_at = formatter.date(from: res.updated_at!)
                                    self.historyList.append(History(id: History.STATUSES, interaction: res.interaction, created: created_at!, modified: updated_at!))
                                    
                                    self.filterItems()
                                    Toast.show(message: "Status updated successfully", controller: self)
                                    
                                    
                                    
                                }catch{
                                    if Constants.LOGGING_ENABLED{
                                        print(error)}}
                                
                                
                            }
                            else{
                                //print(response.result.value)
                                self.alert?.dismiss(animated: false, completion: nil)
                                Toast.show(message: "No Internet Connection / Server Error", controller: self)
                                
                            }
                        }
                    case .failure(let error):
                        //print(response.result.value)
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "\(error)", controller: self)
                    }
                    
                })
                
            }
            
        }
        
    }
    
    func deleteCustomer(customerId: Int,clusterIndex: Int,index: Int)
    {
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "customer_id": customerId
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/customers/\(customerId)";
        
        Alamofire.request(resourceString, method: .delete, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        if Constants.LOGGING_ENABLED{
                            print(value)}
                            self.alert?.dismiss(animated: false, completion: nil)
                            //let json = JSON(value)
                            //let str = String(describing: json);
                            //let jsonData = str.data(using: .utf8)
                            ////let res = try decoder.decode(Bool.self, from: jsonData!)
                            self.homeViewController?.deleteCustomer(index: index, clusterIndex: clusterIndex)
                            //self.deleteCustomer(customerId: customer.id!)
                            AddressInformationViewController.customers.remove(at: index)
                            self.customerTableView.reloadData()
                        self.homeViewController!.items.remove(at: clusterIndex)
                        self.homeViewController!.clusterManager?.clearItems()
                        self.homeViewController!.clusterManager?.add(self.homeViewController!.items)
                        self.homeViewController!.clusterManager?.cluster()
                        self.homeViewController!.currentMarker?.map = nil
                        self.homeViewController!.addressInformationController.view.frame.origin.y = self.view.frame.height
                            Toast.show(message: "Customer and Address deleted successfully", controller: self)
                         
                        
                        
                    }
                    else if response_code == 422 {
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Given Data is Invalid", controller: self)
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
            }
            
        })
    }
    
    
    
    func deleteAddress(customerId: Int,clusterIndex: Int)
    {
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "customer_id": customerId
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/customers/\(customerId)";
        
        Alamofire.request(resourceString, method: .delete, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        if Constants.LOGGING_ENABLED{
                            print(value)}
                            self.alert?.dismiss(animated: false, completion: nil)
                            //let json = JSON(value)
                            //let str = String(describing: json);
                            //let jsonData = str.data(using: .utf8)
                            ////let res = try decoder.decode(Bool.self, from: jsonData!)
                        //self.homeViewController.address
                            //self.deleteCustomer(customerId: customer.id!)
                            self.homeViewController!.addressInformationController.view.frame.origin.y = self.view.frame.height
                        self.homeViewController!.items.remove(at: clusterIndex)
                        self.homeViewController!.clusterManager?.clearItems()
                        self.homeViewController!.clusterManager?.add(self.homeViewController!.items)
                        self.homeViewController!.clusterManager?.cluster()
                        self.homeViewController!.currentMarker?.map = nil
                            Toast.show(message: "Address deleted successfully", controller: self.homeViewController!)
                         
                        
                        
                    }
                    else if response_code == 422 {
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Given Data is Invalid", controller: self)
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
            }
            
        })
    }
    
    func addCustomer(customer: Customer, addressId: Int)
    {
        if Constants.LOGGING_ENABLED{
            print("Add Customer")}
        present(alert!, animated: false, completion: nil)
        var birthday = "";
        let first_name = customer.firstName ?? ""
        let last_name = customer.lastName ?? ""
        let gender = customer.gender ?? ""
        let home_phone = customer.homePhone ?? ""
        let mobile_phone = customer.mobilePhone ?? ""
        let work_phone = customer.workPhone ?? ""
        let personal_email = customer.personalEmail ?? ""
        let work_email = customer.workEmail ?? ""
        if(customer.birthday != "")
        {
            birthday = customer.birthday ?? "";
        }
        let parameters =  [
            "address_id": addressId,
            "first_name": first_name,
            "last_name": last_name,
            "birth_day": birthday,
            "gender": gender,
            "home_phone": home_phone,
            "mobile_phone": mobile_phone,
            "work_phone": work_phone,
            "work_email": work_email,
            "personal_email": personal_email,
            "resident": 1,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/customers/\(addressId)/add";
        
        Alamofire.request(resourceString, method: .post, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if Constants.LOGGING_ENABLED{
                    print("on Success")}
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode(CustomerSearchData.self, from: jsonData!)
                            if Constants.LOGGING_ENABLED{
                                print(res.id!)
                            print(res.first_name!)
                            print(res.last_name ?? "")
                            print(res.mobile_phone ?? "")
                            print(res.home_phone ?? "")
                            print(res.work_phone ?? "")
                            print(res.work_phone ?? "")
                            print(res.personal_email ?? "")
                            print(res.work_email ?? "")
                            }
                            let customerData = Customer(id: res.id!, firstName: res.first_name!, lastName: res.last_name ?? "", mobilePhone: res.mobile_phone ?? "", homePhone: res.home_phone ?? "", workPhone: res.work_phone ?? "", personalEmail: res.personal_email ?? "", workEmail: res.work_email ?? "", resident: res.resident ?? true, genderStr: res.gender ?? "Male", birthday: res.birthday ?? "1995-01-01", address_id: res.address_id!, addressData: res.address ?? InteractionAddressData() )
                            AddressInformationViewController.customers.append(customerData)
                            self.homeViewController?.addCustomer(clusterIndex: self.index!, customer: customerData)
                            self.customerTableView.reloadData()
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 422 {
                        if Constants.LOGGING_ENABLED{
                        print(response.result.value!)
                        }
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Given Data is Invalid", controller: self)
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                        print (response_code)
                        print(response.result.value!)
                        }
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
                if Constants.LOGGING_ENABLED{
                print(error)
                }
            }
            
        })
    }
    
    func editCustomer(customer: Customer, addressId: Int,customerIndex: Int)
    {
        present(alert!, animated: false, completion: nil)
        let income = customer.income ?? "$0 - $0";
        let first_name = customer.firstName ?? ""
        let last_name = customer.lastName ?? ""
        let gender = customer.gender ?? ""
        let net_worth = customer.net_worth ?? "$0 - $0"
        let credit_rating = customer.credit_rating ?? "0 - 0"
        let work_phone = customer.mobilePhone ?? ""
        let personal_email = customer.personalEmail ?? ""
        let customer_id = customer.id ?? 0
        let parameters =  [
            "first_name": first_name,
            "last_name": last_name,
            "gender": gender,
            "income": income,
            "net_worth": net_worth,
            "phone": work_phone,
            "credit_rating": credit_rating,
            "age": customer.age ?? "",
            "email": personal_email,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/customers/\(customer_id)";
        
        Alamofire.request(resourceString, method: .put, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        if Constants.LOGGING_ENABLED{
                            print(value)
                        }
                            self.alert?.dismiss(animated: false, completion: nil)
//                        do{
//
//                            let json = JSON(value)
//                            let str = String(describing: json);
//                            let jsonData = str.data(using: .utf8)
//                            let decoder = JSONDecoder();
//                            let res = try decoder.decode(Bool.self, from: jsonData!)
                            if(AddressInformationViewController.customers.count > 0){
                                AddressInformationViewController.customers[customerIndex] = customer
                            }
                            else{
                                AddressInformationViewController.customers.append(customer)
                            }
                            self.homeViewController?.editCustomer(clusterIndex: self.index!, customer: customer,customerIndex: customerIndex)
                            self.customerTableView.reloadData()
                            Toast.show(message: "Customer updated successfully", controller: self)
                            
                            
                        //}catch{print(error)}
                        
                        
                    }
                    else if response_code == 422 {
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Given Data is Invalid", controller: self)
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                        print (response_code)
                        }
                        print("Error is \(response.result.value!)")
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
            }
            
        })
    }
    
    func addNote(note: String)
    {
        present(alert!, animated: false, completion: nil)
        let notes = note
        let address_id = self.addressInteraction?.address?.id ?? 0
        let parameters =  [
            "note": notes,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/address/note/\(address_id)";
        
        Alamofire.request(resourceString, method: .post, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            if Constants.LOGGING_ENABLED{
                            print(value)
                            }
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode(NotesResponse.self, from: jsonData!)
                            
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            
                            
                            let created_at = formatter.date(from: res.created_at!)
                            let updated_at = formatter.date(from: res.updated_at!)
                            self.historyList.append(History(id: History.NOTES, note: res.note!, user: res.user ?? UsersResponse(), created: created_at!, modified: updated_at!, note_id: res.id!))
                            
                            self.filterItems()
                            
                            Toast.show(message: "Note Added successfully", controller: self)
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 422 {
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Given Data is Invalid", controller: self)
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                        print (response_code)
                        print("Error is \(response.result.value!)")
                        }
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
            }
            
        })
    }
    
    func filterItems()
    {
        histories = [];
        var tmpList: [History] = []
        for i in (0..<historyList.count)
        {
            if(filterIndex == 0)
            {
                tmpList.append(historyList[i])
                if(historyList[i].id == History.APPOINTMENTS)
                {
                    if Constants.LOGGING_ENABLED{
                    print("App Name: - \(historyList[i].title!)")
                    }
                }
            }
            else if(filterIndex == 1)
            {
                if(historyList[i].id == History.NOTES)
                {
                    tmpList.append(historyList[i])
                }
            }
            else if(filterIndex == 2)
            {
                if(historyList[i].id == History.APPOINTMENTS)
                {
                    tmpList.append(historyList[i])
                    if Constants.LOGGING_ENABLED{
                        print("App Name: - \(historyList[i].title!)")}
                }
            }
            else if(filterIndex == 3)
            {
                if(historyList[i].id == History.STATUSES)
                {
                    tmpList.append(historyList[i])
                    
                }
            }
            
        }
        
        //Sort Items
        var sortedArray: [History] = []
        sortedArray = tmpList.sorted(by: {$0.created!.compare($1.created!) == .orderedDescending})
        
        tmpList = sortedArray
        histories = tmpList
        loadInformation(addressInteraction: addressInteraction!, index: index!)
        
    }
    
    func getAddressHistory(address_id:Int)
    {
        histories = [];
        historyList = [];
        historyTableView.reloadData()
        self.historyProgress.isHidden = false
        self.historyProgress.startAnimating()
        do{
            let header = [
                "Authorization": "bearer \(SessionUtils.getToken())"
            ]
            let resourceString = "\(Utils.requestURL)api/address/\(address_id)";
            
            Alamofire.request(resourceString, method: .get, parameters: nil, headers: header).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    if let httpURLResponse = response.response{
                        let response_code = httpURLResponse.statusCode;
                        if response_code == 200 {
                            do{
                                if Constants.LOGGING_ENABLED{
                                    print(value)}
                                self.historyProgress.isHidden = true
                                self.historyProgress.stopAnimating()
                                let json = JSON(value)
                                let str = String(describing: json);
                                let jsonData = str.data(using: .utf8)
                                let decoder = JSONDecoder();
                                let res = try decoder.decode(AddressHistoryResponse.self, from: jsonData!)

                                let formatter = DateFormatter()
                                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                for i in (0..<res.notes!.count)
                                {
                                    if Constants.LOGGING_ENABLED{
                                        print("Date is Before \(res.notes![i].created_at!)")}
                                    let created_at = formatter.date(from: res.notes![i].created_at!)
                                    let updated_at = formatter.date(from: res.notes![i].updated_at!)
                                   
                                    self.historyList.append(History(id: History.NOTES, note: res.notes![i].note!, user: res.notes![i].user ?? UsersResponse(), created: created_at!, modified: updated_at!, note_id: res.notes![i].id!))
                                }
                                
                                for i in (0..<res.interactions!.count)
                                {
                                    let created_at = formatter.date(from: res.interactions![i].created_at!)
                                    let updated_at = formatter.date(from: res.interactions![i].updated_at!)
                                    self.historyList.append(History(id: History.STATUSES, interaction: res.interactions![i].interaction!, created: created_at!, modified: updated_at!,user: res.interactions![i].user!))
                                    
                                }
                                
                                for i in (0..<res.appointments!.count)
                                {
                                    let created_at = formatter.date(from: res.appointments![i].created_at!)
                                    let updated_at = formatter.date(from: res.appointments![i].updated_at!)
                                    let appointment_at = formatter.date(from: res.appointments![i].appointment_at!)
                                    let appointment_end_at = formatter.date(from: res.appointments![i].appointment_end_at!)
                                    
                                    self.historyList.append(History(id: History.APPOINTMENTS, content: res.appointments![i].description!, appointment_at: appointment_at!, appointment_end_at: appointment_end_at!, held: (res.appointments![i].held != nil), title: res.appointments![i].title!, created: created_at!, updated: updated_at!, user: res.appointments![i].user))
                                    
                                }
                                self.filterItems()
                                
                            }catch{
                                if Constants.LOGGING_ENABLED{
                                    print(error)}}
                            
                            
                        }else if response_code == 401{
                            self.historyProgress.isHidden = true
                            self.historyProgress.stopAnimating()
                            Toast.show(message: "Unauthorized User", controller: self)
                            let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            self.navigationController?.pushViewController(viewController!, animated: false);
                            //self.present(viewController!,animated: true,completion: nil)
                            
                        }else{
                            self.historyProgress.isHidden = true
                            self.historyProgress.stopAnimating()
                            Toast.show(message: "No Internet Connection/Server Issue", controller: self)
                        }
                    }
                case .failure(let error):
                    self.historyProgress.isHidden = true
                    self.historyProgress.stopAnimating()
                    if Constants.LOGGING_ENABLED{
                    print(error)
                    }
                    
                }
                
            })
            
            
            
        }
        
        
        
        
    }
    
    
}

extension AddressInformationViewController: UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = interactionCollectionView.dequeueReusableCell(withReuseIdentifier: "interactionImage", for: indexPath) as? InteractionListCell
        
        //Set InteractionImages
        let color = AddressInformationViewController.interactions![indexPath.row].color!
        let icon = AddressInformationViewController.interactions![indexPath.row].icon!
        let tmpImage = Utils.customImage(diameter: 35, color: Utils.hexStringToUIColor(hex: color))
        cell?.interactionImage.image = Utils.textToImage(drawText: icon as NSString, inImage: tmpImage)
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AddressInformationViewController.interactions!.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedInteraction: Interaction = AddressInformationViewController.interactions![indexPath.row]
        let interactionId: Int = AddressInformationViewController.interactions![indexPath.row].id!
        let addressId: Int = (addressInteraction?.address?.id)!
        
        if (self.addressInteraction!.interaction!.id == interactionId)
        {
            Toast.show(message: "Already selected this interaction status", controller: self)
        }else{
    
            let refreshAlert = UIAlertController(title: "", message: "Are you sure to update status?", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (action: UIAlertAction!) in
                refreshAlert.dismiss(animated: false, completion: nil)
                self.updateInteractionStatus(interaction: selectedInteraction, addressId: addressId, interactionId: interactionId)
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                refreshAlert.dismiss(animated: false, completion: nil)
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
        
    }
    
    
    
}

extension AddressInformationViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.customerTableView)
        {
            return AddressInformationViewController.customers.count;
        }
        else
        {
            return self.histories.count;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == self.customerTableView)
        {
            let cell = customerTableView.dequeueReusableCell(withIdentifier: "customer", for: indexPath) as? CustomerListCell
            let customer = AddressInformationViewController.customers[indexPath.row]
            if(customer.resident ?? true)
            {
                cell?.customerImage.image = UIImage(named: "not_resident")
            }else
            {
                cell?.customerImage.image = UIImage(named: "not_resident")
            }
            cell?.customerName.text = "\(customer.firstName ?? "") \(customer.lastName ?? "")"
            //cell?.detailTextLabel!.text = ""
            cell?.customerDescripttion.text = "\(customer.credit_rating!) credit, \(customer.income!) income"
            return cell!
            
        }
        else{
            
            let cell = historyTableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as? HistoryListCell
            
            if(histories[indexPath.row].id == History.NOTES)
            {
                let userName = histories[indexPath.row].user?.name
                let note = histories[indexPath.row].note
                
                let main_text = "A note was added by \(userName ?? SessionUtils.getName())"
                let range = (main_text as NSString).range(of: "\(userName ?? SessionUtils.getName())")
                
                let attribute = NSMutableAttributedString.init(string: main_text)
                attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "textcolor3")!, range: range)
                
                cell?.historyText.attributedText = attribute
                cell?.descriptionText.text = "\(note ?? "")"
                
                cell?.historyImage.image = UIImage(named: "note")
                
                let formatterTime = DateFormatter()
                formatterTime.timeZone = NSTimeZone(abbreviation: "GMT-2.00") as TimeZone?
                formatterTime.dateFormat = "HH:mm:ss"
                let formatterDate = DateFormatter()
                formatterDate.dateFormat = "MMM dd, yyyy"
                
                let time = formatterTime.string(from: histories[indexPath.row].created!)
                if Constants.LOGGING_ENABLED{
                print("Time is-\(time)")
                }
                let date = formatterDate.string(from: histories[indexPath.row].created!)
                
                cell?.historyTime.text = time
                cell?.historyDate.text = date
            }
            if(histories[indexPath.row].id == History.STATUSES)
            {
                let userName = histories[indexPath.row].user?.name
                let status = histories[indexPath.row].interaction?.name
                
                let main_text = "Status changed by \(userName ?? SessionUtils.getName())"
                let range = (main_text as NSString).range(of: "\(userName ?? SessionUtils.getName())")
                
                let attribute = NSMutableAttributedString.init(string: main_text)
                attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "textcolor3")!, range: range)
                
                cell?.historyText.attributedText = attribute
                cell?.descriptionText.text = "\(status ?? "")"
                
                //Set Image
                let color = histories[indexPath.row].interaction!.color!
                let icon = histories[indexPath.row].interaction!.icon!
                let tmpImage = Utils.customImage(diameter: 30, color: Utils.hexStringToUIColor(hex: color))
                cell?.historyImage.image = Utils.textToImage(drawText: icon as NSString, inImage: tmpImage)
                
                let formatterTime = DateFormatter()
                formatterTime.timeZone = NSTimeZone(abbreviation: "GMT-2.00") as TimeZone?
                formatterTime.dateFormat = "HH:mm:ss"
                let formatterDate = DateFormatter()
                formatterDate.dateFormat = "MMM dd, yyyy"
                
                let time = formatterTime.string(from: histories[indexPath.row].created!)
                let date = formatterDate.string(from: histories[indexPath.row].created!)
                
                cell?.historyTime.text = time
                cell?.historyDate.text = date
            }
            
            if(histories[indexPath.row].id == History.APPOINTMENTS)
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "MMM dd yyyy, HH:mm:ss"
                
                let dateStr = formatter.string(from: histories[indexPath.row].appointment_at!)
                let description = histories[indexPath.row].title
                
                let main_text = "Appointment scheduled for: \(dateStr)"
                let range = (main_text as NSString).range(of: "\(dateStr)")
                
                let attribute = NSMutableAttributedString.init(string: main_text)
                attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(named: "textcolor3")!, range: range)
                
                cell?.historyText.attributedText = attribute
                cell?.descriptionText.text = "\(description ?? "")"
                
                cell?.historyImage.image = UIImage(named: "appointment")
                
                let formatterTime = DateFormatter()
                formatterTime.timeZone = NSTimeZone(abbreviation: "GMT-2.00") as TimeZone?
                formatterTime.dateFormat = "HH:mm:ss"
                let formatterDate = DateFormatter()
                formatterDate.dateFormat = "MMM dd, yyyy"
                
                let time = formatterTime.string(from: histories[indexPath.row].created!)
                let date = formatterDate.string(from: histories[indexPath.row].created!)
                
                cell?.historyTime.text = time
                cell?.historyDate.text = date
            }
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = CGFloat()
        if(tableView == historyTableView)
        {
            height = 64
            return height
        }else
        {
            return 45
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(tableView == customerTableView){
            let customerViewController = UIStoryboard.init(name: "CustomerDetail", bundle: Bundle.main).instantiateViewController(withIdentifier: "CustomerDetailViewController") as! CustomerDetailViewController
            customerViewController.customer = AddressInformationViewController.customers[indexPath.row]
            customerViewController.selectedIndex = indexPath.row
            customerViewController.clusterIndex = index
            customerViewController.deleteCustomerDelegate = self
            customerViewController.addressInformationControlller = self
            self.navigationController?.pushViewController(customerViewController, animated: false);
            //self.present(customerViewController, animated: false, completion: nil)
        }
    }
    
    
}

class InteractionListCell: UICollectionViewCell
{
    @IBOutlet weak var interactionImage: UIImageView!
    
}

class CustomerListCell: UITableViewCell
{
    
    @IBOutlet weak var customerDescripttion: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var customerImage: UIImageView!
}

class HistoryListCell: UITableViewCell
{
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var historyImage: UIImageView!
    @IBOutlet weak var historyText: UILabel!
    
    @IBOutlet weak var historyTime: UILabel!
    
    @IBOutlet weak var historyDate: UILabel!
}

extension AddressInformationViewController: DeleteCustomerDelegate{
    func deleteCustomer(index: Int, customer: Customer, clusterIndex: Int) {
        self.navigationController?.popViewController(animated: true)
        //homeViewController?.deleteCustomer(index: index, clusterIndex: clusterIndex)
        self.deleteCustomer(customerId: customer.id!,clusterIndex: clusterIndex,index: index)
        //AddressInformationViewController.customers.remove(at: index)
        //self.customerTableView.reloadData()
    }
    
}

extension AddressInformationViewController: AddCustomerDelegate{
    func addCustomer(customer: Customer) {
        self.navigationController?.popViewController(animated: true)
        self.addCustomer(customer: customer,addressId: (self.addressInteraction?.address?.id)!)
        self.customerTableView.reloadData()
    }
    
    
}

extension AddressInformationViewController: EditCustomerDelegatee{
    func editCustomer(index: Int, customer: Customer, clusterIndex: Int) {
        self.navigationController?.popToViewController(self.homeViewController!, animated: true)
        //print("\(index)")
        //print("\(String(describing: self.addressInteraction?.address!.id))")
        //print("\(clusterIndex)")
        editCustomer(customer: customer, addressId: (self.addressInteraction?.address?.id!)!, customerIndex: index)
        self.customerTableView.reloadData()
    }
    
    
}

extension AddressInformationViewController: AddNoteDelegate{
    func addNote(notes: String) {
        self.navigationController?.popToViewController(self.homeViewController!, animated: true)
        addNote(note: notes)
    }
    
    
}

extension AddressInformationViewController: SendRequestDelegate{
    func sendRequest() {
        if Constants.LOGGING_ENABLED{
        print("Request Sent")
        }
        self.navigationController?.popToViewController(self.homeViewController!, animated: true)
        Toast.show(message: "Request sent successfully to designer", controller: self.homeViewController!)
    }
    
    
}

extension AddressInformationViewController: AddAppointmentDelegate
{
    func addAppointment(appointmentResponsee: AppointmentResponse) {
        self.navigationController?.popToViewController(self.homeViewController!, animated: true)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        
        let created_at = formatter.date(from: appointmentResponsee.created_at!)
        let updated_at = formatter.date(from: appointmentResponsee.updated_at!)
        let appointment_at = formatter.date(from: appointmentResponsee.appointment_at!)
        let appointment_end_at = formatter.date(from: appointmentResponsee.appointment_end_at!)
        
        
        self.historyList.append(History(id: History.APPOINTMENTS, content: appointmentResponsee.description!, appointment_at: appointment_at!, appointment_end_at: appointment_end_at!, held: (appointmentResponsee.held != nil), title: appointmentResponsee.title!, created: created_at!, updated: updated_at!, user: appointmentResponsee.user!))
        
        
        
        self.filterItems()
        
        Toast.show(message: "Appointment is added to your calendar", controller: self)
        
    }
    
    
}
