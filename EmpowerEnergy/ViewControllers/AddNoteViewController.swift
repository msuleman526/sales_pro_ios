//
//  AddNoteViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 09/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit

protocol AddNoteDelegate {
    func addNote(notes: String)
}

class AddNoteViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var noteTxtField: UITextField!
    var addNoteDelegate: AddNoteDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Add New Note"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        noteTxtField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    @IBAction func saveClick(_ sender: Any) {
        self.view.endEditing(true)
        let note = self.noteTxtField.text as String?
        if(note == "")
        {
            Toast.show(message: "Please enter note", controller: self)
        }
        else{
            self.addNoteDelegate?.addNote(notes: note!)
        }
    }
    
    
    override func willMove(toParent parent: UIViewController?) {
           self.navigationController?.isNavigationBarHidden = true
       }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

}
