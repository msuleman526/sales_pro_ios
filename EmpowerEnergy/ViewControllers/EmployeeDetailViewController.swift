//
//  EmployeeDetailViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 25/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import MessageUI

protocol DeleteEmployeeDelegate {
    func deleteEmployee(userId: Int, index: Int)
}

class EmployeeDetailViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var officeLabewl: UILabel!
    var deleteEmployeeDelegate: DeleteEmployeeDelegate?
    var index: Int = 0
    @IBOutlet weak var managerLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordViewHeight: NSLayoutConstraint!
    @IBOutlet weak var trashImage: UIImageView!
    @IBOutlet weak var emailImage: UIImageView!
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var phoneImage: UIImageView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var emailView: UIView!
    //@IBOutlet weak var editView: UIView!
    
    var employee: Employee?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = (employee?.firstName!)!
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        setUpIcons()
        setUpExtraGestures()
        
        passwordView.isHidden = true
        passwordViewHeight.constant = 0
        passwordPadding.constant = 0
        loadInformation()
    }
    
    func loadInformation()
    {
        nameLabel.text = "\(employee?.firstName ?? "") \(employee?.lastName ?? "")"
        phoneLabel.text = employee?.phone ?? ""
        emailLabel.text = employee?.email ?? ""
        typeLabel.text = employee?.type ?? ""
        if employee?.parent == 0
        {
            managerLabel.text = "Not Assigned"
        }else{
            var parent = ""
            for i in (0..<(ViewController.interactionResponse?.users!.count)!)
            {
                if employee?.parent == ViewController.interactionResponse?.users![i].id
                {
                    parent = (ViewController.interactionResponse?.users![i].name)! as String
                }
            }
            
            if parent != ""
            {
                managerLabel.text = parent
            }
        }
        
        if(employee?.offices != nil && (employee?.offices?.count)! > 0)
        {
            officeLabewl.text = employee?.offices![0].name!
        }else{
            officeLabewl.text = "No Office"
        }
    }
    
    @IBOutlet weak var passwordPadding: NSLayoutConstraint!
    func setUpExtraGestures()
    {
        let callGesture = UITapGestureRecognizer(target: self, action: #selector(callAction))
        callView.addGestureRecognizer(callGesture)
        
        let messageGesture = UITapGestureRecognizer(target: self, action: #selector(messageAction))
        messageView.addGestureRecognizer(messageGesture)
        
        let emailGesture = UITapGestureRecognizer(target: self, action: #selector(emailAction))
        emailView.addGestureRecognizer(emailGesture)
        
        let deleteGesture = UITapGestureRecognizer(target: self, action: #selector(deleteAction))
        deleteView.addGestureRecognizer(deleteGesture)
        
    }
    
    func sendEmail() {
        if Constants.LOGGING_ENABLED{
            print("email started")}
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["\(employee!.email!)"])
            mail.setMessageBody("<p>Message from Empower Energy Solutions</p>", isHTML: true)
            present(mail, animated: true)
        } else {
            if Constants.LOGGING_ENABLED{
                print("Email Not Wroking")}
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @objc
    func callAction()
    {
        
        if(employee!.phone != nil || employee!.phone == "" )
        {
            call(number: employee!.phone!)
        }else
        {
            Toast.show(message: "No Phone Numbers", controller: self)
        }
    }
    
    @objc
    func messageAction()
    {
        if(employee?.phone != "")
        {
            UIApplication.shared.open(URL(string: "sms:\(employee!.phone!)")!, options: [:], completionHandler: nil)
        }
        else
        {
            Toast.show(message: "No number available", controller: self)
        }
        
    }
    
    func call(number: String)
    {
        if let phoneCallURL:URL = URL(string: "tel:\(number)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                let alertController = UIAlertController(title: "Connect with Customer", message: "Are you sure you want to call \n\(number)?", preferredStyle: .alert)
                let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                })
                let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
                    
                })
                alertController.addAction(yesPressed)
                alertController.addAction(noPressed)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @objc
    func emailAction()
    {
        sendEmail() 
    }
    
   
    @objc
    func deleteAction()
    {
        
        if employee!.type! == "Admin"
        {
            Toast.show(message: "Cannot Delete SuperAdmin", controller: self)
        }else
        {
        
        let refreshAlert = UIAlertController(title: "", message: "Are you sure to delete employee?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
            self.deleteEmployeeDelegate?.deleteEmployee(userId: self.employee!.id!, index: self.index)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        }
        
    }
    
    
    func setUpIcons()
    {
        self.phoneImage.layer.borderWidth = 1
        self.phoneImage.layer.borderColor = UIColor.white.cgColor
        self.phoneImage.layer.cornerRadius = self.phoneImage.frame.height/2
        
        self.messageImage.layer.borderWidth = 1
        self.messageImage.layer.borderColor = UIColor.white.cgColor
        self.messageImage.layer.cornerRadius = self.messageImage.frame.height/2
        
        self.emailImage.layer.borderWidth = 1
        self.emailImage.layer.borderColor = UIColor.white.cgColor
        self.emailImage.layer.cornerRadius = self.emailImage.frame.height/2
        
        //self.pencilImage.layer.borderWidth = 1
        //self.pencilImage.layer.borderColor = UIColor.white.cgColor
        //self.pencilImage.layer.cornerRadius = self.pencilImage.frame.height/2
        
        self.trashImage.layer.borderWidth = 1
        self.trashImage.layer.borderColor = UIColor.white.cgColor
        self.trashImage.layer.cornerRadius = self.trashImage.frame.height/2
    }
    
    
}
