//
//  ManageEmployeeViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 23/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ManageEmployeeViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var employeesTableView: UITableView!
    var employees: [Employee] = []
    var tempEmployeesList: [Employee] = []
    var alert: UIAlertController?
    
    @IBOutlet weak var searchTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.backItem?.title = "Back"
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        //Add Employees
        manageEmployees()
        
    }
    
    func manageEmployees()
    {
        employees = []
        tempEmployeesList = []
        for i in (0..<ViewController.interactionResponse!.users!.count)
        {
            let id = ViewController.interactionResponse!.users![i].id!
            let firstName = ViewController.interactionResponse!.users![i].first_name ?? ""
            let lastName = ViewController.interactionResponse!.users![i].last_name ?? ""
            let email = ViewController.interactionResponse!.users![i].email!
            let created = Date()
            let updated = Date()
            let type = ViewController.interactionResponse!.users![i].role ?? ViewController.interactionResponse?.roles![0]
            let companies = ViewController.interactionResponse!.users![i].companies!
            let parent = ViewController.interactionResponse!.users![i].parent_id!
            let phone = ViewController.interactionResponse!.users![i].phone ?? ""
            
            employees.append(Employee(id: id, firstName: firstName, lastName: lastName, email: email, created: created, modified: updated, type: type!.name!,offices: companies,parent: parent,phone: phone))
        }
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tempEmployeesList = employees
        manageSorting()
        self.searchTextField.delegate = self
        self.searchTextField.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        tempEmployeesList = []
        
        if textField.text == ""
        {
            tempEmployeesList = employees
        }else{
            for i in (0..<employees.count)
            {
                var name = "\(employees[i].firstName ?? "") \(employees[i].lastName ?? "")"
                name = name.lowercased()
                if name.contains(textField.text!.lowercased())
                {
                    tempEmployeesList.append(employees[i])
                }
            }
        }
        manageSorting()
    }
    
    func manageSorting()
    {
        //var sortedArray: [Employee] = []
        //sortedArray = employees.sorted(by: {$0.created!.compare($1.created!) == .orderedDescending})
        
        //employees = sortedArray
        employeesTableView.reloadData()
    }
    
    
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc
    func deleteClick(tapGesture: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
        let imageView = tapGesture.view as! UIImageView
        let index = imageView.tag
        let refreshAlert = UIAlertController(title: "", message: "Are you sure to delete employee?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
            self.deleteUser(userId: self.tempEmployeesList[index].id!, index: index)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func addNewEmployeeeClick(_ sender: Any) {
        self.view.endEditing(true)
        let customerViewController = UIStoryboard.init(name: "CreateEmployee", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateEmployeeViewController") as! CreateEmployeeViewController
        customerViewController.addEmployeeDelegate = self
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    @objc
    func editClick(tapGesture: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
        let imageView = tapGesture.view as! UIImageView
        let index = imageView.tag
        
        let customerViewController = UIStoryboard.init(name: "EditEmployee", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditEmployeeViewController") as! EditEmployeeViewController
        customerViewController.employee = self.tempEmployeesList[index]
        customerViewController.editEmployeeDelegate = self
        customerViewController.index = index
        self.navigationController?.pushViewController(customerViewController, animated: false);
        
    }
    
    
    func addUser(user: UsersResponse)
    {
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "name": user.name!,
            "first_name": user.first_name ?? "",
            "last_name": user.last_name ?? "",
            "email": user.email!,
            "password": user.password!,
            "office_id": user.office_id!,
            "parent_id": user.parent_id!,
            "status": "Active",
            "phone": user.phone!,
            "role_id": user.role!.id!,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/users";
        
        Alamofire.request(resourceString, method: .post, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            if Constants.LOGGING_ENABLED{
                                print(value)}
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            if Constants.LOGGING_ENABLED{
                                print("Hello0----")}
                            let res = try decoder.decode(UsersResponse2.self, from: jsonData!)
                            if Constants.LOGGING_ENABLED{
                                print("Hello0----")}
                            let formator = DateFormatter()
                            formator.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            
                            let id = res.id!
                            let full_name = res.name!
                            let firstname = res.first_name!
                            let lastname = res.last_name ?? ""
                            let email = res.email!
                            let created = formator.date(from: res.created_at!)!
                            let updated = formator.date(from: res.updated_at!)
                            let type = user.role!
                            
                            
                            ViewController.interactionResponse?.users?.append(UsersResponse(id: id, name: full_name,first_name: firstname,last_name: lastname, email: email, office_id: Int(res.office_id!)!, role: type, parent: Int(res.parent_id!)!, phone: res.phone ?? "",password: "", companies: res.companies!))
                            
                            self.employees.append(Employee(id: id, firstName: firstname,lastName: lastname, email: email, created: created, modified: updated!, type: type.name!,offices: res.companies!,parent:
                                Int(res.parent_id!)!,phone: res.phone ?? ""))
                            
                            self.tempEmployeesList = self.employees
                            self.searchTextField.text = ""
                            
                            if Constants.LOGGING_ENABLED{
                                print("Hello0----")}
                            self.manageSorting()
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 401{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Server Error / Email Already Taken", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    func editUserWithoutPassword(user: UsersResponse,index: Int)
    {
        
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "name": user.name!,
            "first_name": user.first_name ?? "",
            "last_name": user.last_name ?? "",
            "email": user.email!,
            "office_id": user.office_id!,
            "parent_id": user.parent_id!,
            "phone": user.phone!,
            "role_id": user.role!.id!,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/users/\(user.id!)";
        
        Alamofire.request(resourceString, method: .put, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode(UsersResponse2.self, from: jsonData!)
                            let formator = DateFormatter()
                            formator.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            
                            let id = res.id!
                            let fullname = res.name!
                            let firstname = res.first_name!
                            let lastname = res.last_name ?? ""
                            let email = res.email!
                            let created = formator.date(from: res.created_at!)!
                            let updated = formator.date(from: res.updated_at!)
                            let type = user.role!
                            
                            if Constants.LOGGING_ENABLED{
                                print("Phone Number: \(res.phone!)")}
                            ViewController.interactionResponse?.users![index] = UsersResponse(id: id, name: fullname,first_name: firstname,last_name: lastname, email: email, office_id: Int(res.office_id!)!, role: type, parent: Int(res.parent_id!)!, phone: res.phone ?? "",password: "", companies: res.companies!)
                            
                            for i in (0..<self.employees.count)
                            {
                                if(self.employees[i].id! == id)
                                {
                                    self.employees[i] = Employee(id: id, firstName: firstname,lastName: lastname, email: email, created: created, modified: updated!, type: type.name!,offices: res.companies!,parent:
                                    Int(res.parent_id!)!,phone: res.phone ?? "")
                                }
                            }
                        
                            self.tempEmployeesList[index] = Employee(id: id, firstName: firstname,lastName: lastname, email: email, created: created, modified: updated!, type: type.name!,offices: res.companies!,parent:
                                                           Int(res.parent_id!)!,phone: res.phone ?? "")
                            
                            
                            self.manageSorting()
                            Toast.show(message: "User updated successully", controller: self)
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 401{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Server Error.", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    func editUserWithPassword(user: UsersResponse,index: Int)
       {
           
           present(alert!, animated: false, completion: nil)
           let parameters =  [
               "name": user.name!,
               "first_name": user.first_name ?? "",
               "last_name": user.last_name ?? "",
               "password": user.password!,
               "email": user.email!,
               "office_id": user.office_id!,
               "parent_id": user.parent_id!,
               "phone": user.phone!,
               "role_id": user.role!.id!,
               ] as [String : Any]
           
           let header = [
               "Accept": "text/json",
               "Authorization": "bearer \(SessionUtils.getToken())"
           ]
           
           let resourceString = "\(Utils.requestURL)api/users/\(user.id!)";
           
           Alamofire.request(resourceString, method: .put, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
               switch response.result {
               case .success(let value):
                   if let httpURLResponse = response.response{
                       let response_code = httpURLResponse.statusCode;
                       if response_code == 200{
                           do{
                               self.alert?.dismiss(animated: false, completion: nil)
                               let json = JSON(value)
                               let str = String(describing: json);
                               let jsonData = str.data(using: .utf8)
                               let decoder = JSONDecoder();
                               let res = try decoder.decode(UsersResponse2.self, from: jsonData!)
                               let formator = DateFormatter()
                               formator.dateFormat = "yyyy-MM-dd HH:mm:ss"
                               
                               let id = res.id!
                               let first_name = res.first_name!
                               let last_name = res.last_name ?? ""
                               let email = res.email!
                               let created = formator.date(from: res.created_at!)!
                               let updated = formator.date(from: res.updated_at!)
                               let type = user.role!
                               
                            if Constants.LOGGING_ENABLED{
                                print("Phone Number: \(res.phone!)")}
                               ViewController.interactionResponse?.users![index] = UsersResponse(id: id, name: first_name,first_name: first_name, last_name: last_name, email: email, office_id: Int(res.office_id!)!, role: type, parent: Int(res.parent_id!)!, phone: res.phone ?? "",password: "", companies: res.companies!)
                            
                               for i in (0..<self.employees.count)
                               {
                                   if(self.employees[i].id! == id)
                                   {
                                       self.employees[i] = Employee(id: id, firstName: first_name,lastName: last_name, email: email, created: created, modified: updated!, type: type.name!,offices: res.companies!,parent:
                                       Int(res.parent_id!)!,phone: res.phone ?? "")
                                   }
                               }
                               
                               self.tempEmployeesList[index] = Employee(id: id, firstName: first_name,lastName: last_name, email: email, created: created, modified: updated!, type: type.name!,offices: res.companies!,parent:
                                   Int(res.parent_id!)!,phone: res.phone ?? "")
                               
                               
                               self.manageSorting()
                               Toast.show(message: "User updated successully", controller: self)
                               
                           }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                           
                           
                       }
                       else if response_code == 401{
                           self.alert?.dismiss(animated: false, completion: nil)
                           Toast.show(message: "Unauthorized User", controller: self)
                           let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                           self.navigationController?.pushViewController(viewController!, animated: false);
                           //self.present(viewController!,animated: true,completion: nil)
                           
                       }
                       else{
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                           self.alert?.dismiss(animated: false, completion: nil)
                           Toast.show(message: "Employee already exists / Server Error.", controller: self)
                           
                       }
                   }
               case .failure(let error):
                   self.alert?.dismiss(animated: false, completion: nil)
                   if Constants.LOGGING_ENABLED{
                    print(error)}
                   Toast.show(message: "No Internet Connection / Server Error", controller: self)
               }
               
           })
       }
    
    
    func deleteUser(userId: Int, index: Int)
    {
        
        present(alert!, animated: false, completion: nil)
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/users/\(userId)";
        
        Alamofire.request(resourceString, method: .delete, parameters: nil, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        if Constants.LOGGING_ENABLED{
                            print(value)}
//                        do{
//                            self.alert?.dismiss(animated: false, completion: nil)
//                            let json = JSON(value)
//                            let str = String(describing: json);
//                            let jsonData = str.data(using: .utf8)
//                            let decoder = JSONDecoder();
//                            let res = try decoder.decode(DeleteUserResponse.self, from: jsonData!)
                            self.alert?.dismiss(animated: false, completion: nil)
                            /*self.tempEmployeesList.remove(at: index)
                            for i in (0..<self.employees.count)
                            {
                                if(self.employees[i].id == userId)
                                {
                                     self.employees.remove(at: i)
                                }
                            }
                            self.tempEmployeesList = self.employees
                            self.searchTextField.text = ""*/
                            for i in (0..<ViewController.interactionResponse!.users!.count)
                            {
                                if userId == ViewController.interactionResponse?.users![i].id!
                                {
                                    ViewController.interactionResponse?.users?.remove(at: i)
                                    break
                                }
                            }
                            
                            Toast.show(message: "Deleted successfully", controller: self)
                        self.manageEmployees()
                            
                            
                        //}catch{print(error)}
                        
                        
                    }
                    else if response_code == 401{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unable to Delete User / Server Error.", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    
    
    
}

extension ManageEmployeeViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempEmployeesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = employeesTableView.dequeueReusableCell(withIdentifier: "employee", for: indexPath) as! EmployeeDetailCell
        
        cell.nameLabel.text = "\(tempEmployeesList[indexPath.row].firstName ?? "") \(tempEmployeesList[indexPath.row].lastName ?? "")"
        cell.editImage.tag = indexPath.row
        cell.deleteImage.tag = indexPath.row
        
        cell.editImage.isUserInteractionEnabled = true
        let imageGesture = UITapGestureRecognizer(target: self, action: #selector(editClick(tapGesture:)))
        imageGesture.numberOfTapsRequired = 1
        cell.editImage.addGestureRecognizer(imageGesture)
        
        cell.deleteImage.isUserInteractionEnabled = true
        let imageGesture1 = UITapGestureRecognizer(target: self, action: #selector(deleteClick(tapGesture:)))
        imageGesture1.numberOfTapsRequired = 1
        cell.deleteImage.addGestureRecognizer(imageGesture1)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let customerViewController = UIStoryboard.init(name: "EmployeeDetail", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmployeeDetailViewController") as! EmployeeDetailViewController
        customerViewController.employee = self.tempEmployeesList[indexPath.row]
        customerViewController.deleteEmployeeDelegate = self
        customerViewController.index = indexPath.row
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    
}

extension ManageEmployeeViewController: AddEmployeeUserDelegate
{
    func addEmployee(user: UsersResponse)
    {
        self.navigationController?.popViewController(animated: true)
        addUser(user: user)
    }
}

extension ManageEmployeeViewController: DeleteEmployeeDelegate
{
    func deleteEmployee(userId: Int, index: Int) {
        self.navigationController?.popViewController(animated: true)
        deleteUser(userId: userId, index: index)
    }
    
    
}

extension ManageEmployeeViewController: EditEmployeeUserDelegate
{
    func editEmployee(user: UsersResponse, index: Int) {
        self.navigationController?.popViewController(animated: true)
        if(user.password == "")
        {
            if Constants.LOGGING_ENABLED{
                print("No Password Change")}
            editUserWithoutPassword(user: user, index: index)
        }else
        {
            if Constants.LOGGING_ENABLED{
            print("Password Changes")
            print("\(user.password!)")
            print(index)
            }
            editUserWithPassword(user: user, index: index)
        }
    }
    
    
}

class EmployeeDetailCell: UITableViewCell
{
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deleteImage: UIImageView!
    @IBOutlet weak var editImage: UIImageView!
}
