//
//  LoginViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 18/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class LoginViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var forgotPasswordLbl: UITextView!
    @IBOutlet weak var passwordEditTxt: TextField!
    var alert : UIAlertController?
    @IBOutlet weak var emailEditTxt: TextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        self.passwordEditTxt.delegate = self
        self.emailEditTxt.delegate = self
        
        let forgotPasswordGesture = UITapGestureRecognizer(target: self, action: #selector(openForgotPasswored))
        forgotPasswordLbl.addGestureRecognizer(forgotPasswordGesture)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    @objc
    func openForgotPasswored()
    {
        let customerViewController = UIStoryboard.init(name: "ForgotPassword", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    @IBAction func loginActionPerform(_ sender: Any) {
        self.view.endEditing(true)
        let emailAddress = emailEditTxt.text!;
        let passwordStr = passwordEditTxt.text!;
        if(emailAddress=="" || passwordStr=="")
        {
            Toast.show(message: "Please Fill Form Data", controller: self)
        }else
        {
            performLogin(email: emailAddress, password: passwordStr)
        }
    }
    
    func performLogin(email: String, password: String)
    {
        present(alert!, animated: false, completion: nil)
        let parametersLogin =  [
            "email": email,
            "password": password
        ]
        
        let header = [
            "Accept": "text/json"
        ]
        
        let resourceString = "\(Utils.requestURL)api/auth/login";
        
        Alamofire.request(resourceString, method: .post, parameters: parametersLogin, headers: header).responseString(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            if Constants.LOGGING_ENABLED{
                                print(value)}
                            let res = try decoder.decode(UsersResponse.self, from: jsonData!)
                            
                            SessionUtils.setLoginData(userResponse: res)
                            self.getInteractionsWithUsers()
                        
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else{
                         //print(response.result.value)
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        self.alert?.dismiss(animated: false, completion: nil)
                        if(response.result.value!.contains("Unauthorized"))
                        {
                            Toast.show(message: "Invalid Credentials", controller: self)
                        }else if(response.result.value!.contains("User Not Found"))
                        {
                            Toast.show(message: "User Not Found", controller: self)
                        }else{
                            Toast.show(message: "No Internet Connection/ Server Issue", controller: self)
                        }
                        
                    }
                }
            case .failure(let error):
                //print(response.result.value)
                self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
            }
            
        })
        
        
        
        
        
    }
    
    func getInteractionsWithUsers()
    {
        do{
            let API_TOKEN = SessionUtils.getToken()
            let header = [
                "Authorization": "bearer \(API_TOKEN)"
            ]
            let resourceString = "\(Utils.requestURL)api/interactions";
            Alamofire.request(resourceString, method: .get, parameters: nil, headers: header).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    if let httpURLResponse = response.response{
                        let response_code = httpURLResponse.statusCode;
                        if response_code == 200 {
                            do{
                                
                                self.alert?.dismiss(animated: false, completion: nil)
                                let json = JSON(value)
                                let str = String(describing: json);
                                let jsonData = str.data(using: .utf8)
                                let decoder = JSONDecoder();
                                let res = try decoder.decode(InteractionsUserResponse.self, from: jsonData!)
                                
                                ViewController.interactionResponse = res;
                                let loginViewController = UIStoryboard.init(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "Home") as? HomeViewController
                                
                                self.navigationController?.pushViewController(loginViewController!, animated: false);
                                //self.present(loginViewController!,animated: true,completion: nil)
                                
                                //self.navigationController?.popToRootViewController(animated: false)
                                
                                
                                
                            }catch{
                                if Constants.LOGGING_ENABLED{
                                    print(error)}
                                Toast.show(message: "Server Issue/User not exist", controller: self)
                            }
                            
                            
                        }else if response_code == 401{
                            self.alert?.dismiss(animated: false, completion: nil)
                            Toast.show(message: "Unauthorized User", controller: self)
                            let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            self.navigationController?.pushViewController(viewController!, animated: false);
                            //self.present(viewController!,animated: true,completion: nil)
                            
                        }else{
                            if Constants.LOGGING_ENABLED{
                            print(response.result.value!)
                            }
                            self.alert?.dismiss(animated: false, completion: nil)
                            Toast.show(message: "Server Issue/User not exist", controller: self)
                        }
                    }
                case .failure(let error):
                    self.alert?.dismiss(animated: false, completion: nil)
                    Toast.show(message: "No Internet Connection/Server Issue", controller: self)
                    if Constants.LOGGING_ENABLED{
                    print(error)
                    }
                    
                }
                
            })
            
            
            
        }
        
        
        
        
    }
}
