//
//  AddNoteViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 09/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import EventKit
import SwiftyJSON
import Alamofire

protocol AddAppointmentDelegate {
    func addAppointment(appointmentResponsee: AppointmentResponse)
}


class AddAppointmentViewController: UIViewController, UITextFieldDelegate {
    
    var addAppointmentDelegate: AddAppointmentDelegate?
    
    var locationStr: String?
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    var address_id: Int?
    
    var datePicker = UIDatePicker()
    var startTimePicker = UIDatePicker()
    var endTimePicker = UIDatePicker()
    
    @IBOutlet weak var allDaySwitch: UISwitch!
    
    @IBOutlet weak var endTimeField: UITextField!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var startTimeField: UITextField!
    
    var alert: UIAlertController?
    
    var startTime: Date?
    var endTime: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        createDatePicker()
        createStartTimePicker()
        createEndTimePicker()
        
        let formattor = DateFormatter()
        formattor.dateFormat = "HH:mm:ss"
        
        startTime = formattor.date(from: "03:00:00")
        endTime = formattor.date(from: "04:00:00")
        
        self.addressField.text = locationStr
        if locationStr == ""{
            self.addressField.isUserInteractionEnabled = true
        }
        
        allDaySwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        
        self.addressField.delegate = self
        self.dateField.delegate = self
        self.descriptionTextField.delegate = self
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        
        if value
        {
            startTimeField.text = "00:00:00"
            endTimeField.text = "23:59:59"
        }
        
    }
    
    func createDatePicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(datePickerDoneClick))
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelEditing))
        
        toolbar.setItems([doneBtn,cancelBtn], animated: true)
        
        dateField.inputAccessoryView = toolbar
        
        dateField.inputView = datePicker
        
        datePicker.datePickerMode = .date
    }
    
    func createStartTimePicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(startTimePickerDoneClick))
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelEditing))
        
        toolbar.setItems([doneBtn,cancelBtn], animated: true)
        
        startTimeField.inputAccessoryView = toolbar
        
        startTimeField.inputView = startTimePicker
        
        startTimePicker.datePickerMode = .time
    }
    
    func createEndTimePicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(endTimePickerDoneClick))
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelEditing))
        
        toolbar.setItems([doneBtn,cancelBtn], animated: true)
        
        endTimeField.inputAccessoryView = toolbar
        
        endTimeField.inputView = endTimePicker
        
        endTimePicker.datePickerMode = .time
    }
    
    @objc
    func datePickerDoneClick()
    {
        let formattor =  DateFormatter();
        formattor.dateFormat = "yyyy-MM-dd"
        
        dateField.text = formattor.string(from: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    @objc
    func cancelEditing()
    {
        self.view.endEditing(true)
    }
    
    @objc
    func startTimePickerDoneClick()
    {
        let formattor =  DateFormatter();
        formattor.dateFormat = "HH:mm:ss"
        
        if(allDaySwitch.isOn)
        {
            Toast.show(message: "Please disable all day switch", controller: self)
        }else{
            startTime = startTimePicker.date
            if(startTime! > endTime!)
            {
                Toast.show(message: "Start time cannot greater then end time", controller: self)
            }else{
            startTimeField.text = formattor.string(from: startTimePicker.date)
            }
        }
        
        self.view.endEditing(true)
    }
    
    @objc
    func endTimePickerDoneClick()
    {
        let formattor =  DateFormatter();
        formattor.dateFormat = "HH:mm:ss"
        
        if(allDaySwitch.isOn)
        {
            Toast.show(message: "Please disable all day switch", controller: self)
        }else{
            endTime = endTimePicker.date
            if(endTime! < startTime!)
            {
                Toast.show(message: "End time cannot smaller then start time", controller: self)
            }else{
            endTimeField.text = formattor.string(from: endTimePicker.date)
            }
        }
        
        self.view.endEditing(true)
    }
    
    
    @IBAction func addEventClick(_ sender: Any) {
        self.view.endEditing(true)
        let title = titleField.text!
        let description = descriptionTextField.text!
        let startDate = "\(dateField.text!) \(startTimeField.text!)"
        let endDate = "\(dateField.text!) \(endTimeField.text!)"
        
        if(title == "")
        {
            Toast.show(message: "Please enter title", controller: self)
        }
        else if(description == "")
        {
            Toast.show(message: "Please enter description", controller: self)
        }
        else
        {
            present(alert!, animated: false, completion: nil)
            let parameters =  [
                "title": title,
                "appointment_at": startDate,
                "appointment_end_at": endDate,
                "snooze_time": 30,
                "held": 0,
                "description": description,
                ] as [String : Any]
            
            let header = [
                "Accept": "text/json",
                "Authorization": "bearer \(SessionUtils.getToken())"
            ]
            
            let resourceString = "\(Utils.requestURL)api/address/appointment/\(address_id!)";
            
            Alamofire.request(resourceString, method: .post, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    if let httpURLResponse = response.response{
                        let response_code = httpURLResponse.statusCode;
                        if response_code == 200{
                            do{
                                self.alert?.dismiss(animated: false, completion: nil)
                                let json = JSON(value)
                                let str = String(describing: json);
                                let jsonData = str.data(using: .utf8)
                                let decoder = JSONDecoder();
                                let res = try decoder.decode(AppointmentResponse.self, from: jsonData!)
                                
                                let formattor =  DateFormatter();
                                formattor.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                if Constants.LOGGING_ENABLED{
                                print("Title - \(res.title!)")
                                }
                                
                                self.insertEventIntoCalendar(title: res.title!, startDate: formattor.date(from: res.appointment_at!)!, endDate: formattor.date(from: res.appointment_end_at!)!, description: res.description!, location: (res.address?.address) ?? "")
                                
                                self.addAppointmentDelegate?.addAppointment(appointmentResponsee: res)
                                
                            }catch{
                                if Constants.LOGGING_ENABLED{
                                    print(error)}}
                            
                            
                        }
                        else if response_code == 401{
                            self.alert?.dismiss(animated: false, completion: nil)
                            Toast.show(message: "Unauthorized User", controller: self)
                            let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            self.navigationController?.pushViewController(viewController!, animated: false);
                            //self.present(viewController!,animated: true,completion: nil)
                            
                        }
                        else{
                            
                            self.alert?.dismiss(animated: false, completion: nil)
                            Toast.show(message: "Server Error.", controller: self)
                            
                        }
                    }
                case .failure(let error):
                    self.alert?.dismiss(animated: false, completion: nil)
                    if Constants.LOGGING_ENABLED{
                     print(error)
                    }
                    Toast.show(message: "No Internet Connection / Server Error", controller: self)
                }
                
            })
        }
        
        
        
        
    }
    
    func insertEventIntoCalendar(title: String,startDate: Date,endDate: Date,description: String,location: String)
    {
        let eventStore: EKEventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: {(granted, error) in
            
            if granted && error == nil
            {
                if Constants.LOGGING_ENABLED{
                  print("working")
                }
                let event: EKEvent = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate
                event.endDate = endDate
                event.notes = description
                event.location = location
                event.calendar = eventStore.defaultCalendarForNewEvents
                
                do{
                    try eventStore.save(event, span: .thisEvent)
                }catch let error as NSError
                {
                    if Constants.LOGGING_ENABLED{
                        print(error)}
                }
            }else
            {
                if Constants.LOGGING_ENABLED{
                    print("Not workinggg")}
            }
            
            
        })
        
    }
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
}
