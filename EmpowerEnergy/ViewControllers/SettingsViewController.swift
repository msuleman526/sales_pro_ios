//
//  SettingsViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 27/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import GoogleMaps
import GoogleMapsUtils

protocol ChangeMapDelegate
{
    func changeMap()
}

class SettingsViewController: UIViewController {
    
    var changeMapDelegate: ChangeMapDelegate?
    
    var mapTypesList: [String] = []
    @IBOutlet weak var mapTypeView: UIView!
   
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet var popup: UIView!
    
    @IBOutlet weak var popupTable: UITableView!
    @IBOutlet weak var popUpBackgroundView: UIView!
    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Settings"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.backItem?.title = "Back"
        
        popUpBackgroundView.isHidden = true
        popup.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        
        if SessionUtils.getMapType() == 1  || SessionUtils.getMapType() == 0 {
            label.text = "Normal"
        }
        else if SessionUtils.getMapType() == 2{
            label.text = "Satellite"
        }
        else if SessionUtils.getMapType() == 3 {
            label.text = "Terrain"
        }
        else if SessionUtils.getMapType() == 4 {
            label.text = "Hybrid"
        }
        else{
            label.text = "Hybrid"
        }
        
        mapTypesList.append("Normal")
        mapTypesList.append("Setellite")
        mapTypesList.append("Terrain")
        mapTypesList.append("Hybrid")
        
        let dissmissPopupGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup))
        popUpBackgroundView.addGestureRecognizer(dissmissPopupGesture)
        
        let mapViewClickGeture = UITapGestureRecognizer(target: self, action: #selector(mapViewClick))
        mapTypeView.addGestureRecognizer(mapViewClickGeture)
        
    }
    
    @objc
    func dismissPopup()
    {
        self.popup.removeFromSuperview()
        self.popUpBackgroundView.isHidden = true
    }
    
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc
    func mapViewClick()
    {
        popupTable.reloadData()
        viewWillLayoutSubviews()
        self.view.addSubview(popup)
        self.popUpBackgroundView.isHidden = false
        viewWillLayoutSubviews()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tableHeight.constant
            = self.popupTable.contentSize.height
        
        self.popup.frame.size.height =  self.tableHeight.constant + 10
    }
    
    
    
    
    
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        mapTypesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = popupTable.dequeueReusableCell(withIdentifier: "popup", for: indexPath) as! SettingPopupCell
        cell.label.text = mapTypesList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if mapTypesList[indexPath.row] == "Normal"
        {
            SessionUtils.setMapType(value: Int(GMSMapViewType.normal.rawValue))
            label.text = "Normal"
        }
        else if mapTypesList[indexPath.row] == "Setellite"
        {
            SessionUtils.setMapType(value: Int(GMSMapViewType.satellite.rawValue))
            label.text = "Setellite"
        }else if mapTypesList[indexPath.row] == "Terrain"
        {
            SessionUtils.setMapType(value: Int(GMSMapViewType.terrain.rawValue))
            label.text = "Terrain"
        }else if mapTypesList[indexPath.row] == "Hybrid"
        {
            SessionUtils.setMapType(value: Int(GMSMapViewType.hybrid.rawValue))
            label.text = "Hybrid"
        }
        
        dismissPopup()
        self.changeMapDelegate?.changeMap()
        
        
    }
    
    
}

class SettingPopupCell: UITableViewCell
{
    
    @IBOutlet weak var label: UILabel!
}
