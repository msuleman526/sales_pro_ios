//
//  AddEmployeeViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 30/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit

protocol AddEmployeeDelegate {
    func addEmployees(employees: [Employee])
}

class AddEmployeeViewController: UIViewController {
    
    var addEmployeeDelegate: AddEmployeeDelegate?
    
    @IBOutlet weak var employeesTableView: UITableView!
    var employees: [Employee] = []
    var areaEmployees: [Employee] = []
    
    var checkList: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Manage Area Users"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        
        let formator = DateFormatter()
        formator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        for i in (0..<ViewController.interactionResponse!.users!.count)
        {
            let id = ViewController.interactionResponse!.users![i].id!
            let firstName = ViewController.interactionResponse!.users![i].first_name ?? ""
            let lastName = ViewController.interactionResponse!.users![i].last_name ?? ""
            let email = ViewController.interactionResponse!.users![i].email!
            let created = Date()
            let updated = Date()
            let type = ViewController.interactionResponse!.users![i].role!
            
            employees.append(Employee(id: id, firstName: firstName, lastName: lastName, email: email, created: created, modified: updated, type: type.name!,offices: ViewController.interactionResponse!.users![i].companies!,parent: ViewController.interactionResponse!.users![i].parent_id!,phone: ViewController.interactionResponse!.users![i].phone ?? ""))
            
            for j in (0..<areaEmployees.count)
            {
                if areaEmployees[j].id == id
                {
                    checkList.append(i)
                }
            }
        }
    }
    @IBAction func addEmployeeClick(_ sender: Any) {
        
        var finalEmployees: [Employee] = []
        
        if(checkList.count<1)
        {
            Toast.show(message: "Please choose atleast one", controller: self)
        }
        else{
        
        for i in (0..<checkList.count)
        {
            finalEmployees.append(employees[checkList[i]])
            if Constants.LOGGING_ENABLED{
                print("Employee Name: - \(employees[i].firstName!)")}
        }
        
        addEmployeeDelegate?.addEmployees(employees: finalEmployees)
        }
    }
    
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    
    @objc
    func imageClick(tapGesture: UITapGestureRecognizer)
    {
        let imageView = tapGesture.view as! UIImageView
        let index = imageView.tag
        let indexPath = IndexPath(row: index, section: 0)
        let cell = self.employeesTableView.cellForRow(at: indexPath) as! AddEmployeeCell
        
        if(cell.checkBoxImage.tintColor == UIColor.systemGray2)
        {
            cell.checkBoxImage.tintColor = UIColor(named: "mainBlue")
            cell.checkBoxImage.image = UIImage(systemName: "checkmark.circle.fill")
            onEmployeeCheckClick(position: index, isChecked: true)
            
        }else
        {
            cell.checkBoxImage.tintColor = UIColor.systemGray2
            cell.checkBoxImage.image = UIImage(systemName: "circle")
            onEmployeeCheckClick(position: index, isChecked: false)
        }
        
    }
    
    func onEmployeeCheckClick(position: Int, isChecked: Bool)
    {
        
        var checkTempList: [Int] = checkList
        if isChecked
        {
            checkTempList.append(position)
        }
        else
        {
            for i in (0..<checkList.count)
            {
                if(checkList[i] == position)
                {
                    checkTempList.remove(at: i)
                }
            }
            
        }
        checkList = checkTempList
        
        for i in (0..<checkList.count)
        {
            if Constants.LOGGING_ENABLED{
                print("\(checkList[i])")}
        }
        
        
    }
    
    
    
}

extension AddEmployeeViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employees.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = employeesTableView.dequeueReusableCell(withIdentifier: "addemployee") as! AddEmployeeCell
        cell.checkBoxImage.tag = indexPath.row
        cell.checkBoxImage.isUserInteractionEnabled = true
        let imageGesture = UITapGestureRecognizer(target: self, action: #selector(imageClick(tapGesture:)))
        imageGesture.numberOfTapsRequired = 1
        cell.checkBoxImage.addGestureRecognizer(imageGesture)
        
        cell.employeeNameLabel.text = "\(employees[indexPath.row].firstName ?? "") \(employees[indexPath.row].lastName ?? "")"
        
        for i in (0..<checkList.count)
        {
            if(checkList[i] == indexPath.row)
            {
                cell.checkBoxImage.tintColor = UIColor(named: "mainBlue")
                cell.checkBoxImage.image = UIImage(systemName: "checkmark.circle.fill")
                
            }
        }
        
        return cell;
        
        
        
    }

    
    
    
    
    
}

class AddEmployeeCell: UITableViewCell
{
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var checkBoxImage: UIImageView!
    
   
}
