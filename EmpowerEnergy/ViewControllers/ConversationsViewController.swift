//
//  AddNoteViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 09/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ConversationsViewController: UIViewController {
    
    var dateFilters: DateFilter = DateFilter()
    var index:Int = 0;
    
    var selectedDateFilterIndex = 0
    
    @IBOutlet weak var conversationTable: UITableView!
    @IBOutlet weak var dateFilterCollectionView: UICollectionView!
    var interactions: [Interaction] = []
    var alert: UIAlertController?
    
    
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        //dateFilterCollectionView.collectionViewLayout = layout
        
        dateFilters.filters = []
        dateFilters.filters?.append(Today())
        dateFilters.filters?.append(Yesterday())
        dateFilters.filters?.append(ThisWeek())
        dateFilters.filters?.append(LastWeek())
        dateFilters.filters?.append(ThisMonth())
        dateFilters.filters?.append(LastMonth())
        dateFilters.filters?.append(ThisYear())
        dateFilters.filters?.append(LastYear())
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Conversations"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.dateFilterCollectionView.reloadData()
        
        self.topImage.layer.borderWidth = 1
        self.topImage.layer.borderColor = UIColor(named: "mainBlue")?.cgColor
        self.topImage.layer.backgroundColor = UIColor(named: "mainBlue")?.cgColor
        self.topImage.layer.cornerRadius = self.topImage.frame.height/2
        
        getConverstation(startDate: Today.getStartDate(), endDate: Today.getEndDate())
        
    }
    
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    func getConverstation(startDate: String,endDate: String)
    {
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "start": startDate,
            "end": endDate
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/interactions/by-user";
        
        Alamofire.request(resourceString, method: .get, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            if Constants.LOGGING_ENABLED{
                                print("Result - \(value)")}
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode([Interaction].self, from: jsonData!)
                            self.interactions = res
                            //print("Size \(self.interactions.count)")
                            var count = 0;
                            for i in (0..<self.interactions.count)
                            {
                                //print("Size \(self.interactions[i].log_count)")
                                count = count + self.interactions[i].log_count!
                            }
                            //print("Size \(count)")
                            self.topLabel.text = "\(count) interactions"
                            self.conversationTable.reloadData()
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 422 {
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Given Data is Invalid", controller: self)
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                        print (response_code)
                        print("Error is \(response.result.value!)")
                        }
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
            }
            
        })
    }
    
}

extension ConversationsViewController: UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = dateFilterCollectionView.dequeueReusableCell(withReuseIdentifier: "dateFilter", for: indexPath) as! DateFilterCell
        
        if(indexPath.row == selectedDateFilterIndex)
        {
            cell.view.backgroundColor = UIColor(named: "blueDarkBlue")
            cell.label.textColor = UIColor.white
        }else
        {
            cell.view.backgroundColor = UIColor.systemGray5
            cell.label.textColor = UIColor(named: "textcolor3")
        }
        cell.view.layer.cornerRadius = 15
        cell.label
            .text = dateFilters.filters![indexPath.row].name
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dateFilters.filters!.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        selectedDateFilterIndex = indexPath.row
        let range = Range(uncheckedBounds: (0, collectionView.numberOfSections))
        let indexSet = IndexSet(integersIn: range)
        self.dateFilterCollectionView.reloadSections(indexSet)
        if(indexPath.row == 0)
        {
            let startDate = Today.getStartDate()
            let endDate = Today.getEndDate()
            if Constants.LOGGING_ENABLED{
            print("Start Date - \(startDate)")
                print("End Date - \(endDate)")}
            getConverstation(startDate: startDate, endDate: endDate)
        }
        else if(indexPath.row == 1)
        {
            
            let startDate = Yesterday.getStartDate()
            let endDate = Yesterday.getEndDate()
            if Constants.LOGGING_ENABLED{
            print("Start Date - \(startDate)")
                print("End Date - \(endDate)")}
            getConverstation(startDate: startDate, endDate: endDate)
            
        }
            
        else if(indexPath.row == 2)
        {
            
            let startDate = ThisWeek.getStartDate()
            let endDate = ThisWeek.getEndDate()
            if Constants.LOGGING_ENABLED{
            print("Start Date - \(startDate)")
                print("End Date - \(endDate)")}
            getConverstation(startDate: startDate, endDate: endDate)
            
        }
            
        else if(indexPath.row == 3)
        {
            
            let startDate = LastWeek.getStartDate()
            let endDate = LastWeek.getEndDate()
            if Constants.LOGGING_ENABLED{
            print("Start Date - \(startDate)")
                print("End Date - \(endDate)")}
            getConverstation(startDate: startDate, endDate: endDate)
            
        }
            
        else if(indexPath.row == 4)
        {
            
            let startDate = ThisMonth.getStartDate()
            let endDate = ThisMonth.getEndDate()
            if Constants.LOGGING_ENABLED{
            print("Start Date - \(startDate)")
            print("End Date - \(endDate)")
            }
            getConverstation(startDate: startDate, endDate: endDate)
            
        }
            
        else if(indexPath.row == 5)
        {
            
            let startDate = LastMonth.getStartDate()
            let endDate = LastMonth.getEndDate()
            if Constants.LOGGING_ENABLED{
            print("Start Date - \(startDate)")
            print("End Date - \(endDate)")
            }
            getConverstation(startDate: startDate, endDate: endDate)
            
        }
        
        else if(indexPath.row == 6)
        {
            
            let startDate = ThisYear.getStartDate()
            let endDate = ThisYear.getEndDate()
            if Constants.LOGGING_ENABLED{
            print("Start Date - \(startDate)")
                print("End Date - \(endDate)")}
            getConverstation(startDate: startDate, endDate: endDate)
            
        }
        
        else if(indexPath.row == 7)
        {
            
            let startDate = LastYear.getStartDate()
            let endDate = LastYear.getEndDate()
            if Constants.LOGGING_ENABLED{
            print("Start Date - \(startDate)")
                print("End Date - \(endDate)")}
            getConverstation(startDate: startDate, endDate: endDate)
            
        }
    }
    
    
    
    
}



extension ConversationsViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = conversationTable.dequeueReusableCell(withIdentifier: "conversationInteraction", for: indexPath) as? ConversationCell
        
        let color = self.interactions[indexPath.row].color!
        let icon = self.interactions[indexPath.row].icon!
        let tmpImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: color))
        cell?.conversationImage.image = Utils.textToImage(drawText: icon as NSString, inImage: tmpImage)
        
        cell?.conversationLabel.text = "\(interactions[indexPath.row].log_count!) \(interactions[indexPath.row].name!)"
        cell?.conversationLabel.textColor = UIColor(hexString: color)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        self.viewWillLayoutSubviews()
    }
    
}



class DateFilterCell: UICollectionViewCell
{
    //@IBOutlet weak var dateFilterBtn: UILabel!
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var label: UILabel!
}

class ConversationCell: UITableViewCell
{
    
    @IBOutlet weak var conversationLabel: UILabel!
    @IBOutlet weak var conversationImage: UIImageView!
}
