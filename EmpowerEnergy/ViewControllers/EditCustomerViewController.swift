//
//  CreateCustomerViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 04/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit

protocol EditCustomerDelegate {
    func editCustomer(customer: Customer)
}

protocol EditCustomerDelegatee {
    func editCustomer(index: Int, customer: Customer,clusterIndex: Int)
}


class EditCustomerViewController: UIViewController, UITextFieldDelegate {
    
    var editCustomerDelegate: EditCustomerDelegatee?
    
    @IBOutlet weak var genderTextField: DropDown!
    //BarItems
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    
    //Popup Veriables
    @IBOutlet weak var poupTitle: UILabel!
    
    @IBOutlet weak var dropdownTable: UITableView!
    
    @IBOutlet weak var ageTextField: UITextField!
  
    
    @IBOutlet weak var netWorthTextField: UITextField!
    @IBOutlet weak var incomeTextField: UITextField!
    @IBOutlet weak var creditRatingTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    //Text Variables
    @IBOutlet weak var firstNameEditText: UITextField!
    @IBOutlet weak var lastNameEditText: UITextField!
    
    
    @IBOutlet var popup: UIView!
    //Clear and Add Views
    
    //Views For hide And Visible
 
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    let datePicker = UIDatePicker()
    var customer: Customer?
    var emailsList: [String]?
    
    var clusterIndex: Int?
    var index: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup Navigation Items
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        popup.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        
        //Hide Initial Elements

        
        //Setup Customers
        loadCustomer()
        
        //Text Variables
        firstNameEditText.delegate = self
        lastNameEditText.delegate = self
        ageTextField.delegate = self
        emailTextField.delegate = self
        phoneTextField.delegate = self
        incomeTextField.delegate = self
        netWorthTextField.delegate = self
        genderTextField.delegate = self
        creditRatingTextField.delegate = self
        
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func loadCustomer()
    {
        var genderList: [String] = []
        genderList.append("Male")
        genderList.append("Female")
        firstNameEditText.text = customer?.firstName
        lastNameEditText.text = customer?.lastName
        genderTextField.text = customer?.genderStr ?? "Male"
        genderTextField.optionArray = genderList
        
        ageTextField.text = customer?.age ?? ""
        phoneTextField.text = customer?.mobilePhone ?? ""
        emailTextField.text = customer?.personalEmail ?? ""
        
        netWorthTextField.text = customer?.net_worth ?? "$0 - $0"
        incomeTextField.text = customer?.income ?? "$0 - $0"
        creditRatingTextField.text = customer?.credit_rating ?? "0 - 0"
        
        genderTextField.didSelect{(selectedText , index ,id) in
              self.genderTextField.text = "\(selectedText)"
        }
        
        
        
    }
    
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tableHeight.constant
            = self.dropdownTable.contentSize.height
        self.popup.frame.size.height = poupTitle.frame.size.height + dropdownTable.frame.size.height
    }
    
    
    @objc
    func cancelDatePicker()
    {
        self.view.endEditing(true)
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
        self.view.endEditing(true)
        let customer: Customer = Customer()
        let firstName = firstNameEditText.text ?? ""
        let lastName = lastNameEditText.text ?? ""
        let gender = genderTextField.text ?? ""
        let age = ageTextField.text ?? ""
        let personalEmail = emailTextField.text ?? ""
        let income = incomeTextField.text ?? ""
        let netWorth = netWorthTextField.text ?? ""
        let creditRating = creditRatingTextField.text ?? ""
        let workPhone = phoneTextField.text ?? ""
        
        if (firstName == "") {
            Toast.show(message: "Please Enter First Name", controller: self)
        }
        else if firstName.count > 20 || lastName.count > 20
        {
            Toast.show(message: "Customer First and Last name length must be less then 20", controller: self)
        }
        else if(gender != "Male" && gender != "Female")
        {
            Toast.show(message: "Please choose valid gender", controller: self)
        }
        else{
            
            customer.id = self.customer!.id
            customer.address_id = self.customer?.id
            customer.firstName = firstName
            customer.lastName = lastName
            customer.age = age
            customer.personalEmail = personalEmail
            customer.mobilePhone = workPhone
            customer.income = income
            customer.net_worth = netWorth
            customer.credit_rating = creditRating
            customer.gender = gender
            customer.resident = true
            editCustomerDelegate?.editCustomer(index: index!, customer: customer, clusterIndex: clusterIndex!)
        }
        
    }
    
   
    
   
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    
    
}

//extension EditCustomerViewController: UITableViewDelegate, UITableViewDataSource
//{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return emailsList!.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = dropdownTable.dequeueReusableCell(withIdentifier: "editListCell", for: indexPath) as! EditListCell
//        cell.rowItem.text = emailsList![indexPath.row]
//        return cell;
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let value = emailsList![indexPath.row]
//        if(value == "Add Personal Email")
//        {
//            personalEmailView.isHidden = false
//            personalEmailTop.constant = 6
//            personalEmailHeight.constant = 21
//        }
//        if(value == "Add Work Email")
//        {
//            workEmailView.isHidden = false
//            workEmailTop.constant = 6
//            workEmailHeight.constant = 21
//        }
//        if(value == "Add Mobile Phone")
//        {
//            mobilePhoneView.isHidden = false
//            mobilePhoneTop.constant = 6
//            mobilePhoneHeight.constant = 21
//        }
//        if(value == "Add Home Phone")
//        {
//            homePhoneView.isHidden = false
//            homePhoneTop.constant = 6
//            homePhoneHeight.constant = 21
//        }
//        if(value == "Add Work Phone")
//        {
//            workPhoneView.isHidden = false
//            workPhoneTop.constant = 6
//            workPhoneHeight.constant = 21
//        }
//        if(value == "Male" || value == "Female")
//        {
//            genderLabel.text = value
//        }
//        popupBackgroundView.isHidden = true
//        self.popup.removeFromSuperview()
//
//    }
//
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
//    {
//        self.viewWillLayoutSubviews()
//    }
//
//
//}

class EditListCell: UITableViewCell
{
    
    @IBOutlet weak var rowItem: UILabel!
}
