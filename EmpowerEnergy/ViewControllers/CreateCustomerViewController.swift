//
//  CreateCustomerViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 04/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit

protocol AddCustomerDelegate {
    func addCustomer(customer: Customer)
}


class CreateCustomerViewController: UIViewController,UITextFieldDelegate {
    
    var addCustomerDelegate: AddCustomerDelegate?
    //Height Constraints
    @IBOutlet weak var personalEmailHeight: NSLayoutConstraint!
    @IBOutlet weak var workPhoneHeight: NSLayoutConstraint!
    @IBOutlet weak var homePhoneHeight: NSLayoutConstraint!
    @IBOutlet weak var mobilePhoneHeight: NSLayoutConstraint!
    @IBOutlet weak var workEmailHeight: NSLayoutConstraint!
    
    //BarItems
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBOutlet weak var genderTextField: DropDown!
    
    
    //Popup Veriables
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var addListTable: UITableView!
    @IBOutlet var popup: UIView!
    @IBOutlet weak var popupBackgroundView: UIView!
    
    //Text Variables
    @IBOutlet weak var firstNameEditText: UITextField!
    @IBOutlet weak var lastNameEditText: UITextField!
    @IBOutlet weak var workEmailEditText: UITextField!
    @IBOutlet weak var personalEmailEditText: UITextField!
    @IBOutlet weak var mobilePhoneEditText: UITextField!
    @IBOutlet weak var homePhoneEditText: UITextField!
    @IBOutlet weak var workPhoneEditText: UITextField!
    
    @IBOutlet weak var birthdayEditText: UITextField!
    
    //Clear and Add Views
    @IBOutlet weak var clearWorkPhone: UIImageView!
    @IBOutlet weak var clearHomePhone: UIImageView!
    @IBOutlet weak var clearMobilePhone: UIImageView!
    @IBOutlet weak var clearWorkEmail: UIImageView!
    @IBOutlet weak var clearPersonalEmai: UIImageView!
    @IBOutlet weak var addEmailView: UIView!
    @IBOutlet weak var addPhoneView: UIView!
    
    //Views For hide And Visible
    @IBOutlet weak var workEmailView: UIView!
    @IBOutlet weak var personalEmailView: UIView!
    @IBOutlet weak var homePhoneView: UIView!
    @IBOutlet weak var workPhoneView: UIView!
    @IBOutlet weak var mobilePhoneView: UIView!
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    //Constraints
    @IBOutlet weak var workPhoneTop: NSLayoutConstraint!
    @IBOutlet weak var homePhoneTop: NSLayoutConstraint!
    @IBOutlet weak var mobilePhoneTop: NSLayoutConstraint!
    @IBOutlet weak var personalEmailTop: NSLayoutConstraint!
    @IBOutlet weak var workEmailtop: NSLayoutConstraint!
    
    let datePicker = UIDatePicker()
    
    var emailsList: [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup Navigation Items
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Create New Customer"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        popupBackgroundView.isHidden = true
        popup.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        
        firstNameEditText.delegate = self
        lastNameEditText.delegate = self
        workEmailEditText.delegate = self
        personalEmailEditText.delegate = self
        mobilePhoneEditText.delegate = self
        homePhoneEditText.delegate = self
        workPhoneEditText.delegate = self
        
        //Hide Initial Elements
        hideElements()
        createDatePicker()
        setUpExtraGestures()
        genderTextField.text = "Male"
        
        NotificationCenter.default.addObserver(self, selector: #selector(onResume), name:  UIApplication.willEnterForegroundNotification, object: nil)
        setUpDropDownList()
        
    }
    
    func setUpDropDownList()
    {
        emailsList = []
        emailsList?.append("Male")
        emailsList?.append("Female")
        genderTextField.optionArray = emailsList!
        genderTextField.didSelect{(selectedText , index ,id) in
              self.genderTextField.text = "\(selectedText)"
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    @objc func onResume() {
        print("Working");
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func createDatePicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneBtn  = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneDatePicker))
        let cancleBtn  = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelDatePicker))
        toolbar.setItems([doneBtn,cancleBtn], animated: true)
        birthdayEditText.inputAccessoryView = toolbar
        birthdayEditText.inputView = datePicker
        
        datePicker.datePickerMode = .date
    }
    
    @objc
    func doneDatePicker()
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        birthdayEditText.text = "\(formatter.string(from: datePicker.date))"
        self.view.endEditing(true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tableHeight.constant
            = self.addListTable.contentSize.height
        self.popup.frame.size.height = popupTitle.frame.size.height + addListTable.frame.size.height
    }
    
    
    @objc
    func cancelDatePicker()
    {
        self.view.endEditing(true)
    }
    
//    @objc
//    func dismissPopup()
//    {
//        self.popup.removeFromSuperview()
//        self.popupBackgroundView.isHidden = true
//    }
    
    func hideElements()
    {
        personalEmailTop.constant = 0
        workEmailtop.constant = 0
        mobilePhoneTop.constant = 0
        homePhoneTop.constant = 0
        workPhoneTop.constant = 0
        
        personalEmailHeight.constant = 0
        workEmailHeight.constant = 0
        mobilePhoneHeight.constant = 0
        homePhoneHeight.constant = 0
        workPhoneHeight.constant = 0
        
        personalEmailView.isHidden = true
        workEmailView.isHidden = true
        mobilePhoneView.isHidden = true
        homePhoneView.isHidden = true
        workPhoneView.isHidden = true
    }
    
    func setUpExtraGestures()
    {
        clearPersonalEmai.isUserInteractionEnabled = true
        clearWorkEmail.isUserInteractionEnabled = true
        clearMobilePhone.isUserInteractionEnabled = true
        clearHomePhone.isUserInteractionEnabled = true
        clearWorkPhone.isUserInteractionEnabled = true
        
        let addEmailGesture = UITapGestureRecognizer(target: self, action: #selector(addEmail))
        addEmailView.addGestureRecognizer(addEmailGesture)
        
        let addPhoneGesture = UITapGestureRecognizer(target: self, action: #selector(addPhone))
        addPhoneView.addGestureRecognizer(addPhoneGesture)
        
        let dismissGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPersonalEmail))
        clearPersonalEmai.addGestureRecognizer(dismissGesture)
        
        let dismissGesture1 = UITapGestureRecognizer(target: self, action: #selector(dismissWorkEmail))
        clearWorkEmail.addGestureRecognizer(dismissGesture1)
        
        let dismissGesture2 = UITapGestureRecognizer(target: self, action: #selector(dismissMobilePhone))
        clearMobilePhone.addGestureRecognizer(dismissGesture2)
        
        let dismissGesture3 = UITapGestureRecognizer(target: self, action: #selector(dismissHomePhone))
        clearHomePhone.addGestureRecognizer(dismissGesture3)
        
        let dismissGesture4 = UITapGestureRecognizer(target: self, action: #selector(dismissWorkPhone))
        clearWorkPhone.addGestureRecognizer(dismissGesture4)
        
       // genderLabel.addTarget(self, action: #selector(selectGender), for: .touchDown)
        
        //let dissmissPopupGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup))
        //popupBackgroundView.addGestureRecognizer(dissmissPopupGesture)
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
        self.view.endEditing(true)
        let customer: Customer = Customer()
        let firstName = firstNameEditText.text ?? ""
        let lastName = lastNameEditText.text ?? ""
        let birthday = birthdayEditText.text ?? ""
        let gender = genderTextField.text ?? ""
        let personalEmail = personalEmailEditText.text ?? ""
        let workEmail = workEmailEditText.text ?? ""
        let mobilePhone = mobilePhoneEditText.text ?? ""
        let homePhone = homePhoneEditText.text ?? ""
        let workPhone = workPhoneEditText.text ?? ""
        
        if (firstName == "") {
            Toast.show(message: "Please Enter First Name", controller: self)
        }
        else if firstName.count > 20 || lastName.count > 20
        {
            Toast.show(message: "Customer First and Last name length must be less then 20", controller: self)
        }
        else if(gender != "Male" && gender != "Female")
        {
            Toast.show(message: "Please choose valid gender", controller: self)
        }
        else{
            customer.firstName = firstName
            customer.lastName = lastName
            customer.mobilePhone = mobilePhone
            customer.homePhone = homePhone
            customer.workEmail = workEmail
            customer.workPhone = workPhone
            customer.personalEmail = personalEmail
            customer.birthday = birthday
            customer.gender = gender
            addCustomerDelegate?.addCustomer(customer: customer)
        }
        
    }
    
    @objc
    func dismissPersonalEmail()
    {
        self.personalEmailView.isHidden = true
        self.personalEmailTop.constant = 0
        self.personalEmailHeight.constant = 0
        self.personalEmailEditText.text = ""
    }
    
    @objc
    func dismissWorkEmail()
    {
        self.workEmailView.isHidden = true
        self.workEmailtop.constant = 0
        self.workEmailHeight.constant = 0
        self.workEmailEditText.text = ""
    }
    
    @objc
    func dismissMobilePhone()
    {
        self.mobilePhoneView.isHidden = true
        self.mobilePhoneTop.constant = 0
        self.mobilePhoneHeight.constant = 0
        self.mobilePhoneEditText.text = ""
    }
    
    @objc
    func dismissHomePhone()
    {
        self.homePhoneView.isHidden = true
        self.homePhoneHeight.constant = 0
        self.homePhoneTop.constant = 0
        self.homePhoneEditText.text = ""
    }
    
    @objc
    func dismissWorkPhone()
    {
        self.workPhoneView.isHidden = true
        self.workPhoneTop.constant = 0
        self.workPhoneHeight.constant = 0
        self.workPhoneEditText.text = ""
    }
    
    @objc
    func addPhone()
    {
        popupTitle.text = "Add Phone"
        emailsList = []
        if (mobilePhoneView.isHidden == true) {
            emailsList?.append("Add Mobile Phone")
        }
        if (homePhoneView.isHidden == true) {
            emailsList?.append("Add Home Phone")
        }
        if (workPhoneView.isHidden == true) {
            emailsList?.append("Add Work Phone")
        }
        addListTable.reloadData()
        if(emailsList!.count>0)
        {
            self.view.addSubview(popup)
            //.center = self.view.center
            self.popupBackgroundView.isHidden = false
        }
        else
        {
            Toast.show(message: "All phone fields are active", controller: self)
        }
    }
    
    @objc
    func addEmail()
    {
        popupTitle.text = "Add Email"
        emailsList = []
        if (personalEmailView.isHidden == true) {
            emailsList?.append("Add Personal Email")
        }
        if (workEmailView.isHidden == true) {
            emailsList?.append("Add Work Email")
        }
        addListTable.reloadData()
        if(emailsList!.count>0)
        {
            self.view.addSubview(popup)
            //popup.center = self.view.center
            self.popupBackgroundView.isHidden = false
        }
        else
        {
            Toast.show(message: "All email fields are active", controller: self)
        }
    }
    
    @objc
    func selectGender()
    {
        if Constants.LOGGING_ENABLED{
         print("gender Click")
        }
        popupTitle.text = "Select Gender"
        emailsList = []
        emailsList?.append("Male")
        emailsList?.append("Female")
        addListTable.reloadData()
        self.view.addSubview(popup)
        //popup.center = self.view.center
        self.popupBackgroundView.isHidden = false
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    
    
}

extension CreateCustomerViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emailsList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = addListTable.dequeueReusableCell(withIdentifier: "addListCell", for: indexPath) as! AddListCell
        cell.label.text = emailsList![indexPath.row]
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let value = emailsList![indexPath.row]
        if(value == "Add Personal Email")
        {
            personalEmailView.isHidden = false
            personalEmailTop.constant = 6
            personalEmailHeight.constant = 21
        }
        if(value == "Add Work Email")
        {
            workEmailView.isHidden = false
            workEmailtop.constant = 6
            workEmailHeight.constant = 21
        }
        if(value == "Add Mobile Phone")
        {
            mobilePhoneView.isHidden = false
            mobilePhoneTop.constant = 6
            mobilePhoneHeight.constant = 21
        }
        if(value == "Add Home Phone")
        {
            homePhoneView.isHidden = false
            homePhoneTop.constant = 6
            homePhoneHeight.constant = 21
        }
        if(value == "Add Work Phone")
        {
            workPhoneView.isHidden = false
            workPhoneTop.constant = 6
            workPhoneHeight.constant = 21
        }
        popupBackgroundView.isHidden = true
        self.popup.removeFromSuperview()
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        self.viewWillLayoutSubviews()
    }
    
    
}

class AddListCell: UITableViewCell
{
    
    @IBOutlet weak var label: UILabel!
}
