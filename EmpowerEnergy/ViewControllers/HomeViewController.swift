//
//  MainViewViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 27/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import GoogleMaps
import GoogleMapsUtils
import Alamofire
import SwiftyJSON
import Bottomsheet

class HomeViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate, GMUClusterManagerDelegate, GMUClusterRendererDelegate{
    
    var addressList: [AddressInteractionClusterItem] = []
    @IBOutlet var addressListPopup: UIView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var addressListTable: UITableView!
    @IBOutlet weak var addAddressStreetLbl: UILabel!
    @IBOutlet weak var addAddressDescriptionLbl: UILabel!
    @IBOutlet weak var addAddressLatLngLbl: UILabel!
    @IBOutlet weak var addAddressView: UIView!
    
    var addressLatitude: Double?
    var addressLongitude: Double?
    var addressStreet: String?
    var addressCity: String?
    var addressPostCode: Int?
    var addressState: String?
    
    
    @IBOutlet weak var addAddressPin: UIView!
    
    
    @IBOutlet weak var employeeListBtn: UIButton!
    @IBOutlet weak var myLocationViewBtn: UIView!
    @IBOutlet weak var progressView: UIActivityIndicatorView!
    @IBOutlet weak var leaderBoardView: UIView!
    var mainAreaResponse: [AreasResponse] = []
    @IBOutlet weak var drawAreaBottomSheet: UIView!
    
    var currentMarker: GMSMarker?
    
    @IBOutlet weak var manageEmployeesViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addAddressPinBtn: UIView!
    
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var leaderBoardViewHeight: NSLayoutConstraint!
    @IBOutlet weak var manageEmployeesView: UIView!
    var drawCoordinates = [CLLocationCoordinate2D]()
    var drawPolygon: GMSPolygon! = GMSPolygon()
    var isDrawingModeEnabled = false
    var polygonDeleteMarkers = [DeleteMarker]()
    
    var polygonsList: [GMSPolygon] = []
    
    var selectedColor = ""
    
    @IBOutlet weak var drawAreaButton: UIButton!
    enum CardState{
        case expanded
        case halfexpanded
        case collapsed
    }
    
    var colorList: [String] = []
    
    @IBOutlet weak var colorCollectionView: UICollectionView!
    
    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var myAreaButton: UIButton!
    @IBOutlet weak var conversationsView: UIView!
    @IBOutlet weak var leftLeadingAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var drawerEmailLabel: UILabel!
    @IBOutlet weak var drawerNameLabel: UILabel!
    
    @IBOutlet weak var progressDialog: UIActivityIndicatorView!
    @IBOutlet weak var logoutMenuItem: UIView!
    
    @IBOutlet weak var privacyBtn: UIView!
    @IBOutlet weak var sunroofView: UILabel!
    
    
    @IBOutlet var mainMenu: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var rightSideMenu: UIView!
    
    @IBOutlet weak var drawerMenu: UIView!
    @IBOutlet weak var google_map: GMSMapView!
    var locationManager : CLLocationManager = CLLocationManager()
    
    let isClustering: Bool = true;
    let isCustom: Bool = true;
    
    var targetLatLong:CLLocationCoordinate2D? = nil
    var items: [AddressInteractionClusterItem] = [];
    
    var clusterManager: GMUClusterManager?
    
    var areaItems: [AreaClusterItem] = []
    var areaClusterManager: GMUClusterManager?
    
    @IBOutlet weak var appointmentView: UIView!
    var locationResponse: [InteractionAddressData] = [];
    
    var addressInformationController: AddressInformationViewController!
    var areaDetailController: AreaDetailViewController!
    var visualEffectView: UIVisualEffectView!
    
    var visualEffectAreaView: UIVisualEffectView!
    
    let cardHeight: CGFloat = 600;
    var cardHandleAreaHeight: CGFloat = 65;
    
    var cardVisible = false
    var halfExpanded = false
    
    var areaCardVisible = false
    var areaHalfExpanded = false
    
    var nextState: CardState
    {
        if cardVisible == false && halfExpanded == false
        {
            return .halfexpanded
        }
        else if halfExpanded == true && cardVisible == true
        {
            return .expanded
        }
        else if halfExpanded == false && cardVisible == true
        {
            return .halfexpanded
        }
        else
        {
            return .collapsed
        }
    }
    
    var runningAnimations = [UIViewPropertyAnimator]()
    var animationProgressWhenInterupted: CGFloat = 0
    
    var runningAreaAnimations = [UIViewPropertyAnimator]()
    
    
    lazy var canvasView:CanvasView = {
        
        var overlayView = CanvasView(frame: self.google_map
            .frame)
        overlayView.isUserInteractionEnabled = true
        overlayView.delegate = self
        return overlayView
        
    }()
    
    
    @IBOutlet weak var drawAreaButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var drawAreaButtonPadding: NSLayoutConstraint!
    
    @IBOutlet weak var drawBtnView: UIView!
    var alert: UIAlertController?
    @IBOutlet weak var menuHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCard()
        setUpAreaCard()
        self.navigationController?.isNavigationBarHidden = true
        self.drawerNameLabel.text = "\(SessionUtils.getName()) - \(SessionUtils.getUserType())"
        self.drawerEmailLabel.text = "\(SessionUtils.getEmail())"
        self.rightSideMenu.isHidden = true
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        addressListPopup.center = self.view.center
        
        //print(SessionUtils.getToken())
        
        //myAreaButton.backgroundColor = UIColor.white
        //myAreaButton.layer.cornerRadius = myAreaButton.frame.height/2
        //myAreaButton.layer.shadowOpacity = 0.25
        
        //drawAreaButton.backgroundColor = UIColor.white
        //drawAreaButton.layer.cornerRadius = drawAreaButton.frame.height/2
        //drawAreaButton.layer.shadowOpacity = 0.25
        //myAreaButton.layer.shadowRadius = 5
        //myAreaButton.layer.shadowOffset = CGSize(width: 0, height: 10)
        
        // self.drawerMenu.isHidden = true
        setUpGoogleMap()
        setUpExtraGestures()
        setUpDrawerMenuItemGestures()
        progressDialog.isHidden = true
        
        getAreas()
        
        initializeColors()
        
        drawAreaBottomSheet.isHidden = true
        
        if SessionUtils.getUserType() == "Sales Rep"
        {
            drawAreaButton.isHidden = true
            drawAreaButtonHeight.constant = 0
            drawBtnView.isHidden = true
            //drawAreaButtonPadding.constant = 0
            menuHeight.constant = 230 - 43
            manageEmployeesView.isHidden = true
            manageEmployeesViewHeight.constant = 0
            
            leaderBoardView.isHidden = true
            leaderBoardViewHeight.constant = 0
            employeeListBtn.isHidden = true
        }
            
        else if SessionUtils.getUserType() == "Manager"
        {
            manageEmployeesView.isHidden = true
            manageEmployeesViewHeight.constant = 0
        }
        
        dismissAddAddressView()
        
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        var constant = 0
        print(self.addressListTable.contentSize.height)
        if(self.addressListTable.contentSize.height > 520)
        {
            constant = 520
        }
        else
        {
            constant = Int(self.addressListTable.contentSize.height + 30)
        }
        self.tableHeight.constant = CGFloat(constant)
        self.addressListPopup.frame.size.height =  self.tableHeight.constant
        self.addressListPopup.center = CGPoint(x: self.view.frame.size.width/2, y: (self.view.frame.size.height)/2)
    }
    
    
    func getAddressByPin(latitude: Double, longitude: Double)
    {
        if Constants.LOGGING_ENABLED
        {
            print("Getting Result")
        }
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in

            // Place details
            var placeMark: CLPlacemark!
        
            if(placemarks!.count>0)
            {
                placeMark = placemarks![0]
                
                self.addressCity = placeMark.locality ?? ""
                self.addressState = placeMark.administrativeArea ?? ""
                self.addressStreet = "\(placeMark.subThoroughfare ?? "") \(placeMark.thoroughfare ?? "")"
                self.addressPostCode =  Int(placeMark.postalCode ?? "0")
                self.addressLatitude = latitude
                self.addressLongitude = longitude
                
                if(self.addressStreet == "" || self.addressStreet == " ")
                {
                    self.addAddressStreetLbl.text = "Not Found"
                }
                if(self.addressState == "" || self.addressState == " ")
                {
                   self.addAddressDescriptionLbl.text = "Not Found"
                }
                if(self.addressCity == "" || self.addressCity == " ")
                {
                   self.addAddressDescriptionLbl.text = "Not Found"
                }
                    
                if(self.addressCity != "" && self.addressCity != " " && self.addressState != "" && self.addressState != " "){
                    
                    self.addAddressLatLngLbl.text = "\(latitude), \(longitude)"
                    self.addAddressStreetLbl.text = "\(self.addressStreet!)"
                    self.addAddressDescriptionLbl.text = "\(String(describing: self.addressCity!)), \(String(describing: self.addressPostCode!)), \(String(describing: self.addressState!))"
                }
                
            }else{
                self.addAddressLatLngLbl.text = "Not Found"
                self.addAddressStreetLbl.text = "Not Found"
                self.addAddressDescriptionLbl.text = "Not Found"
            }
            
        });
    }
    
    @objc func enableAndDisableAddAddress()
    {
        if(addAddressView.isHidden == true)
        {
            enableAddAddressView()
        }else
        {
            dismissAddAddressView()
            disableDrawEvents()
        }
    }
    
    func dismissAddAddressView()
    {
        addressStreet = ""
        addressState = ""
        addressCity = ""
        addAddressDescriptionLbl.text = ""
        addAddressStreetLbl.text = ""
        addAddressLatLngLbl.text = ""
        addAddressView.isHidden = true
        addAddressPin.isHidden = true
    }
    
    func enableAddAddressView(){
        disableDrawEvents();
        addressStreet = ""
        addressState = ""
        addressCity = ""
        addAddressDescriptionLbl.text = ""
        addAddressStreetLbl.text = ""
        addAddressLatLngLbl.text = ""
        addAddressView.isHidden = false
        addAddressPin.isHidden = false
        let camera = google_map.camera
        getAddressByPin(latitude: camera.target.latitude, longitude: camera.target.longitude)
    }
    
    @IBAction func addAddressCancelClick(_ sender: Any) {
        dismissAddAddressView()
        disableDrawEvents()
    }
    
    @IBAction func addAddressCreateClick(_ sender: Any) {
        
        if(addressStreet == "" || addressStreet == " ")
        {
            Toast.show(message: "Invalid Address Information", controller: self)
        }
        else if(addressCity == "" || addressCity == " ")
        {
            Toast.show(message: "Invalid Address Information", controller: self)
        }
        else if(addressState == "" || addressState == " ")
        {
            Toast.show(message: "Invalid Address Information", controller: self)
        }
        else{
            addAddressInformation()
        }
    }
    
    @objc
    func showMyLocation()
    {
        if CLLocationManager.locationServicesEnabled() {
            let latitude = locationManager.location?.coordinate.latitude
            let longitude = locationManager.location?.coordinate.longitude
            
            if latitude == nil || longitude == nil || latitude == 0.0 || longitude == 0.0
            {
                Toast.show(message: "Enable location from settings", controller: self)
            }else
            {
                google_map.camera = GMSCameraPosition(latitude: latitude!, longitude: longitude!, zoom: 18.0)
            }
        }
        
        
    }
    
    
    
    
    
    @IBAction func drawAreaAction(_ sender: Any) {
        
        dismissAddAddressView()
        self.drawCoordinates.removeAll()
        self.google_map.addSubview(canvasView)
        //let origImage = UIImage(systemName: "plus")
        //let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        //drawAreaButton.setImage(tintedImage, for: .normal)
        //drawAreaButton.tintColor = UIColor.white
        //drawAreaButton.backgroundColor = UIColor.gray
        drawAreaBottomSheet.isHidden = false
        //        if(drawAreaButton.backgroundColor == UIColor.secondarySystemBackground)
        //        {
        //            self.drawCoordinates.removeAll()
        //            self.google_map.addSubview(canvasView)
        //            //let origImage = UIImage(systemName: "plus")
        //            //let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        //            //drawAreaButton.setImage(tintedImage, for: .normal)
        //            //drawAreaButton.tintColor = UIColor.white
        //            //drawAreaButton.backgroundColor = UIColor.gray
        //            drawAreaBottomSheet.isHidden = false
        //        }else
        //        {
        //            disableDrawEvents()
        //        }
        
    }
    
    @IBAction func employeesListBtnClick(_ sender: Any) {
        
        let areaViewController = UIStoryboard.init(name: "EmployeesList", bundle: Bundle.main).instantiateViewController(withIdentifier: "EmployeesListViewController") as! EmployeesListViewController
        
        self.navigationController?.pushViewController(areaViewController, animated: false);
        
    }
    
    func showCurrentMarker(latitude: Double,longitude: Double,number: String)
    {
        
        
        // I have taken a pin image which is a custom image
        var markerImage = Utils.resizeImage(image: UIImage(named: "clustermarker")!, sizeImage: CGSize(width: 23, height: 23))
        markerImage = Utils.textToImageForMarker(drawText: number as NSString, inImage: markerImage)
        
        //creating a marker view
        let markerView = UIImageView(image: markerImage)
        currentMarker = GMSMarker()
        //changing the tint color of the image
        markerView.tintColor = UIColor.red
        
        currentMarker!.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        currentMarker!.iconView = markerView
        currentMarker!.map = google_map
    }
    
    func dismissCurrentMarker()
    {
        currentMarker?.map = nil
    }
    
    
    
    
    
    @IBAction func onCancelDrawAreaAction(_ sender: Any) {
        disableDrawEvents()
    }
    
    @IBAction func onCreateDrawAreaAction(_ sender: Any) {
        
        if(drawCoordinates.count>2)
        {
            let areaViewController = UIStoryboard.init(name: "CreateArea", bundle: Bundle.main).instantiateViewController(withIdentifier: "CreateAreaViewController") as! CreateAreaViewController
            areaViewController.selectedColor = self.selectedColor
            areaViewController.drawCoordinates = self.drawCoordinates
            areaViewController.createAreaDelegate = self
            self.navigationController?.pushViewController(areaViewController, animated: false);
        }
        else
        {
            Toast.show(message: "Please draw proper area", controller: self)
        }
    }
    
    func disableDrawEvents()
    {
        drawCoordinates = []
        self.canvasView.image = nil
        self.canvasView.removeFromSuperview()
        
        let origImage = UIImage(systemName: "plus")
        let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        drawAreaButton.setImage(tintedImage, for: .normal)
        drawAreaButton.tintColor = UIColor(named: "textColor")
        drawAreaButton.backgroundColor = UIColor.secondarySystemBackground
        
        drawAreaBottomSheet.isHidden = true
        drawPolygon.map = nil
        
    }
    
   @objc func handleGesture(){
      tapGestureAction()
       
    }
    
    
    
    func initializeColors()
    {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = -10
        layout.minimumInteritemSpacing = 0
        colorCollectionView.collectionViewLayout = layout
        
        colorList = []
        colorList.append("#FF5733")
        colorList.append("#FF8D33")
        colorList.append("#FFD133")
        colorList.append("\(UIColor.systemGray2.toHexString())")
        colorList.append("#FF333C")
        colorList.append("#8333FF")
        colorList.append("#FF33B8")
        colorList.append("#9C9B9D")
        colorList.append("#009BFA")
        colorList.append("#81303F9F")
        colorList.append("#8F00FFC4")
        colorList.append("#009688")
        
        selectedColor = colorList[0]
    }
    
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    
    
    //  Initialize Map View
    func setUpGoogleMap()
    {
        //show_marker(position: google_map.camera.target)
        let camera = GMSCameraPosition.camera(withLatitude: 41.65689673944328, longitude: -72.66650788486002, zoom: 17.0)
        google_map.camera = camera
        //let google_map = GMSMapView.map(withFrame: .zero, camera: camera)
        google_map.settings.compassButton = true
        
        google_map.isMyLocationEnabled = true;
        //let compassBtn = google_map.subviews.last! as! UIButton
        //compassBtn.autoresizingMask = UIView.AutoresizingMask(arrayLiteral: UIView.AutoresizingMask.flexibleBottomMargin)
        //var frame = compassBtn.frame//
        //frame.origin.y = 10
        //compassBtn.frame = frame
        //google_map.settings.myLocationButton = true
        //google_map.settings.compassButton = true
        google_map.delegate = self
        locationManager.delegate  = self
        locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        /*if let nav_height = self.navigationController?.navigationBar.frame.height
         {
         let status_height = view.window?.windowScene!.statusBarManager!.statusBarFrame.size.height
         google_map.padding = UIEdgeInsets (top: nav_height+status_height!, left: 0, bottom: 0, right: 0)
         }*/
        
        if(SessionUtils.getLatitude() == 0.0 || SessionUtils.getLongitude() == 0.0 )
        {
            //print("Latitude and Longitude are Null")
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                //locationManager.location?.coordinate.latitude
                showMyLocation()
            }
            else
            {
                Toast.show(message: "Your Location Services are Off Please enable from setting", controller: self)
            }
        }
        else
        {
            let camera = GMSCameraPosition.camera(withLatitude: SessionUtils.getLatitude(), longitude: SessionUtils.getLongitude(), zoom: Float(SessionUtils.getZoom()))
            google_map.camera = camera
        }
        //print(GMSMapViewType.hybrid.rawValue)
        //print(GMSMapViewType.normal.rawValue)
        //print(GMSMapViewType.satellite.rawValue)
        //print(GMSMapViewType.terrain.rawValue)
        //print(SessionUtils.getMapType())
        
        
        
        
        if SessionUtils.getMapType() == 1  || SessionUtils.getMapType() == 0 {
            google_map.mapType = GMSMapViewType.normal
        }
        else if SessionUtils.getMapType() == 2{
            google_map.mapType = GMSMapViewType.satellite
        }
        else if SessionUtils.getMapType() == 3 {
            google_map.mapType = GMSMapViewType.terrain
        }
        else if SessionUtils.getMapType() == 4 {
            google_map.mapType = GMSMapViewType.hybrid
        }
        else{
            google_map.mapType = GMSMapViewType.hybrid
        }
        //google_map.translatesAutoresizingMaskIntoConstraints = true
        
        //self.view = self.mainView
        //Configure Cluster Manager For Locations
        if isClustering{
            
            var iconGenerator = GMUDefaultClusterIconGenerator()
            if(isCustom)
            {
                var images: [UIImage] = []
                // for i in 1...4{
                images.append(Utils.resizeImage(image: UIImage(named: "clustermarker")!, sizeImage: CGSize(width: 25, height: 25)))
                //}
                iconGenerator = GMUDefaultClusterIconGenerator(buckets: [200], backgroundImages: images)
            }
            let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
            let renderer = GMUDefaultClusterRenderer(mapView: google_map, clusterIconGenerator: iconGenerator)
            renderer.maximumClusterZoom = 19
            renderer.minimumClusterSize = 2
            renderer.animatesClusters = true
            renderer.delegate = self
            clusterManager = GMUClusterManager(map: google_map, algorithm: algorithm, renderer: renderer)
            clusterManager!.cluster()
            clusterManager!.setDelegate(self, mapDelegate: self)
        }
        
        //AreaCluster Settings
        var iconGenerator1 = GMUDefaultClusterIconGenerator()
        if(isCustom)
        {
            var images: [UIImage] = []
            for i in 1...39{
                
                let tmpImage = Utils.resizeImage(image: UIImage(named: "bubble")!, sizeImage: CGSize(width: 100, height: 25))
                images.append(Utils.drawText(text: "\(i)  Areas" as NSString, inImage: tmpImage)!)
            }
            iconGenerator1 = GMUDefaultClusterIconGenerator(buckets: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40], backgroundImages: images)
        }
        
        let algorithmArea = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: google_map, clusterIconGenerator: iconGenerator1)
        renderer.delegate = self
        areaClusterManager = GMUClusterManager(map: google_map, algorithm: algorithmArea, renderer: renderer)
        areaClusterManager!.cluster()
        areaClusterManager!.setDelegate(self, mapDelegate: self)
        
        
        //animateCamera()
        
        
        
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //let userLocation :CLLocation = locations[0] as CLLocation
        //print("\(userLocation.coordinate.latitude)")
        //print("\(userLocation.coordinate.longitude)")
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        if(cluster.items is [AddressInteractionClusterItem]){
            let selectedItems = cluster.items as! [AddressInteractionClusterItem]
            if(selectedItems.count<30)
            {
                self.halfExpanded = false
                self.cardVisible = false
                self.addressInformationController.view.frame.origin.y = self.view.frame.height
                self.dismissCurrentMarker()
                
                self.addressList = []
                self.addressList = selectedItems
                addressListTable.reloadData()
                viewWillLayoutSubviews()
                self.view.addSubview(addressListPopup)
                self.rightSideMenu.isHidden = false
                viewWillLayoutSubviews()
            }else
            {
                Toast.show(message: "Address size must be less than 30", controller: self)
            }
        }
        return true
    }

    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
        if clusterItem is AddressInteractionClusterItem{
            dismissAddAddressView()
            let addressInteractionClusterItem = clusterItem as! AddressInteractionClusterItem
            dismissCurrentMarker()
            cardVisible = true
            halfExpanded = true
            self.areaHalfExpanded = false
            self.areaCardVisible = false
            self.areaDetailController.view.frame.origin.y = self.view.frame.height
            self.showCurrentMarker(latitude: addressInteractionClusterItem.addressInteraction!.address!.location!.latitude!, longitude: addressInteractionClusterItem.addressInteraction!.address!.location!.longitude!,number: addressInteractionClusterItem.addressInteraction?.address?.number ?? "")
            self.addressInformationController.view.frame.origin.y = self.cardHandleAreaHeight
            self.addressInformationController.getAddressHistory(address_id: (addressInteractionClusterItem.addressInteraction?.address?.id)!)
            self.addressInformationController.loadInformation(addressInteraction: addressInteractionClusterItem.addressInteraction!, index: addressInteractionClusterItem.index!)
            
        }
        if(clusterItem is AreaClusterItem)
        {
            dismissAddAddressView()
            let areaClusterItem = clusterItem as! AreaClusterItem
            dismissCurrentMarker()
            areaCardVisible = true
            areaHalfExpanded = true
            self.halfExpanded = false
            self.cardVisible = false
            self.addressInformationController.view.frame.origin.y = self.view.frame.height
            self.areaDetailController.view.frame.origin.y = self.cardHandleAreaHeight
            //print("Hash Value \(clusterItem.hash)")
            self.areaDetailController.loadInformation(area: areaClusterItem.area!, index: areaClusterItem.index!)
            
        }
        return true;
    }
    
    @IBAction func myAreaButton(_ sender: Any) {
    }
    func animateCamera()
    {
        let camera = GMSCameraPosition.camera(withLatitude: 41.63072254, longitude: -72.66435136302, zoom: 12.0)
        self.google_map.animate(to: camera)
        
    }
    
    @IBAction func myAreaButtonclick(_ sender: Any) {
        
        let myAreasController = UIStoryboard.init(name: "MyAreas", bundle: Bundle.main).instantiateViewController(withIdentifier: "MyAreaViewController") as! MyAreaViewController
        myAreasController.myAreaDelegate = self
        self.navigationController?.pushViewController(myAreasController, animated: false);
    }
    
    @objc
    func searchLocationClick()
    {
        if Constants.LOGGING_ENABLED{
            print("wroking")}
        let searchController = UIStoryboard.init(name: "Search", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        searchController.addressSearchDelegate = self
        searchController.customerSearchDelegate = self
        //self.present(searchController, animated: true, completion: nil)
        self.navigationController?.pushViewController(searchController, animated: false);
    }
    
    
    
    @IBAction func drawerBtnAction(_ sender: Any) {
        self.leftLeadingAnchor.constant = 0;
        //self.drawerMenu.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        DispatchQueue.main.async {
            self.rightSideMenu.isHidden = false
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
            
        if(addAddressPin.isHidden == false && addAddressView.isHidden == false)
        {
            getAddressByPin(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude);
        }
            
        if Constants.LOGGING_ENABLED{
            print("Latitude: \(mapView.camera.target.latitude) Longitude: \(mapView.camera.target.longitude) - \(mapView.camera.zoom)")
        }
        let cameraPos: GMSCameraPosition = mapView.camera
        
        SessionUtils.setCurrentLatLong(latitude: cameraPos.target.latitude, longitude: cameraPos.target.longitude, zoom: Double(cameraPos.zoom))
        if(progressDialog.isHidden == true)
        {
            let cameraPosition: GMSCameraPosition = mapView.camera
            let latLng:CLLocationCoordinate2D = cameraPosition.target;
            if targetLatLong == nil{
                targetLatLong = latLng
                //var zoom: Float =  cameraPosition.zoom;
                
                /// Remember to Save Current State of Map To Session Cache Data
                //Save Current Target Location To Session Management
                
                items = []
                progressDialog.isHidden = false
                progressDialog.startAnimating()
                locationResponse = [];
                getNearByLocations(latitude: latLng.latitude, longitude: latLng.longitude, distance: 0.2)
                
            }
            else{
                //let zoom:Float = cameraPosition.zoom;
                
                /// Remember to Save Current State of Map To Session Cache Data
                //Save Current Target Location To Session Management
                
                let coordinate1 = CLLocation(latitude: latLng.latitude, longitude: latLng.longitude)
                let coordinate2 = CLLocation(latitude: targetLatLong!.latitude, longitude: targetLatLong!.longitude)
                let distance: Float = Float(coordinate1.distance(from: coordinate2))
                if Constants.LOGGING_ENABLED{
                    print("Distance is: - \(distance)")}
                if(distance>300)
                {
                    targetLatLong = latLng
                    /// Remember to Save Current State of Map To Session Cache Data
                    //Save Current Target Location To Session Management
                    
                    items = []
                    progressDialog.isHidden = false
                    progressDialog.startAnimating()
                    locationResponse = [];
                    getNearByLocations(latitude: latLng.latitude, longitude: latLng.longitude, distance: 0.2)
                }
                
            }
            
            
        }else{
            print("hello")
        }
    }
    
    @objc func tapLogout(){
        SessionUtils.logout()
        //self.navigationController?.popToRootViewController(animated: false)
        
        let refreshAlert = UIAlertController(title: "", message: "Are you sure to logout?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
            SessionUtils.logout()
            let logoutViewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            self.navigationController?.pushViewController(logoutViewController!, animated: false);
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    @objc func tapGestureAction(){
        self.leftLeadingAnchor.constant = -240;
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        //self.rightSideMenu.isHidden = true
        DispatchQueue.main.async {
            self.rightSideMenu.isHidden = true
        }
        self.addressListPopup.removeFromSuperview()
        
    }
    
    
    
    func setUpDrawerMenuItemGestures()
    {
        let tapGestureLogout = UITapGestureRecognizer(target: self, action: #selector(tapLogout))
        logoutMenuItem.addGestureRecognizer(tapGestureLogout)
        
        let tapGestureConversations = UITapGestureRecognizer(target: self, action: #selector(tabConversations))
        conversationsView.addGestureRecognizer(tapGestureConversations)
        
        
        let tapGestureSunroof = UITapGestureRecognizer(target: self, action: #selector(tabSunroof))
        sunroofView.addGestureRecognizer(tapGestureSunroof)
        
        let tapGesturePrivacy = UITapGestureRecognizer(target: self, action: #selector(tabPrivacy))
        privacyBtn.addGestureRecognizer(tapGesturePrivacy)
        
        let tapGestureAppointment = UITapGestureRecognizer(target: self, action: #selector(tabAppointments))
        appointmentView.addGestureRecognizer(tapGestureAppointment)
        
        let tapGestureEmployees = UITapGestureRecognizer(target: self, action: #selector(tabManageEmployees))
        manageEmployeesView.addGestureRecognizer(tapGestureEmployees)
        
        let tapGestureLeaderBoard = UITapGestureRecognizer(target: self, action: #selector(tabLeaderBoard))
        leaderBoardView.addGestureRecognizer(tapGestureLeaderBoard)
        
        let tapGestureSettings = UITapGestureRecognizer(target: self, action: #selector(tabSettings))
        settingsView.addGestureRecognizer(tapGestureSettings)
        
    }
    
    
    @objc func tabConversations()
    {
        let customerViewController = UIStoryboard.init(name: "Conversations", bundle: Bundle.main).instantiateViewController(withIdentifier: "ConversationsViewController") as! ConversationsViewController
        self.navigationController?.pushViewController(customerViewController, animated: false);
        //self.present(customerViewController, animated: true, completion: nil)
    }
    
    @objc func tabLeaderBoard()
    {
        let customerViewController = UIStoryboard.init(name: "LeaderBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "LeaderBoardViewController") as! LeaderBoardViewController
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    @objc func tabSettings()
    {
        let customerViewController = UIStoryboard.init(name: "Settings", bundle: Bundle.main).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        customerViewController.changeMapDelegate = self
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    
    @objc func tabManageEmployees()
    {
        let manageEmployeeViewController = UIStoryboard.init(name: "ManageEmployee", bundle: Bundle.main).instantiateViewController(withIdentifier: "ManageEmployeeViewController") as! ManageEmployeeViewController
        self.navigationController?.pushViewController(manageEmployeeViewController, animated: false);
    }
    
    @objc func tabAppointments()
    {
        let appViewController = UIStoryboard.init(name: "Appointment", bundle: Bundle.main).instantiateViewController(withIdentifier: "AppointmentViewController") as! AppointmentViewController
        self.navigationController?.pushViewController(appViewController, animated: false);
    }
    
    @objc func tabSunroof()
    {
        let customerViewController = UIStoryboard.init(name: "Sunroof", bundle: Bundle.main).instantiateViewController(withIdentifier: "SunroofViewController") as! SunroofViewController
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    @objc func tabPrivacy()
    {
        let customerViewController = UIStoryboard.init(name: "Privacy", bundle: Bundle.main).instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    
    func setUpExtraGestures()
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        rightSideMenu.addGestureRecognizer(tapGesture)
        
        let searchGesture = UITapGestureRecognizer(target: self, action: #selector(searchLocationClick))
        searchView.addGestureRecognizer(searchGesture)
        
        let myLocationGesture = UITapGestureRecognizer(target: self, action: #selector(showMyLocation))
        myLocationViewBtn.addGestureRecognizer(myLocationGesture)
        
        let pinGesture = UITapGestureRecognizer(target: self, action: #selector(enableAndDisableAddAddress))
        addAddressPinBtn.addGestureRecognizer(pinGesture)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
           swipeLeft.direction = .left
        self.drawerMenu.addGestureRecognizer(swipeLeft)
        
    }
    
    
    // Get Locations of Homes from Server on Camera Idle
    func getNearByLocations(latitude: Double, longitude: Double, distance: Double)
    {
        do{
            let parametersNearBy =  [
                "latitude": latitude,
                "longitude": longitude,
                "distance": distance
                ] as [String : Any]
            
            let header = [
                "Accept": "text/json",
                "Authorization": "bearer \(SessionUtils.getToken())"
            ]
            
            let resourceString = "\(Utils.requestURL)api/nearby";
            
            Alamofire.request(resourceString, method: .get, parameters: parametersNearBy, headers: header).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    if let httpURLResponse = response.response{
                        let response_code = httpURLResponse.statusCode;
                        if response_code == 200 {
                            do{
                                if Constants.LOGGING_ENABLED
                                {
                                    print(value)
                                }
                                let json = JSON(value)
                                let str = String(describing: json);
                                let jsonData = str.data(using: .utf8)
                                let decoder = JSONDecoder();
                                let res = try decoder.decode([InteractionAddressData].self, from: jsonData!)
                                self.locationResponse = res
                                //Manage Data Comes from server
                                self.manageDataOnCameraIdle(locationResponse: self.locationResponse)
                                
                                //Cluster All Location Data to Map
                                self.clusterManager!.clearItems()
                                self.clusterManager!.add(self.items)
                                self.clusterManager!.cluster()
                                self.progressDialog.stopAnimating()
                                self.progressDialog.isHidden = true
                                if Constants.LOGGING_ENABLED{
                                print("Cluster Items \(self.items.count)")
                                    print(value)
                                }
                                self.clusterManager?.cluster()
                                
                            }catch{
                                if Constants.LOGGING_ENABLED{
                                    print(error)}
                                self.clusterManager!.clearItems()
                                self.clusterManager!.add(self.items)
                                self.clusterManager!.cluster()
                                self.progressDialog.stopAnimating()
                                self.progressDialog.isHidden = true
                                
                            }
                            
                            
                        }else if response_code == 401{
                            //print(response.result.value)
                            Toast.show(message: "Unauthorized User", controller: self)
                            let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            self.navigationController?.pushViewController(viewController!, animated: false);
                            //self.present(viewController!,animated: true,completion: nil)
                            
                        }else{
                            if Constants.LOGGING_ENABLED{
                                print("Error - \(response.result.value!)")}
                            Toast.show(message: "No Internet Connection/Server Issue", controller: self)
                            self.clusterManager!.clearItems()
                            self.clusterManager!.add(self.items)
                            self.clusterManager!.cluster()
                            self.progressDialog.stopAnimating()
                            self.progressDialog.isHidden = true
                        }
                    }
                case .failure(let error):
                    if Constants.LOGGING_ENABLED{
                        print(error)}
                    Toast.show(message: "There is Some Server Issue", controller: self)
                    
                }
                
            })
            
            
            
        }
        
        
        
    }
    
    func addAddressInformation()
    {
        
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "latitude": self.addressLatitude!,
            "longitude": self.addressLongitude!,
            "address": self.addressStreet!,
            "zipcode": self.addressPostCode!,
            "city": self.addressCity!,
            "state": self.addressState!,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/address";
        
        Alamofire.request(resourceString, method: .post, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            if Constants.LOGGING_ENABLED{
                               print(value)
                            }
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let addressResponse = try decoder.decode(InteractionAddressDataForAddress.self, from: jsonData!)
                            self.manageDataOnAddAddress(locationResponse: addressResponse)
                            Toast.show(message: "Address Added succesfully", controller: self)
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 401{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                    if Constants.LOGGING_ENABLED{
                        print(response.result.value!)
                        
                        }
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Server Error.", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    
    
    //Manage Data On Camera Idle
    func manageDataOnCameraIdle(locationResponse: [InteractionAddressData]) {
        
        items = []
        clusterManager?.clearItems()
        clusterManager?.cluster()
        for i in (0..<locationResponse.count)
        {
            //Customer Array
            var customersList: [Customer] = [];
            
            var city: String = "";
            var country: String = "";
            let addressLine: String = locationResponse[i].address ?? ""
            city = locationResponse[i].city?.name ?? "Unknown"
            if(locationResponse[i].city?.state != nil){
                country = locationResponse[i].city!.state!.name ?? "USA"
            }else
            {
                country = "USA"
            }
            
            let id:CLong = locationResponse[i].id!
            
            let latitude = Double(locationResponse[i].latitude!)
            let longitude = Double(locationResponse[i].longitude!)
            let location:Location = Location(latitude: latitude!-0.000012, longitude: longitude!)
            let number = locationResponse[i].address!.westernArabicNumeralsOnly
            let postal = locationResponse[i].zipcode ?? ""
            let region = locationResponse[i].region ?? ""
            let street = locationResponse[i].address ?? ""
            let unit = locationResponse[i].unit ?? ""
            
            let address: Address = Address(id: id, number: number , street: street, unit: unit, city: city, region: region, postal: postal, country: country, location: location, addressLine: addressLine)
            
            var interaction: Interaction? = nil;
            if(locationResponse[i].current_interaction == nil)
            {
                interaction = Interaction(id: 1, type: "", name: "None", icon: "", color: "#FFFFFF", ordinal: 1)
            }else
            {
                interaction = Interaction(id: locationResponse[i].current_interaction!.id!, type: locationResponse[i].current_interaction!.type!, name: locationResponse[i].current_interaction!.name!, icon: locationResponse[i].current_interaction!.icon!, color: locationResponse[i].current_interaction!.color!, ordinal: locationResponse[i].current_interaction!.ordinal!)
            }
            let credit_rating: String = locationResponse[i].credit_rating ?? "0-0"
            let income: String = locationResponse[i].income ?? "$0 - $0"
            let net_worth: String = locationResponse[i].net_worth ?? "$0 - $0"
             let age: String = "34"
            let firstName: String = locationResponse[i].first_name ?? ""
            let lastName: String = locationResponse[i].last_name ?? ""
            let gender:String =  locationResponse[i].gender ?? "Male"
            let customerId: Int = locationResponse[i].id!
            let mobilePhone: String = locationResponse[i].phone ?? ""
            let personalEmail: String = locationResponse[i].email ?? ""
            let resident: Bool = true
            let address_id: Int = id
                
            if(firstName != ""){
                customersList.append(Customer(id: customerId, firstName: firstName, lastName: lastName, mobilePhone: mobilePhone, personalEmail: personalEmail, resident: resident, age: age, net_worth: net_worth, credit_rating: credit_rating, genderStr: gender, income: income, address_id: address_id))
            }
            
            
            let position = CLLocationCoordinate2D(latitude: location.latitude!, longitude: location.longitude!)
            let addressInteraction: InteractionAddress = InteractionAddress(address: address, interaction: interaction!, customers: customersList)
            
            let markerView = UIImage(named: "ic_no_interaction_48dp")
            
            
            let clusterItem : AddressInteractionClusterItem = AddressInteractionClusterItem(addressInteraction: addressInteraction, index: i,position: position,name: addressLine,image: markerView!)
            
            self.items.append(clusterItem)
            
        }
        
    }
    
    //Manage Data On Camera Idle
    func manageDataOnAddAddress(locationResponse: InteractionAddressDataForAddress) {
            
        let customersList: [Customer] = [];
        
            
        var city: String = "";
        var country: String = "";
        let addressLine: String = locationResponse.address ?? ""
        city = locationResponse.city!.name ?? ""
        if(locationResponse.city!.state != nil){
            country = locationResponse.city!.state!.name ?? "USA"
        }else
        {
            country = "USA"
        }
        
        let id:CLong = locationResponse.id!
        
        let latitude = Double(locationResponse.latitude!)
        let longitude = Double(locationResponse.longitude!)
        let location:Location = Location(latitude: latitude!, longitude: longitude!)
        let number = locationResponse.address!.westernArabicNumeralsOnly
        let postal = locationResponse.zipcode ?? "0"
        let region = locationResponse.region ?? ""
        let street = locationResponse.address ?? ""
        let unit = locationResponse.unit ?? ""
        
        let address: Address = Address(id: id, number: number , street: street, unit: unit, city: city, region: region, postal: String(postal), country: country, location: location, addressLine: addressLine)
        
        var interaction: Interaction? = nil;
        if(locationResponse.current_interaction == nil)
        {
            interaction = Interaction(id: 1, type: "", name: "None", icon: "", color: "#FFFFFF", ordinal: 1)
        }else
        {
            interaction = Interaction(id: locationResponse.current_interaction!.id!, type: locationResponse.current_interaction!.type!, name: locationResponse.current_interaction!.name!, icon: locationResponse.current_interaction!.icon!, color: locationResponse.current_interaction!.color!, ordinal: locationResponse.current_interaction!.ordinal!)
        }
        
        
        let position = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
        let addressInteraction: InteractionAddress = InteractionAddress(address: address, interaction: interaction!, customers: customersList)
        
        let markerView = UIImage(named: "ic_no_interaction_48dp")
        
        
        let clusterItem : AddressInteractionClusterItem = AddressInteractionClusterItem(addressInteraction: addressInteraction, index:items.count,position: position,name: addressLine,image: markerView!)
        
        self.clusterManager!.cluster()
        self.items.append(clusterItem)
        self.clusterManager!.clearItems()
        self.clusterManager!.add(items)
        self.clusterManager!.cluster()
            
        
    }
    
    // Render Cluster Item Icons on Google Map
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if marker.userData! is AddressInteractionClusterItem
        {
            let customerClusterItem = (marker.userData! as! AddressInteractionClusterItem)
            
            if customerClusterItem.addressInteraction?.interaction?.name == "None"
            {
                let image = Utils.resizeImage(image: customerClusterItem.image!, sizeImage: CGSize(width: 23, height: 23))
                
                let imageFinal = Utils.textToImageForMarker(drawText: (customerClusterItem.addressInteraction?.address!.number!)! as NSString, inImage: image)
                
                marker.icon = imageFinal
               // marker.icon = Utils.resizeImage(image: Utils.resizeImage(image: Utils.textToImageForMarker(drawText: customerClusterItem.addressInteraction?.address!.number as! NSString, inImage: customerClusterItem.image!), sizeImage: CGSize(width: 30, height: 30)), sizeImage: CGSize(width: 30, height: 50))
                
            }else
            {
                let color = customerClusterItem.addressInteraction!.interaction!.color!
                let icon = customerClusterItem.addressInteraction!.interaction!.icon!
                let tmpImage = Utils.customImage(diameter: 23, color: Utils.hexStringToUIColor(hex: color))
                marker.icon = Utils.textToImage(drawText: icon as NSString, inImage: Utils.textToImageForMarker(drawText: (customerClusterItem.addressInteraction?.address!.number!)! as NSString, inImage: tmpImage))
            }
        }
        else if marker.userData! is AreaClusterItem
        {
            var width = 70
            let areaClusterItem = (marker.userData! as! AreaClusterItem)
            if((areaClusterItem.area?.name?.count)! > 6)
            {
                if((areaClusterItem.area?.name?.count)! > 6 && ((areaClusterItem.area?.name?.count)!) <= 9)
                {
                    width = 85
                }
                if((areaClusterItem.area?.name?.count)! > 9 && ((areaClusterItem.area?.name?.count)!) <= 12)
                {
                    width = 100
                }
                if((areaClusterItem.area?.name?.count)! > 12 && ((areaClusterItem.area?.name?.count)!) <= 15)
                {
                    width = 105
                }
                if((areaClusterItem.area?.name?.count)! > 15 && ((areaClusterItem.area?.name?.count)!) <= 18)
                {
                    width = 120
                }
                if((areaClusterItem.area?.name?.count)! > 18 && ((areaClusterItem.area?.name?.count)!) <= 21)
                {
                    width = 135
                }
                else
                {
                    width = 150
                }
            }
            let tmpImage = Utils.resizeImage(image: UIImage(named: "bubble")!, sizeImage: CGSize(width: width, height: 25))
            marker.icon = Utils.drawText(text: (areaClusterItem.area?.name!)! as NSString, inImage: tmpImage)
        }
    }
    
    
    func getAreas()
    {
        do{
            let header = [
                "Accept": "text/json",
                "Authorization": "bearer \(SessionUtils.getToken())"
            ]
            
            let resourceString = "\(Utils.requestURL)api/areas";
            
            Alamofire.request(resourceString, method: .get, parameters: nil, headers: header).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    if let httpURLResponse = response.response{
                        let response_code = httpURLResponse.statusCode;
                        if response_code == 200 {
                            do{
                                let json = JSON(value)
                                if Constants.LOGGING_ENABLED{
                                    print(value)}
                                let str = String(describing: json);
                                let jsonData = str.data(using: .utf8)
                                let decoder = JSONDecoder();
                                let res = try decoder.decode([AreasResponse].self, from: jsonData!)
                                self.mainAreaResponse = res
                                self.showAreas(area_re: self.mainAreaResponse, officeIndex: self.office_index)
                                
                                //Manage Data Comes from server
                                
                            }catch{
                                if Constants.LOGGING_ENABLED{
                                    print(error)}
                                
                            }
                            
                            
                        }else if response_code == 401{
                            
                            Toast.show(message: "Unauthorized User", controller: self)
                            let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            self.navigationController?.pushViewController(viewController!, animated: false);
                            self.present(viewController!,animated: true,completion: nil)
                            
                        }else{
                            if Constants.LOGGING_ENABLED{
                                print("Error - \(httpURLResponse.statusCode)")}
                            Toast.show(message: "No Internet Connection/Server Issue", controller: self)
                        }
                    }
                case .failure(let error):
                    if Constants.LOGGING_ENABLED{
                        print(error)}
                    Toast.show(message: "There is Some Server Issue", controller: self)
                    
                }
                
            })
            
        }
    }
    var office_index:Int  = 0
    var area_response: [AreasResponse] = [];
    func showAreas(area_re: [AreasResponse],officeIndex: Int)
    {
        var areaDataList: [AreaData] = []
        area_response = area_re
        areaItems = []
        clusterManager?.clearItems()
        clusterManager?.cluster()
        
        for i in (0..<polygonsList.count)
        {
            polygonsList[i].map = nil
        }
        
        polygonsList = []
        
        if(area_response.count > office_index)
        {
            var created_at: Date?
            for i in (0..<area_response[officeIndex].areas!.count)
            {
                areaDataList.append(area_response[officeIndex].areas![i])
            }
            
            if(area_response[officeIndex].created_areas != nil)
            {
                for i in (0..<area_response[officeIndex].created_areas!.count)
                {
                    var checkIfExist:Bool = false
                    for j in (0..<areaDataList.count)
                    {
                        if(areaDataList[j].id == area_response[officeIndex].created_areas![i].id)
                        {
                            checkIfExist = true
                        }
                    }
                    if(!checkIfExist)
                    {
                        areaDataList.append(area_response[officeIndex].created_areas![i])
                    }
                }
                
            }
            
            for i in (0..<areaDataList.count)
            {
                do{
                    var modified_at: Date?
                    let formattor =  DateFormatter();
                    formattor.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    created_at = formattor.date(from: areaDataList[i].created_at!)
                    modified_at = formattor.date(from: areaDataList[i].updated_at!)
                    
                    var locationList: [Location] = []
                    var latlngs: [LatLngs] = []
                    let jsonLocation = JSON(areaDataList[i].geography!)
                    let strValue = String(describing: jsonLocation)
                    let jsonLocationData = strValue.data(using: .utf8)
                    let decoder = JSONDecoder();
                    latlngs = try decoder.decode([LatLngs].self, from: jsonLocationData!)
                    
                    for j in (0..<latlngs.count)
                    {
                        locationList.append(Location(latitude: latlngs[j].latitude!, longitude: latlngs[j].longitude!))
                    }
                    var employees: [Employee] = []
                    for j in (0..<areaDataList[i].users!.count)
                    {
                        
                        let created = formattor.date(from: areaDataList[i].users![j].created_at!)!
                        let modified = formattor.date(from: areaDataList[i].users![j].updated_at!)!
                        let id = areaDataList[i].users![j].parent_id!;
                        if Constants.LOGGING_ENABLED{
                            print(id)}
                        employees.append(Employee(id: areaDataList[i].users![j].id!, firstName: areaDataList[i].users![j].first_name ?? "",lastName: areaDataList[i].users![j].last_name ?? "", email: areaDataList[i].users![j].email!, created: created, modified: modified, type: areaDataList[i].users![j].role!.name!,offices: areaDataList[i].users![j].companies ?? [],parent: areaDataList[i].users![j].parent_id!,phone: areaDataList[i].users![j].phone ?? ""))
                    }
                    
                    
                    let area = Area(id: areaDataList[i].id!, name: areaDataList[i].name!, color: areaDataList[i].color!, created: created_at!, modified: modified_at!, coordinates: locationList,employees: employees, total_addresses: areaDataList[i].total_addresses ?? 0)
                    
                    //Manager Area Clusters
                    let path = GMSMutablePath()
                    for i in (0..<area.coordinates!.count)
                    {
                        path.add(CLLocationCoordinate2D(latitude: area.coordinates![i].latitude!, longitude: area.coordinates![i].longitude!))
                    }
                    
                    if(SessionUtils.getUserType() != "Sales Rep")
                    {
                        var coordinates: [CLLocationCoordinate2D] = []
                        for i in (0..<area.coordinates!.count)
                        {
                            coordinates.append(CLLocationCoordinate2D(latitude: area.coordinates![i].latitude!, longitude: area.coordinates![i].longitude!))
                        }
                        let center = self.geographicMidpoint(betweenCoordinates: coordinates)
                        
                        let markerView = UIImage(named: "bubble")
                        
                        let clusterItem : AreaClusterItem = AreaClusterItem(area: area, index: i,position: center, marker: markerView!)
                        self.areaItems.append(clusterItem)
                    }
                    
                    let polygon = GMSPolygon(path: path)
                    polygon.strokeColor = UIColor(hexString: area.color!,alpha: 1.0)
                    polygon.fillColor  = UIColor(hexString: area.color!,alpha: 0.3)
                    polygon.strokeWidth = 2.0
                    polygon.map = self.google_map
                    polygonsList.append(polygon)
                    
                }catch{
                    if Constants.LOGGING_ENABLED{
                        print(error)}}
            }
            areaClusterManager?.clearItems()
            areaClusterManager?.cluster()
            areaClusterManager?.add(areaItems)
            areaClusterManager?.cluster()
            
            
            
            
        }
        else
        {
            Toast.show(message: "No Areas Available to show", controller: self)
        }
        
    }
    
    // Address Inoformation Bottom View Configurations and Settings
    func setUpCard()
    {
        visualEffectView = UIVisualEffectView()
        visualEffectView.frame = self.view.frame
        
        //self.view.addSubview(visualEffectView)
        
        cardHandleAreaHeight = self.view.frame.height/2
        
        addressInformationController = UIStoryboard.init(name: "AddressInformation", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddressInformationViewController") as? AddressInformationViewController
        addressInformationController.homeViewController = self
        
        self.addChild(addressInformationController)
        self.view.insertSubview(addressInformationController.view, at:4)
        
        addressInformationController.view.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.bounds.width, height: self.view.frame.height
        )
        
        addressInformationController.view.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleCardTap(recognizer:)))
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleCardPan(recognizer:)))
        
        addressInformationController.handleArea.addGestureRecognizer(tapGestureRecognizer)
        addressInformationController.handleArea.addGestureRecognizer(panGestureRecognizer)
        
        //addressInformationController.handleArea2.addGestureRecognizer(tapGestureRecognizer)
        //addressInformationController.handleArea2.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    func setUpAreaCard()
    {
        visualEffectAreaView = UIVisualEffectView()
        visualEffectAreaView.frame = self.view.frame
        
        cardHandleAreaHeight = self.view.frame.height/2
        
        areaDetailController = UIStoryboard.init(name: "AreaDetail", bundle: Bundle.main).instantiateViewController(withIdentifier: "AreaDetailViewController") as? AreaDetailViewController
        areaDetailController.homeViewController = self
        areaDetailController.deleteAreaDelegate = self
        areaDetailController.editAreaDelegate = self
        self.addChild(areaDetailController)
        self.view.insertSubview(areaDetailController.view, at:4)
        
        areaDetailController.view.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.bounds.width, height: self.view.frame.height
        )
        
        areaDetailController.view.clipsToBounds = true
        
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleAreaCardPan(recognizer:)))
        
        areaDetailController.handleArea.addGestureRecognizer(panGestureRecognizer)
        
    }
    
    @objc
    func handleCardTap(recognizer: UITapGestureRecognizer) {
        
    }
    
    
    @objc
    func handleCardPan(recognizer: UIPanGestureRecognizer) {
        
        //print(recognizer.velocity(in: view).y)
        switch recognizer.state {
        case .began:
            //Start transition
            startInteractiveTransition(state: nextState, duration: 0.9,velocity:Int(recognizer.velocity(in: view).y))
        case .changed:
            //Update Transition
            updateInteractiveTransition(fractionCompleted: 0)
        case .ended:
            //Continue Transion
            continueInteractiveTransition()
        default:
            break
        }
        
    }
    
    @objc
    func handleAreaCardPan(recognizer: UIPanGestureRecognizer) {
        
        //print(recognizer.velocity(in: view).y)
        switch recognizer.state {
        case .began:
            //Start transition
            startInteractiveTransitionArea(state: nextState, duration: 0.1,velocity:Int(recognizer.velocity(in: view).y))
        case .changed:
            //Update Transition
            updateInteractiveTransitionArea(fractionCompleted: 0)
        case .ended:
            //Continue Transion
            continueInteractiveTransitionArea()
        default:
            break
        }
        
    }
    
    func animateTransitionIfNeeded(state:CardState,duration:TimeInterval,velocity: Int)
    {
        if runningAnimations.isEmpty
        {
           // let fr = UIViewPropertyAnimator(\)
            let frameAnimator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 0.8)
            {
                let height = self.addressInformationController.view.frame.origin.y
                if(velocity < 0)
                {
                    if Constants.LOGGING_ENABLED{
                        print("Moving Up")}
                    //Moving Up
                    if height == self.cardHandleAreaHeight
                    {
                        self.halfExpanded = false
                        self.cardVisible = true
                        self.addressInformationController.view.frame.origin.y = 0
                    }
                }
                else
                {
                    if Constants.LOGGING_ENABLED{
                        print("Moving Down")}
                    //Moving Down
                    if height == 0
                    {
                        if Constants.LOGGING_ENABLED{
                        print("Moving Down0")}
                        self.halfExpanded = true
                        self.cardVisible = true
                        self.addressInformationController.view.frame.origin.y = self.cardHandleAreaHeight
                    }
                    else if height == self.cardHandleAreaHeight
                    {
                        if Constants.LOGGING_ENABLED{
                        print("Moving Down1")}
                        self.halfExpanded = false
                        self.cardVisible = false
                        self.addressInformationController.view.frame.origin.y = self.view.frame.height
                        self.dismissCurrentMarker()
                    }
                }
                
            }
            
            frameAnimator.addCompletion { _ in
                self.cardVisible = !self.cardVisible
                self.runningAnimations.removeAll()
            }
            
            frameAnimator.startAnimation()
            runningAnimations.append(frameAnimator)
        }
    }
    
    func animateTransitionIfNeededArea(state:CardState,duration:TimeInterval,velocity: Int)
    {
        if runningAnimations.isEmpty
        {
            let frameAnimator = UIViewPropertyAnimator(duration: 0.4, dampingRatio: 0.8)
            {
                let height = self.areaDetailController.view.frame.origin.y
                if(velocity < 0)
                {
                    if Constants.LOGGING_ENABLED{
                        print("Moving Up")}
                    //Moving Up
                    if height == self.cardHandleAreaHeight
                    {
                        self.areaHalfExpanded = false
                        self.areaCardVisible = true
                        self.areaDetailController.view.frame.origin.y = 0
                    }
                }
                else
                {
                    if Constants.LOGGING_ENABLED{
                        print("Moving Down")}
                    //Moving Down
                    if height == 0
                    {
                        if Constants.LOGGING_ENABLED{
                            print("Moving Down0")}
                        self.areaHalfExpanded = true
                        self.areaCardVisible = true
                        self.areaDetailController.view.frame.origin.y = self.cardHandleAreaHeight
                    }
                    else if height == self.cardHandleAreaHeight
                    {
                        if Constants.LOGGING_ENABLED{
                            print("Moving Down1")}
                        self.areaHalfExpanded = false
                        self.areaCardVisible = false
                        self.areaDetailController.view.frame.origin.y = self.view.frame.height
                    }
                }
                
            }
            
            frameAnimator.addCompletion { _ in
                self.areaCardVisible = !self.areaCardVisible
                self.runningAnimations.removeAll()
            }
            
            frameAnimator.startAnimation()
            runningAnimations.append(frameAnimator)
        }
    }
    
    func startInteractiveTransition(state:CardState,duration:TimeInterval,velocity: Int)
    {
        if runningAnimations.isEmpty
        {
            animateTransitionIfNeeded(state: state, duration: duration,velocity: velocity)
        }
        for animator in runningAnimations
        {
            animator.pauseAnimation()
            animationProgressWhenInterupted = animator.fractionComplete
        }
    }
    
    func startInteractiveTransitionArea(state:CardState,duration:TimeInterval,velocity: Int)
    {
        if runningAnimations.isEmpty
        {
            animateTransitionIfNeededArea(state: state, duration: duration,velocity: velocity)
        }
        for animator in runningAnimations
        {
            animator.pauseAnimation()
            animationProgressWhenInterupted = animator.fractionComplete
        }
    }
    
    func updateInteractiveTransition(fractionCompleted: CGFloat)
    {
        for animator in runningAnimations
        {
            animator.fractionComplete = fractionCompleted + animationProgressWhenInterupted
        }
    }
    
    func updateInteractiveTransitionArea(fractionCompleted: CGFloat)
    {
        for animator in runningAnimations
        {
            animator.fractionComplete = fractionCompleted + animationProgressWhenInterupted
        }
    }
    
    func continueInteractiveTransition()
    {
        for animator in  runningAnimations
        {
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
    
    func continueInteractiveTransitionArea()
    {
        for animator in  runningAnimations
        {
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }
    
    func deleteCustomer(index: Int, clusterIndex:Int)
    {
        items[clusterIndex].addressInteraction?.customers!.remove(at: index)
    }
    
    
    func updateStatus(clusterIndex: Int, interaction:Interaction)
    {
        let clusterItem = items[clusterIndex]
        clusterItem.addressInteraction?.interaction = interaction
        items.remove(at: clusterIndex)
        items.append(clusterItem)
        clusterManager?.clearItems()
        clusterManager?.add(items)
        clusterManager?.cluster()
        
    }
    
    func showAreaFullPage()
    {
        self.areaHalfExpanded = false
        self.areaCardVisible = true
        self.areaDetailController.view.frame.origin.y = 0
    }
    
    func addCustomer(clusterIndex: Int, customer:Customer)
    {
        items[clusterIndex].addressInteraction?.customers?.append(customer)
        
    }
    
    func editCustomer(clusterIndex: Int, customer:Customer,customerIndex: Int)
    {
        if((items[clusterIndex].addressInteraction?.customers!.count)! > 0){
            items[clusterIndex].addressInteraction?.customers![customerIndex] = customer
            
        }
        else{
            items[clusterIndex].addressInteraction?.customers!.append(customer)
        }
        
    }
    
    func geographicMidpoint(betweenCoordinates coordinates: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D {
        
        guard coordinates.count > 1 else {
            return coordinates.first ?? // return the only coordinate
                CLLocationCoordinate2D(latitude: 0, longitude: 0) // return null island if no coordinates were given
        }
        
        var x = Double(0)
        var y = Double(0)
        var z = Double(0)
        
        for coordinate in coordinates {
            let lat = coordinate.latitude * .pi / 180.0
            let lon = coordinate.longitude * .pi / 180.0
            x += cos(lat) * cos(lon)
            y += cos(lat) * sin(lon)
            z += sin(lat)
        }
        
        x /= Double(coordinates.count)
        y /= Double(coordinates.count)
        z /= Double(coordinates.count)
        
        let lon = atan2(y, x)
        let hyp = sqrt(x * x + y * y)
        let lat = atan2(z, hyp)
        
        return CLLocationCoordinate2D(latitude: lat * 180.0 / .pi, longitude: lon * 180.0 / .pi)
    }
    
    func createPolygonFromTheDrawablePoints(){
        //let numberOfPoints = self.drawCoordinates.count
        addPolyGonInMapView(drawableLoc: drawCoordinates) //neglects a single touch
    }
    
    func  addPolyGonInMapView( drawableLoc:[CLLocationCoordinate2D]){
        drawPolygon.map = nil
        isDrawingModeEnabled = true
        let path = GMSMutablePath()
        for loc in drawableLoc{
            
            path.add(loc)
            
        }
        drawPolygon = GMSPolygon(path: path)
        drawPolygon.strokeWidth = 2
        
        drawPolygon.strokeColor = UIColor(hexString: selectedColor)
        drawPolygon.fillColor = UIColor(hexString: selectedColor).withAlphaComponent(0.4)
        drawPolygon.map = google_map
    }
    
    func addPolygonDeleteAnnotation(endCoordinate location:CLLocationCoordinate2D,polygon:GMSPolygon){
        
        let marker = DeleteMarker(location: location,polygon: polygon)
        let deletePolygonView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        deletePolygonView.layer.cornerRadius = 15
        deletePolygonView.backgroundColor = UIColor.white
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 16, height: 16))
        imageView.center = deletePolygonView.center
        imageView.image = UIImage(named: "delete")
        deletePolygonView.addSubview(imageView)
        marker.iconView = deletePolygonView
        marker.map = google_map
        polygonDeleteMarkers.append(marker)
    }
    
    
    
}


extension HomeViewController: MyAreaClickDelegate
{
    func selectedMyArea(area: Area) {
        self.navigationController?.popViewController(animated: true)
        //Draw Dummy Area
        //let path = GMSMutablePath()
        //let bounds = GMSCoordinateBounds.init()
        var coordinates: [CLLocationCoordinate2D] = []
        for i in (0..<area.coordinates!.count)
        {
            coordinates.append(CLLocationCoordinate2D(latitude: area.coordinates![i].latitude!, longitude: area.coordinates![i].longitude!))
        }
        
        let centerCoordinate = geographicMidpoint(betweenCoordinates: coordinates)
        
        let camera = GMSCameraPosition.camera(withLatitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude, zoom: 15.0)
        google_map.camera = camera
        /* let polygon = GMSPolygon(path: path)
         polygon.strokeColor = UIColor(hexString: area.color!,alpha: 1.0)
         polygon.fillColor  = UIColor(hexString: area.color!,alpha: 0.3)
         polygon.strokeWidth = 3.0
         polygon.map = self.google_map*/
        
        
    }
    
    
    
    
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = colorCollectionView.dequeueReusableCell(withReuseIdentifier: "colorcell", for: indexPath) as! ColorCell
        
        cell.colorImageView.layer.cornerRadius = cell.colorImageView.frame.height/2
        
        cell.colorImageView.backgroundColor = UIColor(hexString: colorList[indexPath
            .row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedColor = colorList[indexPath.row]
        if drawPolygon != nil && drawCoordinates.count > 0{
        drawPolygon.strokeColor = UIColor(hexString: selectedColor)
        drawPolygon.fillColor = UIColor(hexString: selectedColor).withAlphaComponent(0.4)
        drawPolygon.map = google_map
        }
    }
    
    
    
    
}

extension HomeViewController:NotifyTouchEvents{
    
    func touchBegan(touch:UITouch){
        if Constants.LOGGING_ENABLED{
        print("drawpolygon")
        }
        let location = touch.location(in: self.google_map)
        let coordinate = self.google_map.projection.coordinate(for: location)
        self.drawCoordinates.append(coordinate)
        drawCoordinates = []
        createPolygonFromTheDrawablePoints()
        
    }
    
    func touchMoved(touch:UITouch){
        
        let location = touch.location(in: self.google_map)
        let coordinate = self.google_map.projection.coordinate(for: location)
        self.drawCoordinates.append(coordinate)
        createPolygonFromTheDrawablePoints()
        
    }
    
    func touchEnded(touch:UITouch){
        
        let location = touch.location(in: self.google_map)
        let coordinate = self.google_map.projection.coordinate(for: location)
        self.drawCoordinates.append(coordinate)
        //createPolygonFromTheDrawablePoints()
        //drawPolygon?.map = nil
    }
    
}


class ColorCell: UICollectionViewCell
{
    
    @IBOutlet weak var colorImageView: UIImageView!
}


extension HomeViewController: AddressSearchDelegate
{
    func addressSearchResult(address: InteractionAddressData) {
        self.navigationController?.popToViewController(self, animated: false)
        if(address.latitude != "" && address.longitude != "")
        {
            let latitude = Double(address.latitude!)
            let longitude = Double(address.longitude!)
            if(latitude != 0.0 && longitude != 0.0)
            {
                let positon = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!), zoom: 19.7)
                google_map.animate(to: positon)
            }
        }
    }
    
    
}

extension HomeViewController: CustomerSearchDelegate
{
    func customerSearchResult(customerResult: InteractionAddressData) {
        let address = customerResult
        self.navigationController?.popToViewController(self, animated: false)
        if(address.latitude != "" && address.longitude != "")
        {
            let latitude = Double(address.latitude!)
            let longitude = Double(address.longitude!)
            if(latitude != 0.0 && longitude != 0.0)
            {
                let positon = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!), zoom: 19.7)
                google_map.animate(to: positon)
            }
        }
    }
    
    
}

extension HomeViewController: CreateAreaDelegate
{
    func createArea(area: CreateAreaResponse, users: [UsersResponse]) {
        self.navigationController?.popViewController(animated: true)
        
        do{
            let formattor =  DateFormatter();
            formattor.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let created_at = formattor.date(from: area.created_at!)
            let modified_at = formattor.date(from: area.updated_at!)
            
            var locationList: [Location] = []
            var latlngs: [LatLngs] = []
            let jsonLocation = JSON(area.geography!)
            let strValue = String(describing: jsonLocation)
            let jsonLocationData = strValue.data(using: .utf8)
            let decoder = JSONDecoder();
            latlngs = try decoder.decode([LatLngs].self, from: jsonLocationData!)
            
            for j in (0..<latlngs.count)
            {
                locationList.append(Location(latitude: latlngs[j].latitude!, longitude: latlngs[j].longitude!))
            }
            var employees: [Employee] = []
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            for j in (0..<users.count)
            {
                
                let created = formatter.date(from: users[j].created_at!)!
                let modified = formatter.date(from: users[j].updated_at!)!
                employees.append(Employee(id: users[j].id!, firstName: users[j].first_name ?? "",lastName:  users[j].last_name ?? "", email: users[j].email!, created: created, modified: modified, type: users[j].role!.name!,offices: users[j].companies ?? [],parent: users[j].parent_id!,phone: users[j].phone ?? ""))
            }
            
            
            let area = Area(id: area.id!, name: area.name!, color: area.color!, created: created_at!, modified: modified_at!, coordinates: locationList,employees: employees)
            
            //Manager Area Clusters
            let path = GMSMutablePath()
            var coordinates: [CLLocationCoordinate2D] = []
            for i in (0..<area.coordinates!.count)
            {
                path.add(CLLocationCoordinate2D(latitude: area.coordinates![i].latitude!, longitude: area.coordinates![i].longitude!))
                
                coordinates.append(CLLocationCoordinate2D(latitude: area.coordinates![i].latitude!, longitude: area.coordinates![i].longitude!))
            }
            
            if(SessionUtils.getUserType() != "Sales Rep")
            {
                var coordinates: [CLLocationCoordinate2D] = []
                for i in (0..<area.coordinates!.count)
                {
                    coordinates.append(CLLocationCoordinate2D(latitude: area.coordinates![i].latitude!, longitude: area.coordinates![i].longitude!))
                }
                let center = self.geographicMidpoint(betweenCoordinates: coordinates)
                
                let markerView = UIImage(named: "bubble")
                
                let clusterItem : AreaClusterItem = AreaClusterItem(area: area, index: areaItems.count,position: center, marker: markerView!)
                self.areaItems.append(clusterItem)
            }
            
            let centerCoordinate = geographicMidpoint(betweenCoordinates: coordinates)
            
            let camera = GMSCameraPosition.camera(withLatitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude, zoom: 17.0)
            google_map.camera = camera
            
            let polygon = GMSPolygon(path: path)
            polygon.strokeColor = UIColor(hexString: area.color!,alpha: 1.0)
            polygon.fillColor  = UIColor(hexString: area.color!,alpha: 0.3)
            polygon.strokeWidth = 2.0
            polygon.map = self.google_map
            polygonsList.append(polygon)
            
        }catch{
            if Constants.LOGGING_ENABLED{
                print(error)}}
        areaClusterManager!.clearItems()
        areaClusterManager!.cluster()
        areaClusterManager!.add(areaItems)
        areaClusterManager!.cluster()
        disableDrawEvents()
        Toast.show(message: "\(area.name!) created successfully", controller: self)
        
    }
    
    
}

extension HomeViewController: EditAreaDelegate
{
    func editArea(areaResponse: CreateAreaResponse, index: Int,employees: [Employee]) {
        self.areaItems[index].area!.name = areaResponse.name
        self.areaItems[index].area!.employees = employees
        self.areaClusterManager?.cluster()
        self.areaClusterManager?.clearItems()
        self.areaClusterManager?.add(areaItems)
        self.areaClusterManager?.cluster()
    }
    
}

extension HomeViewController: DeleteAreaDelegate
{
    func deleteArea(index: Int) {
        self.areaHalfExpanded = false
        self.areaCardVisible = false
        self.areaDetailController.view.frame.origin.y = self.view.frame.height
        dismissCurrentMarker()
        Toast.show(message: "Area deleted successfully", controller: self)
        
        var areaItemsTmp: [AreaClusterItem] = []
        areaClusterManager?.clearItems()
        areaClusterManager?.cluster()
        //areaClusterManager?.remove(areaItems[index])
        areaItems.remove(at: index)
        
        polygonsList[index].map = nil
        polygonsList.remove(at: index)
        
        for i in (0..<areaItems.count)
        {
            areaItemsTmp.append(AreaClusterItem(area: areaItems[i].area!, index: i, position: areaItems[i].position, marker: areaItems[i].marker))
        }
        areaItems = []
        areaItems = areaItemsTmp
        areaClusterManager?.add(areaItems)
        areaClusterManager?.cluster()
        
    }
    
    
}

extension HomeViewController: ChangeMapDelegate{
    func changeMap() {
        if SessionUtils.getMapType() == 1  || SessionUtils.getMapType() == 0 {
            google_map.mapType = GMSMapViewType.normal
        }
        else if SessionUtils.getMapType() == 2{
            google_map.mapType = GMSMapViewType.satellite
        }
        else if SessionUtils.getMapType() == 3 {
            google_map.mapType = GMSMapViewType.terrain
        }
        else if SessionUtils.getMapType() == 4 {
            google_map.mapType = GMSMapViewType.hybrid
        }
        else{
            google_map.mapType = GMSMapViewType.hybrid
        }
    }
    
    
}


extension String {
    var westernArabicNumeralsOnly: String {
            return filter { "0"..."9" ~= $0 }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = addressListTable.dequeueReusableCell(withIdentifier: "addresslist", for: indexPath) as! AddressListCell
        cell.label.text = addressList[indexPath.row].addressInteraction!.address!.street ?? "Unknown"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("hello")
        dismissAddAddressView()
        self.rightSideMenu.isHidden = true
        self.addressListPopup.removeFromSuperview()
        let addressInteractionClusterItem = addressList[indexPath.row]
        var zoom = 17.0
        if self.google_map.camera.zoom >= 17.0{
            zoom = Double(self.google_map.camera.zoom)
        }
        self.google_map.camera = GMSCameraPosition(latitude: addressInteractionClusterItem.addressInteraction!.address!.location!.latitude! - 0.000060, longitude: addressInteractionClusterItem.addressInteraction!.address!.location!.longitude!, zoom: Float(zoom))
        
        dismissCurrentMarker()
        cardVisible = true
        halfExpanded = true
        self.areaHalfExpanded = false
        self.areaCardVisible = false
        self.areaDetailController.view.frame.origin.y = self.view.frame.height
        self.showCurrentMarker(latitude: addressInteractionClusterItem.addressInteraction!.address!.location!.latitude!, longitude: addressInteractionClusterItem.addressInteraction!.address!.location!.longitude!,number: addressInteractionClusterItem.addressInteraction?.address?.number ?? "")
        self.addressInformationController.view.frame.origin.y = self.cardHandleAreaHeight
        self.addressInformationController.getAddressHistory(address_id: (addressInteractionClusterItem.addressInteraction?.address?.id)!)
        self.addressInformationController.loadInformation(addressInteraction: addressInteractionClusterItem.addressInteraction!, index: addressInteractionClusterItem.index!)
        
        
    }
    
}

class AddressListCell: UITableViewCell
{
    
    @IBOutlet weak var label: UILabel!
}
