//
//  MainViewViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 27/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import GoogleMaps
import GoogleMapsUtils
import Alamofire
import SwiftyJSON

class MainViewViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate, GMUClusterManagerDelegate, GMUClusterRendererDelegate {

    
    @IBOutlet weak var logoutMenuItem: UIView!
    @IBOutlet weak var drawerEmailLabel: UILabel!
    @IBOutlet weak var drawerNameLabel: UILabel!
    
    @IBOutlet weak var leftLeadingAnchor: NSLayoutConstraint!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var topMainView: UIView!
    @IBOutlet weak var rightSideMenu: UIView!
    
    @IBOutlet weak var progressDialog: UIActivityIndicatorView!
    @IBOutlet weak var drawerMenu: UIView!
    @IBOutlet weak var google_map: GMSMapView!
    var locationManager : CLLocationManager = CLLocationManager()
    
    let isClustering: Bool = true;
    let isCustom: Bool = true;
    
    var targetLatLong:CLLocationCoordinate2D? = nil
    var items: [AddressInteractionClusterItem] = [];
    
    var clusterManager: GMUClusterManager?
     var locationResponse: [InteractionAddressData] = [];
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.drawerNameLabel.text = SessionUtils.getName()
        self.drawerEmailLabel.text = SessionUtils.getEmail()
        self.rightSideMenu.isHidden = true
        setUpGoogleMap()
        setUpExtraGestures()
        setUpDrawerMenuItemGestures()
        progressDialog.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    func setUpGoogleMap()
    {
        //show_marker(position: google_map.camera.target)
        //let camera = GMSCameraPosition.camera(withLatitude: 41.63072254, longitude: -72.66435136302, zoom: 12.0)
        //let google_map = GMSMapView.map(withFrame: .zero, camera: camera)
        google_map.settings.compassButton = true
        google_map.isMyLocationEnabled = true;
        google_map.settings.myLocationButton = true
        google_map.delegate = self
        locationManager.delegate  = self
        locationManager.requestWhenInUseAuthorization()
        
        if SessionUtils.getMapType() == 1  || SessionUtils.getMapType() == 0 {
            google_map.mapType = GMSMapViewType.init(rawValue: 1)!
        }
        else if SessionUtils.getMapType() == 2{
            google_map.mapType = GMSMapViewType.init(rawValue: 2)!
        }
        else if SessionUtils.getMapType() == 3 {
            google_map.mapType = GMSMapViewType.init(rawValue: 3)!
        }
        else if SessionUtils.getMapType() == 4 {
            google_map.mapType = GMSMapViewType.init(rawValue: 4)!
        }
        else if SessionUtils.getMapType() == 5 {
            google_map.mapType = GMSMapViewType.init(rawValue: 5)!
        }
        //google_map.translatesAutoresizingMaskIntoConstraints = true
        
        //self.view = self.mainView
        //Configure Cluster Manager For Locations
        if isClustering{
            
            var iconGenerator = GMUDefaultClusterIconGenerator()
            if(isCustom)
            {
                var images: [UIImage] = []
                images.append(Utils.resizeImage(image: UIImage(named: "clustermarker")!, sizeImage: CGSize(width: 25, height: 25)))
                iconGenerator = GMUDefaultClusterIconGenerator(buckets: [10], backgroundImages: images)
            }
            let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
            let renderer = GMUDefaultClusterRenderer(mapView: google_map, clusterIconGenerator: iconGenerator)
            renderer.delegate = self
            clusterManager = GMUClusterManager(map: google_map, algorithm: algorithm, renderer: renderer)
            clusterManager!.cluster()
            clusterManager!.setDelegate(self, mapDelegate: self)
        }
        //animateCamera()
        
        
        
        
        
    }
    
    func animateCamera()
    {
        let camera = GMSCameraPosition.camera(withLatitude: 41.63072254, longitude: -72.66435136302, zoom: 12.0)
        self.google_map.animate(to: camera)
        
    }
    
    

    @IBAction func menuBtnAction(_ sender: Any) {
        self.leftLeadingAnchor.constant = 0;
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        DispatchQueue.main.async {
            self.rightSideMenu.isHidden = false
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("Latitude: \(mapView.camera.target.latitude) Longitude: \(mapView.camera.target.longitude)")
        
        if(progressDialog.isHidden == true)
        {
            let cameraPosition: GMSCameraPosition = mapView.camera
            let latLng:CLLocationCoordinate2D = cameraPosition.target;
            if targetLatLong == nil{
                targetLatLong = latLng
                let zoom: Float =  cameraPosition.zoom;
                
                /// Remember to Save Current State of Map To Session Cache Data
                //Save Current Target Location To Session Management
                
                items = []
                progressDialog.isHidden = false
                progressDialog.startAnimating()
                locationResponse = [];
                getNearByLocations(latitude: latLng.latitude, longitude: latLng.longitude, distance: 0.2)
                
            }
            else{
                let zoom:Float = cameraPosition.zoom;
                
                /// Remember to Save Current State of Map To Session Cache Data
                //Save Current Target Location To Session Management
                
                let coordinate1 = CLLocation(latitude: latLng.latitude, longitude: latLng.longitude)
                let coordinate2 = CLLocation(latitude: targetLatLong!.latitude, longitude: targetLatLong!.longitude)
                let distance: Float = Float(coordinate1.distance(from: coordinate2))
                print("Distance is: - \(distance)")
                if(distance>300)
                {
                    targetLatLong = latLng
                    /// Remember to Save Current State of Map To Session Cache Data
                    //Save Current Target Location To Session Management
                    
                    items = []
                    progressDialog.isHidden = false
                    progressDialog.startAnimating()
                    locationResponse = [];
                    getNearByLocations(latitude: latLng.latitude, longitude: latLng.longitude, distance: 0.2)
                }
                
            }
            
            
        }
    }
    
    func show_marker(position: CLLocationCoordinate2D)
    {
        let marker = GMSMarker()
        marker.position = position
        marker.title = title
        marker.map = google_map
        
    }
    
    @objc func tapLogout(){
        SessionUtils.logout()
        let logoutViewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "Main") as? ViewController
        self.navigationController?.pushViewController(logoutViewController!, animated: false);
        self.present(logoutViewController!,animated: true,completion: nil)
        self.navigationController?.popToRootViewController(animated: false)
        
        
    }
    
    
    
    @objc func tapGestureAction(){
        self.leftLeadingAnchor.constant = -300;
        DispatchQueue.main.async {
            self.rightSideMenu.isHidden = true
        }
        
    }
    
    
    
    func setUpDrawerMenuItemGestures()
    {
        let tapGestureLogout = UITapGestureRecognizer(target: self, action: #selector(tapLogout))
        logoutMenuItem.addGestureRecognizer(tapGestureLogout)
    }
    
    
    func setUpExtraGestures()
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        rightSideMenu.addGestureRecognizer(tapGesture)
        
    }
    
    
    func getNearByLocations(latitude: Double, longitude: Double, distance: Double)
    {
        do{
            let parametersNearBy =  [
                "latitude": latitude,
                "longitude": longitude,
                "distance": distance,
                "api_token": SessionUtils.getToken()
                ] as [String : Any]
            
            let header = [
                "Accept": "text/json"
            ]
            
            let resourceString = "\(Utils.requestURL)api/nearby";
            
            Alamofire.request(resourceString, method: .get, parameters: parametersNearBy, headers: header).responseJSON(completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    if let httpURLResponse = response.response{
                        let response_code = httpURLResponse.statusCode;
                        if response_code == 200 {
                            do{
                                let json = JSON(value)
                                let str = String(describing: json);
                                let jsonData = str.data(using: .utf8)
                                let decoder = JSONDecoder();
                                let res = try decoder.decode([InteractionAddressData].self, from: jsonData!)
                                self.locationResponse = res
                                //Manage Data Comes from server
                                self.manageDataOnCameraIdle(locationResponse: self.locationResponse)
                                
                                //Cluster All Location Data to Map
                                self.clusterManager!.clearItems()
                                self.clusterManager!.add(self.items)
                                self.clusterManager!.cluster()
                                self.progressDialog.stopAnimating()
                                self.progressDialog.isHidden = true
                                print("Cluster Items \(self.items.count)")
                                self.clusterManager?.cluster()
                                
                            }catch{
                                print(error)
                                self.clusterManager!.clearItems()
                                self.clusterManager!.add(self.items)
                                self.clusterManager!.cluster()
                                self.progressDialog.stopAnimating()
                                self.progressDialog.isHidden = true
                                
                            }
                            
                            
                        }else if response_code == 401{
                            
                            Toast.show(message: "Unauthorized User", controller: self)
                            let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                            self.navigationController?.pushViewController(viewController!, animated: false);
                            self.present(viewController!,animated: true,completion: nil)
                            
                        }else{
                            print("Error - \(httpURLResponse.statusCode)")
                            Toast.show(message: "No Internet Connection/Server Issue", controller: self)
                            self.clusterManager!.clearItems()
                            self.clusterManager!.add(self.items)
                            self.clusterManager!.cluster()
                            self.progressDialog.stopAnimating()
                            self.progressDialog.isHidden = true
                        }
                    }
                case .failure(let error):
                    print(error)
                    Toast.show(message: "There is Some Server Issue", controller: self)
                    
                }
                
            })
            
            
            
        }
        
        
        
    }
    
    func manageDataOnCameraIdle(locationResponse: [InteractionAddressData]) {
        
        items = []
        clusterManager?.clearItems()
        clusterManager?.cluster()
        for var i in (0..<locationResponse.count)
        {
            //Customer Array
            let customersList: [Customer] = [];
            
            var city: String = "";
            var country: String = "";
            let addressLine: String = locationResponse[i].street!
            city = try locationResponse[i].city!.name!
            country = try locationResponse[i].city!.country!.name!
            
            let id:CLong = locationResponse[i].id!
            
            let latitude = Double(locationResponse[i].latitude!)
            let longitude = Double(locationResponse[i].longitude!)
            let location:Location = Location(latitude: latitude!, longitude: longitude!)
            let number = locationResponse[i].number
            let postal = String(locationResponse[i].postcode!)
            let region = locationResponse[i].region ?? ""
            let street = locationResponse[i].street ?? ""
            let unit = locationResponse[i].unit ?? ""
            
            let address: Address = Address(id: id, number: number ?? "44000", street: street, unit: unit, city: city, region: region, postal: postal, country: country, location: location, addressLine: addressLine)
            
            var interaction: Interaction? = nil;
            if(locationResponse[i].current_interaction == nil)
            {
                interaction = Interaction(id: 1, type: "", name: "None", icon: "", color: "#FFFFFF", ordinal: 1)
            }else
            {
                interaction = Interaction(id: locationResponse[i].current_interaction!.id!, type: locationResponse[i].current_interaction!.type!, name: locationResponse[i].current_interaction!.name!, icon: locationResponse[i].current_interaction!.icon!, color: locationResponse[i].current_interaction!.color!, ordinal: locationResponse[i].current_interaction!.ordinal!)
            }
            
            for var j in (0..<locationResponse[i].customers!.count)
            {
                
            }
            var position = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
            var addressInteraction: InteractionAddress = InteractionAddress(address: address, interaction: interaction!, customers: customersList)
            
            let markerView = UIImage(named: "ic_no_interaction_48dp")
            markerView!.size
            
            let clusterItem : AddressInteractionClusterItem = AddressInteractionClusterItem(addressInteraction: addressInteraction, index: i,position: position,name: addressLine,image: markerView!)
            
            self.items.append(clusterItem)
            
        }
        
    }
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if marker.userData! is AddressInteractionClusterItem
        {
            let customerClusterItem = (marker.userData! as! AddressInteractionClusterItem)
            marker.icon =  Utils.resizeImage(image: customerClusterItem.image!, sizeImage: CGSize(width: 25, height: 25))
        }
    }
    
}
