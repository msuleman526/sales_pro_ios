//
//  AreaDetailViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 09/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol DeleteAreaDelegate
{
    func deleteArea(index: Int)
}

protocol EditAreaDelegate
{
    func editArea(areaResponse: CreateAreaResponse, index: Int,employees: [Employee])
}

class AreaDetailViewController: UIViewController, UITextFieldDelegate {
    
    var editAreaDelegate: EditAreaDelegate?
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var popupBackgroundView: UIView!
    @IBOutlet var popup: UIView!
    @IBOutlet weak var popupTableView: UITableView!
    @IBOutlet weak var addEmployeeButton: UIButton!
    var deleteAreaDelegate: DeleteAreaDelegate?
    @IBOutlet weak var areaLabel: UILabel!
    
    @IBOutlet weak var areaKnockedLabel: UILabel!
    @IBOutlet weak var areaAddressLabel: UILabel!
    @IBOutlet weak var areaEmployeeLabel: UILabel!
    @IBOutlet weak var areaNameEditText: UITextField!
    @IBOutlet weak var deleteView: UIView!
    
    
    
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var topBarView: UIView!
    
    var area: Area?
    var index: Int?
    
    @IBOutlet weak var handleArea: UIView!
    var homeViewController:HomeViewController?
    
    var employees: [Employee] = []
    var areaName = ""
    
    var areaUsers: [UsersResponse] = []
    
    var users = ""
    
    @IBOutlet weak var areaEmployeeCollectionView: UICollectionView!
    
    var alert: UIAlertController?
    
    var officesList: [Office] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Search Location"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        let imageGesture = UITapGestureRecognizer(target: self, action: #selector(deleteAreaClick))
        deleteView.addGestureRecognizer(imageGesture)
        
        let imageGesture1 = UITapGestureRecognizer(target: self, action: #selector(addEmployeeClick))
        addEmployeeButton.addGestureRecognizer(imageGesture1)
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        officesList = SessionUtils.getOffices()
        
        popup.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        
        let dissmissPopupGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup))
        popupBackgroundView.addGestureRecognizer(dissmissPopupGesture)
        
        let editClickGesture = UITapGestureRecognizer(target: self, action: #selector(editClick))
        editView.addGestureRecognizer(editClickGesture)
        
        self.areaNameEditText.delegate = self
        
    }
    
    
    
    @objc
    func editClick()
    {
        self.view.endEditing(true)
        areaName = areaNameEditText.text!
        areaUsers = []
        
        
        if(areaName == "")
        {
            Toast.show(message: "Please enter area Name", controller: homeViewController!)
            
        }
        else
        {
            var employeesIds:[Int] = []
            do{
                
                
                
                for i in (0..<employees.count)
                {
                    employeesIds.append(employees[i].id!)
                    
                }
                
                let jsonEmployeeIds = try JSONEncoder().encode(employeesIds)
                self.users = NSString(data: jsonEmployeeIds, encoding: String.Encoding.utf8.rawValue)! as String
                
                homeViewController?.showAreaFullPage()
                
                popupTableView.reloadData()
                viewWillLayoutSubviews()
                self.view.addSubview(popup)
                self.popupBackgroundView.isHidden = false
                viewWillLayoutSubviews()
                
                
                
                
            }catch{
                if Constants.LOGGING_ENABLED{
                print(error)}
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    @objc
    func addEmployeeClick()
    {
        
        let customerViewController = UIStoryboard.init(name: "AddEmployee", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddEmployeeViewController") as! AddEmployeeViewController
        customerViewController.areaEmployees = self.area!.employees!
        customerViewController.addEmployeeDelegate = self
        self.navigationController?.pushViewController(customerViewController, animated: false);
        
    }
    
    @objc
    func deleteAreaClick()
    {
        let refreshAlert = UIAlertController(title: "", message: "Are you sure to delete area?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
            self.deleteArea(areaId: self.area!.id!)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func loadInformation(area: Area, index: Int)
    {
        self.area = area
        self.index = index
        topBarView.backgroundColor = UIColor(hexString: area.color!)
        deleteView.backgroundColor = UIColor(hexString: area.color!)
        editView.backgroundColor = UIColor(hexString: area.color!)
        self.employees = self.area!.employees!
        areaLabel.text = self.area!.name!
        areaNameEditText.text = self.area!.name!
        areaNameEditText.tintColor = UIColor(hexString: area.color!)
        areaAddressLabel.text = "\(self.area!.total_addresses ?? 0) addresses"
        
        
        var areaEmployees:String = ""
        
        for j in (0..<self.area!.employees!.count)
        {
            if(j == self.area!.employees!.count-1)
            {
                areaEmployees.append(contentsOf: self.area!.employees![j].firstName! )
            }
            else
            {
                areaEmployees.append(contentsOf: "\(self.area!.employees![j].firstName!  ), ")
            }
        }
        
        self.areaEmployeeLabel.text = areaEmployees
        self.areaEmployeeCollectionView.reloadData()
        self.areaEmployeeCollectionView.collectionViewLayout.invalidateLayout()
        self.areaEmployeeCollectionView.layoutSubviews()
        
    }
    
    
    func deleteArea(areaId: Int)
    {
        
        present(alert!, animated: false, completion: nil)
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/areas/\(areaId)";
        
        Alamofire.request(resourceString, method: .delete, parameters: nil, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        if Constants.LOGGING_ENABLED{
                            print(value)}
//                        do{
//                            self.alert?.dismiss(animated: false, completion: nil)
//                            let json = JSON(value)
//                            let str = String(describing: json);
//                            let jsonData = str.data(using: .utf8)
//                            let decoder = JSONDecoder();
//                            let res = try decoder.decode(DeleteUserResponse.self, from: jsonData!)
//                            self.deleteAreaDelegate?.deleteArea(index: self.index!)
//
//
//                        }catch{print(error)}
                          self.alert?.dismiss(animated: false, completion: nil)
                          self.deleteAreaDelegate?.deleteArea(index: self.index!)
                        
                        
                    }
                    else if response_code == 401{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unable to Delete User / Server Error.", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    @objc
    func dismissPopup()
    {
        self.popup.removeFromSuperview()
        self.popupBackgroundView.isHidden = true
    }
    
    func editArea(name: String, colorStr: String, office_id: Int, users: String, created_by: Int)
    {
        
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "name": name,
            "created_by": created_by,
            "color": colorStr,
            "office_id": office_id,
            "users": users,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/areas/\(area!.id!)";
        
        Alamofire.request(resourceString, method: .put, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let myAreaResponse = try decoder.decode(CreateAreaResponse.self, from: jsonData!)
                            self.areaLabel.text = myAreaResponse.name!
                            self.editAreaDelegate?.editArea(areaResponse: myAreaResponse, index: self.index!,employees: self.employees)
                            //self.createAreaDelegate?.createArea(area: myAreaResponse,users: self.areaUsers)
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 401{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Employee already exists / Server Error.", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    
    
    
    @objc
    func deleteEmployeeClick(tapGesture: UITapGestureRecognizer)
    {
        if Constants.LOGGING_ENABLED{
            print("Click on Employee Delete")}
        if(employees.count == 1)
        {
            if Constants.LOGGING_ENABLED{
                print("1 Employee")}
            Toast.show(message: "Can't Delete Last Employee", controller: homeViewController!)
        }
        else
        {
            let refreshAlert = UIAlertController(title: "", message: "Are you sure to delete employee? For save changes click on edit.", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
                refreshAlert.dismiss(animated: false, completion: nil)
                let imageView = tapGesture.view as! UIImageView
                let index = imageView.tag
                self.employees.remove(at: index)
                self.area!.employees!.remove(at: index)
                self.areaEmployeeCollectionView.reloadData()
                self.areaEmployeeCollectionView.collectionViewLayout.invalidateLayout()
                self.areaEmployeeCollectionView.layoutSubviews()
                
                //Reload Text Field
                var areaEmployees:String = ""
                
                for j in (0..<self.employees.count)
                {
                    if(j == self.employees.count-1)
                    {
                        areaEmployees.append(contentsOf: self.employees[j].firstName! )
                    }
                    else
                    {
                        areaEmployees.append(contentsOf: "\(self.employees[j].firstName ?? ""), ")
                    }
                }
                self.areaEmployeeLabel.text = areaEmployees
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                refreshAlert.dismiss(animated: false, completion: nil)
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
        
    }
    
}

extension AreaDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return employees.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = areaEmployeeCollectionView.dequeueReusableCell(withReuseIdentifier: "areaemployeecell", for: indexPath) as! AreaEmployeeCell
        cell.employeeName.text = employees[indexPath.row].firstName ?? ""
        
        cell.deleteImage.tag = indexPath.row
        cell.deleteImage.isUserInteractionEnabled = true
        let imageGesture = UITapGestureRecognizer(target: self, action: #selector(deleteEmployeeClick(tapGesture:)))
        imageGesture.numberOfTapsRequired = 1
        cell.deleteImage.addGestureRecognizer(imageGesture)
        return cell
    }
    
    
}

class AreaEmployeeCell: UICollectionViewCell
{
    @IBOutlet weak var deleteImage: UIImageView!
    
    @IBOutlet weak var employeeName: UILabel!
    @IBOutlet weak var employeeImage: UIImageView!
}

extension AreaDetailViewController: AddEmployeeDelegate
{
    func addEmployees(employees: [Employee]) {
        self.navigationController?.popViewController(animated: true)
        self.employees = employees
        self.area!.employees = employees
        var areaEmployees:String = ""
        
        for j in (0..<self.area!.employees!.count)
        {
            if(j == self.area!
                .employees!.count-1)
            {
                areaEmployees.append(contentsOf: self.area!.employees![j].firstName! )
            }
            else
            {
                areaEmployees.append(contentsOf: "\(self.area!.employees![j].firstName!  ), ")
            }
        }
        
        self.areaEmployeeLabel.text = areaEmployees
        self.areaEmployeeCollectionView.reloadData()
        self.areaEmployeeCollectionView.collectionViewLayout.invalidateLayout()
        self.areaEmployeeCollectionView.layoutSubviews()
        Toast.show(message: "For save changes click on edit", controller: self)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tableHeight.constant
            = self.popupTableView.contentSize.height
        self.popup.frame.size.height =  self.tableHeight.constant + 35
        
    }
    
    
}


extension AreaDetailViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return officesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = popupTableView.dequeueReusableCell(withIdentifier: "popup", for: indexPath) as! OfficePopupAreaCell
        cell.label.text = officesList[indexPath.row].name!
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popupTableView
        {
            dismissPopup()
            editArea(name: areaName, colorStr: area!.color!, office_id: officesList[indexPath.row].id!, users: users as String, created_by: SessionUtils.getUserId())
        }
    }
    
    
    
    
}

class OfficePopupAreaCell: UITableViewCell
{
    
    
    @IBOutlet weak var label: UILabel!
}
