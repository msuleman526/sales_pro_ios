//
//  MyAreaViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 02/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import GoogleMaps
import GoogleMapsUtils

protocol MyAreaClickDelegate {
    func selectedMyArea(area: Area)
}

class MyAreaViewController: UIViewController {
    
    var myAreaDelegate: MyAreaClickDelegate?
    
    @IBOutlet weak var myAreaTableView: UITableView!
    var alert: UIAlertController?
    var myareas: [Area] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Navigation Controller
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "My Areas"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        //Loading Alert initialization
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        getMyAreas()
    }
    
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func getMyAreas()
    {
        myareas = []
        present(alert!, animated: false, completion: nil)
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/my-areas";
        
        Alamofire.request(resourceString, method: .get, parameters: nil, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            self.alert?.dismiss(animated: false, completion: nil)
                            if Constants.LOGGING_ENABLED{
                                print(value)}
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode([MyAreaResponse].self, from: jsonData!)
                            
                            for i in (0..<res.count)
                            {
                                var locationList: [Location] = []
                                var latlngs: [LatLngs] = []
                                let jsonLocation = JSON(res[i].geography!)
                                let strValue = String(describing: jsonLocation)
                                let jsonLocationData = strValue.data(using: .utf8)
                                latlngs = try decoder.decode([LatLngs].self, from: jsonLocationData!)
                                
                                for j in (0..<latlngs.count)
                                {
                                    locationList.append(Location(latitude: latlngs[j].latitude!, longitude: latlngs[j].longitude!))
                                }
                                
                                let formatter = DateFormatter()
                                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                
                                let created_at = formatter.date(from: res[i].created_at!)
                                let updated_at = formatter.date(from: res[i].updated_at!)
                                
                                self.myareas.append(Area(id: res[i].id!, name: res[i].name!, color: res[i].color!, created: created_at!, modified: updated_at!, coordinates: locationList))
                            }
                            
                            self.myAreaTableView.reloadData()
                            
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 422 {
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Given Data is Invalid", controller: self)
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                        print (response_code)
                            print("Error is \(response.result.value!)")}
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
            }
            
        })
    }
    
    
    
}

extension MyAreaViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myareas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myAreaTableView.dequeueReusableCell(withIdentifier: "MyAreaCell", for: indexPath) as? MyAreaCell
        cell?.myAreaName.text = myareas[indexPath.row].name
        let color = myareas[indexPath.row].color!
        //let icon = myareas[indexPath.row].name?.prefix(1).capitalized
        let icon = ""
        let tmpImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: color))
        cell?.myAreaImage.image = Utils.textToImage(drawText: icon as NSString, inImage: tmpImage)
        return cell!;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.myAreaDelegate?.selectedMyArea(area: myareas[indexPath.row])
    }
    
    
}

class MyAreaCell: UITableViewCell{
    
    @IBOutlet weak var myAreaImage: UIImageView!
    @IBOutlet weak var myAreaName: UILabel!
}


