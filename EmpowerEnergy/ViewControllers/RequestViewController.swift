//
//  RequestViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 02/07/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import GoogleMaps
import GoogleMapsUtils
import SwiftyJSON
import Alamofire

protocol SendRequestDelegate {
    func sendRequest()
}

class RequestViewController: UIViewController {
    
    //@IBOutlet weak var tableHeight: NSLayoutConstraint!
    // @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    
    var sendRequestDelegate: SendRequestDelegate?
    @IBOutlet weak var informationView: UIView!
    @IBOutlet weak var commentsField: UITextField!
    var addressInteraction: InteractionAddress?
    
    @IBOutlet weak var customersFiled: UITextField!
    
    @IBOutlet weak var selectTimeField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    
    var datePicker = UIDatePicker()
    
    @IBOutlet weak var latLngField: UITextField!
    @IBOutlet weak var popUpBackGroundView: UIView!
    var drawCoordinates = [[CLLocationCoordinate2D]]()
    var drawPolygon = [GMSPolygon]()
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet var popup: UIView!
    @IBOutlet weak var popupTableView: UITableView!
    @IBOutlet weak var google_map: GMSMapView!
    
    @IBOutlet weak var startDrawingBtn: UIButton!
    var alert: UIAlertController?
    
    var singleCoordinates: [CLLocationCoordinate2D] = []
    
    var timeList: [String] = []
    
    @IBOutlet weak var timeTextFieldDrop: DropDown!
    var currentPolygonIndex = 0
    
    lazy var canvasView:CanvasView = {
        
        var overlayView = CanvasView(frame: self.google_map
            .frame)
        overlayView.isUserInteractionEnabled = true
        overlayView.delegate = self
        return overlayView
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = ""
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.backItem?.title = "Back"
        
        setUpGoogleMap()
        
        timeList.append("Urgent")
        timeList.append("In House")
        timeList.append("On Call With Customer")
        
        popup.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        
        let dissmissPopupGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup))
        popUpBackGroundView.addGestureRecognizer(dissmissPopupGesture)
        
        //timeLabel.isUserInteractionEnabled = true
        //let timeClickPopupGesture = UITapGestureRecognizer(target: self, action: #selector(timeClick))
        //timeLabel.addGestureRecognizer(timeClickPopupGesture)
        
        addressField.text = addressInteraction!.address!.street!
        latLngField.text = "\(addressInteraction!.address!.location!.latitude!), \(addressInteraction!.address!.location!.longitude!)"
        
        if(addressInteraction!.customers!.count > 0)
        {
            customersFiled.text = addressInteraction!.customers![0].firstName!
        }
        
        createTimeDialog()
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        //drawPolygon = []
        drawCoordinates = []
        
        //self.informationView.layer.cornerRadius = 10
        //self.informationView.clipsToBounds = true
        
        drawPolygon = []
        
        timeTextFieldDrop.optionArray = timeList
        timeTextFieldDrop.didSelect{(selectedText , index ,id) in
            self.timeTextFieldDrop.text = ""
            if(self.timeList[index] == "Urgent")
            {
                let refreshAlert = UIAlertController(title: "", message: "Please only use an urgent request as a last resort. Are you sure this is urgent?", preferredStyle: UIAlertController.Style.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                    refreshAlert.dismiss(animated: false, completion: nil)
                    self.timeTextFieldDrop.text = self.timeList[index]
                    self.selectTimeField.text = ""
                    
                }))
                
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                    refreshAlert.dismiss(animated: false, completion: nil)
                }))
                
                self.present(refreshAlert, animated: true, completion: nil)
                
            }else
            {
                self.timeTextFieldDrop.text = self.timeList[index]
                               self.selectTimeField.text = ""
            }
            
        }
        
    }
    
    @objc
    func dismissPopup()
    {
        self.popup.removeFromSuperview()
        self.popUpBackGroundView.isHidden = true
        self.view.addSubview(canvasView)
    }
    
    func popupHeight()
    {
        self.tableHeight.constant
            = self.popupTableView.contentSize.height
        self.popup.frame.size.height =  self.tableHeight.constant + 10
    }
    
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //  Initialize Map View
    func setUpGoogleMap()
    {
        if Constants.LOGGING_ENABLED{
            print("Loading Map")}
        //show_marker(position: google_map.camera.target)
        let camera = GMSCameraPosition.camera(withLatitude: self.addressInteraction!.address!.location!.latitude!, longitude: self.addressInteraction!.address!.location!.longitude!, zoom: 20.0)
        google_map.camera = camera
        google_map.mapType = GMSMapViewType.satellite
        
        
        
        
        
        
    }
    
    func createTimeDialog()
    {
        //let textField = tapGesture.view as! UITextField
        //let index = textField.tag
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(datePickerDoneClick))
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelEditing))
        
        toolbar.setItems([doneBtn,cancelBtn], animated: true)
        
        self.selectTimeField.inputAccessoryView = toolbar
        
        self.selectTimeField.inputView = datePicker
        
        datePicker.datePickerMode = .dateAndTime
        
    }
    
    
    @IBAction func clearMapAction(_ sender: Any) {
        
        drawCoordinates = []
        singleCoordinates = []
        for i in (0..<drawPolygon.count)
        {
            drawPolygon[i].map = nil
        }
        google_map.clear()
        drawPolygon = []
        currentPolygonIndex = 0
    }
    
    @IBAction func startEndDrawingAction(_ sender: Any) {
        
        if startDrawingBtn.titleLabel!.text == "Start Drawing"
        {
            initializeDrawEvent()
            startDrawingBtn.backgroundColor = UIColor.lightGray
            startDrawingBtn.setTitle("Stop Drawing", for: .normal)
        }
        else{
            endDrawEvent()
            startDrawingBtn.backgroundColor = UIColor(named: "textColor")
            startDrawingBtn.setTitle("Start Drawing", for: .normal)
        }
        
        
        
        
    }
    
    @objc
    func datePickerDoneClick()
    {
        let formattor =  DateFormatter();
        formattor.dateFormat = "yyyy-MM-dd HH-mm:ss"
        
        self.selectTimeField.text = formattor.string(from: datePicker.date)
        self.timeTextFieldDrop.text = ""
        self.view.endEditing(true)
    }
    
    @objc
    func cancelEditing()
    {
        self.view.endEditing(true)
    }
    
    @IBAction func sendClickAction(_ sender: Any) {
        self.view.endEditing(true)
        let latitude = addressInteraction!.address!.location!.latitude
        let longitude = addressInteraction!.address!.location!.longitude
        let street = addressInteraction!.address!.street!
        let customers = customersFiled.text!
        let comment = commentsField.text!
        var time = ""
        var coordinates = ""
        
        if selectTimeField.text! == ""
        {
            time = timeTextFieldDrop.text!
        }else
        {
            time = selectTimeField.text!
        }
        
        if time == ""
        {
            Toast.show(message: "Please choose time", controller: self)
        }
        else if(drawCoordinates.count<1)
        {
            Toast.show(message: "Please draw on house", controller: self)
        }else if(customers == "")
        {
            Toast.show(message: "Please enter customer name", controller: self)
        }else if(time == "")
        {
            Toast.show(message: "Please choose time", controller: self)
        }
        else
        {
            do{
                var coordinatesList: [[DLocation]] = []
                for i in (0..<drawCoordinates.count)
                {
                    var coordinatesSingle: [DLocation] = []
                    for j in (0..<drawCoordinates[i].count)
                    {
                        coordinatesSingle.append(DLocation(latitude: drawCoordinates[i][j].latitude, longitude: drawCoordinates[i][j].longitude))
                    }
                    coordinatesList.append(coordinatesSingle)
                }
                
                let jsonData = try JSONEncoder().encode(coordinatesList)
                coordinates  = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                
                //Send Data to Server
                present(alert!, animated: false, completion: nil)
                let image = self.google_map.takeScreenshot()
                if Constants.LOGGING_ENABLED{
                    print("Coordinates - \(coordinates)")}
                
                /*print("Api_Token - \(SessionUtils.getToken())")
                print("Address - \(street)")
                print("Latitude - \(latitude!)")
                print("Longitude - \(longitude!)")
                print("Coordinates - \(coordinates)")
                print("Customer - \(customers)")
                print("Comment - \(comment)")
                print("Time - \(time)")
                print("image - \(image)")*/
                
                
                
                
                let parameters =  [
                    "address": street,
                    "latitude": latitude!,
                    "longitude": longitude!,
                    "coordinates": coordinates,
                    "customer": customers,
                    "comment": comment,
                    "time": time,
                    "image": image
                    ] as [String : Any]
                
                let header = [
                    "Accept": "text/json",
                    "Authorization": "bearer \(SessionUtils.getToken())"
                ]
                
                let resourceString = "\(Utils.requestURL)api/designer/request";
                
                Alamofire.request(resourceString, method: .post, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let value):
                        if let httpURLResponse = response.response{
                            let response_code = httpURLResponse.statusCode;
                            if response_code == 200{
                                self.alert?.dismiss(animated: false, completion: nil)
//                                do{
//                                    self.alert?.dismiss(animated: false, completion: nil)
//                                    let json = JSON(value)
//                                    let str = String(describing: json);
//                                    let jsonData = str.data(using: .utf8)
//                                    let decoder = JSONDecoder();
//                                    let res = try decoder.decode(RequestResponse.self, from: jsonData!)
                                if Constants.LOGGING_ENABLED{
                                    print("Return Value - \(value)")}
                                    self.sendRequestDelegate?.sendRequest()
                                    
                                //}catch{print(error)}
                                
                                
                            }
                            else if response_code == 401{
                                self.alert?.dismiss(animated: false, completion: nil)
                                Toast.show(message: "Unauthorized User", controller: self)
                                let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                                self.navigationController?.pushViewController(viewController!, animated: false);
                                //self.present(viewController!,animated: true,completion: nil)
                                
                            }
                            else{
                                if Constants.LOGGING_ENABLED{
                                    print(response.result.value!)}
                                self.alert?.dismiss(animated: false, completion: nil)
                                Toast.show(message: "Server Error.", controller: self)
                                
                            }
                        }
                    case .failure(let error):
                        self.alert?.dismiss(animated: false, completion: nil)
                        if Constants.LOGGING_ENABLED{
                            print(error)}
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                    }
                    
                })
                
            }catch let error as NSError
            {
                if Constants.LOGGING_ENABLED{
                    print(error)}
            }
        }
        
        
    }
    
    
    func initializeDrawEvent()
    {
        self.view.addSubview(canvasView)
        //self.google_map.addSubview(canvasView)
        //self.google_map.bringSubviewToFront(canvasView)
    }
    
    func endDrawEvent()
    {
        self.canvasView.image = nil
        self.canvasView.removeFromSuperview()
    }
    
    func createPolygonFromTheDrawablePoints(coordinates: [CLLocationCoordinate2D]){
        addPolyGonInMapView(drawableLoc: coordinates) //neglects a single touch
    }
    
    
    func addPolyGonInMapView(drawableLoc: [CLLocationCoordinate2D]){
        if Constants.LOGGING_ENABLED{
            print("INDEX - \(currentPolygonIndex)")}
        if(drawPolygon.count == currentPolygonIndex)
        {
            drawPolygon.append(GMSPolygon())
        }
        drawPolygon[currentPolygonIndex].map = nil
        let path = GMSMutablePath()
        for loc in drawableLoc{
            
            path.add(loc)
            
        }
        // var count = drawPolygon.count
        if Constants.LOGGING_ENABLED{
            print("Size of Polygons - \(drawPolygon.count)")}
        drawPolygon[currentPolygonIndex] = GMSPolygon(path: path)
        drawPolygon[currentPolygonIndex].strokeWidth = 2
        
        drawPolygon[currentPolygonIndex].strokeColor = UIColor(hexString: "#8333FF")
        drawPolygon[currentPolygonIndex].fillColor = UIColor(hexString: "#8333FF").withAlphaComponent(0.5)
        drawPolygon[currentPolygonIndex].map = google_map
    }
    
    @objc
    func timeClick()
    {
        self.view.endEditing(true)
        popupTableView.reloadData()
        popupHeight()
        self.view.addSubview(popup)
        self.popUpBackGroundView.isHidden = false
        popupHeight()
    }
    
    
    
}

extension RequestViewController:NotifyTouchEvents{
    
    func touchBegan(touch:UITouch){
        //let location = touch.location(in: self.google_map)
        //let coordinate = self.google_map.projection.coordinate(for: location)
        //self.drawCoordinates.append(coordinate)
        //drawCoordinates = []
        //createPolygonFromTheDrawablePoints()
        singleCoordinates = []
        
    }
    
    func touchMoved(touch:UITouch){
        
        let location = touch.location(in: self.google_map)
        let coordinate = self.google_map.projection.coordinate(for: location)
        self.singleCoordinates.append(coordinate)
        createPolygonFromTheDrawablePoints(coordinates: singleCoordinates)
        
    }
    
    func touchEnded(touch:UITouch){
        
        let location = touch.location(in: self.google_map)
        let coordinate = self.google_map.projection.coordinate(for: location)
        singleCoordinates.append(coordinate)
        self.drawCoordinates.append(singleCoordinates)
        
        currentPolygonIndex = drawCoordinates.count
        //createPolygonFromTheDrawablePoints()
        //drawPolygon?.map = nil
    }
    
    
}

extension RequestViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = popupTableView.dequeueReusableCell(withIdentifier: "popup", for: indexPath) as! RequestPopupCell
        cell.label.text = timeList[indexPath.row]
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismissPopup()
//        if(self.timeList[indexPath.row] == "Urgent")
//        {
//            let refreshAlert = UIAlertController(title: "", message: "Please only use an urgent request as a last resort. Are you sure this is urgent?", preferredStyle: UIAlertController.Style.alert)
//
//            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
//                refreshAlert.dismiss(animated: false, completion: nil)
//                self.timeLabel.text = self.timeList[indexPath.row]
//                self.selectTimeField.text = ""
//
//            }))
//
//            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
//                refreshAlert.dismiss(animated: false, completion: nil)
//            }))
//
//            present(refreshAlert, animated: true, completion: nil)
//        }else
//        {
//            self.timeLabel.text = self.timeList[indexPath.row]
//                           self.selectTimeField.text = ""
//        }
        
        
    }
    
    
}


class RequestPopupCell: UITableViewCell{
    
    
    @IBOutlet weak var label: UILabel!
    
}

extension UIView {
    
    func takeScreenshot() -> String {
        
        //begin
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        // draw view in that context.
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        
        // get iamge
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let imageData:NSData =  image!.jpegData(compressionQuality: 0.4)! as NSData
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        
        // UIPasteboard.general.string = "data:image/jpg;base64,\(strBase64)"
        
        return "data:image/jpg;base64,\(strBase64)"
        
    }
    
}
