//
//  CreateEmployeeViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 25/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit

protocol EditEmployeeUserDelegate{
    func editEmployee(user: UsersResponse,index: Int)
}

class EditEmployeeViewController: UIViewController, UITextFieldDelegate {
    
    var editEmployeeDelegate: EditEmployeeUserDelegate?
    
    var index = 0
    
    @IBOutlet weak var officeTextFiled: DropDown!
    @IBOutlet weak var assignTextFeild: DropDown!
    @IBOutlet weak var userTypeTextField: DropDown!
    
    @IBOutlet weak var passwordEye: UIImageView!
    
    @IBOutlet weak var tableHeight3: NSLayoutConstraint!
    @IBOutlet weak var popupTableView3: UITableView!
    @IBOutlet var popup3: UIView!
    @IBOutlet weak var popupTableView1: UITableView!
    
    var office_id = 0
    var assigned_to = 0
    var user_type: String?
    
    @IBOutlet weak var workEmailLabel: UITextField!
    @IBOutlet weak var passwordLabel: UITextField!
    @IBOutlet weak var workPhoneLabel: UITextField!
    @IBOutlet weak var nameLabel: UITextField!
    
    @IBOutlet weak var lastNameTextFiled: UITextField!
    
    @IBOutlet weak var tableHeight2: NSLayoutConstraint!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet var popup1: UIView!
    
    var employee: Employee?
    var managersList: [UsersResponse] =  []
    var managersListStr: [String] =  []
    
    
    @IBOutlet weak var popupTableView: UITableView!
    
    @IBOutlet weak var popUpBackGroundView: UIView!
    @IBOutlet var popup: UIView!
    var popUpList: [String] = []
    var popUpList1: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Edit User"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        for i in (0..<(ViewController.interactionResponse?.users!.count)!)
        {
            if(ViewController.interactionResponse?.users![i].role!.name! == "Manager")
            {
                managersList.append((ViewController.interactionResponse?.users![i])!)
                managersListStr.append(("\(String(describing: ViewController.interactionResponse?.users![i].first_name ?? "")) \(ViewController.interactionResponse?.users![i].last_name ?? "")"))
            }
        }
        
        popUpList = []
        for i in (0..<(ViewController.interactionResponse?.roles!.count)!)
        {
            if ViewController.interactionResponse?.roles![i].name == "Sales Rep"
            {
                popUpList.append("Rep")
                
            }else{
                popUpList.append((ViewController.interactionResponse?.roles![i])!.name!)
            }
        }
        
        popUpList1 = []
        if Constants.LOGGING_ENABLED{
            print("OFFCIECOUNT - \(SessionUtils.getOffices().count)")}
        for i in (0..<(SessionUtils.getOffices().count))
        {
            popUpList1.append((SessionUtils.getOffices()[i].name)!)
        }
        
        popUpBackGroundView.isHidden = true
        popup.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        popup1.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        popup3.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        
        let dissmissPopupGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup))
        popUpBackGroundView.addGestureRecognizer(dissmissPopupGesture)
        
        //adminLabel.isUserInteractionEnabled = true
        //officeLabel.isUserInteractionEnabled = true
        //managerLabel.isUserInteractionEnabled = true
        
        //let typePopupGesture = UITapGestureRecognizer(target: self, action: #selector(typesClick))
        //adminLabel.addGestureRecognizer(typePopupGesture)
        
        //let officePopupGesture = UITapGestureRecognizer(target: self, action: #selector(officesClick))
        //officeLabel.addGestureRecognizer(officePopupGesture)
        
        
       // let managerPopupGesture = UITapGestureRecognizer(target: self, action: #selector(managerClick))
       // managerLabel.addGestureRecognizer(managerPopupGesture)
        
        office_id = SessionUtils.getOffices()[0].id!
        assigned_to = managersList[0].id!
        //user_type = popUpList[0]
        
        officeTextFiled.text = SessionUtils.getOffices()[0].name!
        assignTextFeild.text = managersList[0].name!
        userTypeTextField.text = popUpList[0]
        
        office_id = employee!.offices![0].id! as Int
        assigned_to = employee!.parent
        user_type = employee!.type!
        
        if Constants.LOGGING_ENABLED{
            print(office_id)
            print(assigned_to)
            print(user_type!)
        }
        loadInformation()
        
        heightOfPopup3()
        heightOfPopup1()
        
        self.nameLabel.delegate = self
        self.passwordLabel.delegate = self
        self.workPhoneLabel.delegate = self
        
        setUpDropDownList()
        
        
    }
    
    func setUpDropDownList()
    {
        //self.userTypeTextField.addGestureRecognizer(UITapGestureRecognizer(target: nil, action: #selector(showDropDown)))
        userTypeTextField.optionArray = popUpList
        userTypeTextField.didSelect{(selectedText , index ,id) in
              self.userTypeTextField.text = "\(selectedText)"
            self.user_type = self.popUpList[index]
        }
        
        assignTextFeild.optionArray = managersListStr
        assignTextFeild.didSelect{(selectedText , index ,id) in
              self.assignTextFeild.text = "\(selectedText)"
              self.assigned_to = self.managersList[index].id!
        }
        
        officeTextFiled.optionArray = popUpList1
        officeTextFiled.didSelect{(selectedText , index ,id) in
              self.officeTextFiled.text = "\(selectedText)"
              self.office_id = SessionUtils.getOffices()[index].id!
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           self.view.endEditing(true)
       }
    
    func loadInformation()
    {
        var managerStr = "No Manager"
        nameLabel.text = employee!.firstName ?? ""
        lastNameTextFiled.text = employee?.lastName ?? ""
        workPhoneLabel.text = employee!.phone!
        workEmailLabel.text = employee!.email!
        passwordLabel.text = employee!.password ?? ""
        
        for i in (0..<(ViewController.interactionResponse?.users!.count)!)
        {
            if(ViewController.interactionResponse?.users![i].id! == employee!.parent)
            {
                managerStr = ViewController.interactionResponse!.users![i].name! as String
                assignTextFeild.selectedIndex = i
            }
        }
        
        userTypeTextField.text = employee!.type!
        assignTextFeild.text = managerStr
        officeTextFiled.text = employee?.offices![0].name
        
        let eyeImage = UITapGestureRecognizer(target: self, action: #selector(passwordEyeClick))
        passwordEye.isUserInteractionEnabled = true
        passwordEye.addGestureRecognizer(eyeImage)
        passwordLabel.isSecureTextEntry = true
        
    }
    
    
    @objc func passwordEyeClick()
    {
        if passwordEye.image == UIImage(systemName: "eye.fill")
        {
            passwordEye.image = UIImage(systemName: "eye.slash.fill")
            passwordLabel.isSecureTextEntry = false
        }else
        {
            passwordEye.image = UIImage(systemName: "eye.fill")
            passwordLabel.isSecureTextEntry = true
        }
    }
    
    @IBAction func editClickAction(_ sender: Any) {
        self.view.endEditing(true)
        let mobilePhone = workPhoneLabel.text!
        let emailAddress = workEmailLabel.text!
        let full_name = nameLabel.text!
        let last_name = lastNameTextFiled.text ?? ""
        let password = passwordLabel.text!
        
        if(full_name == "")
        {
            Toast.show(message: "Please Enter Full Name", controller: self)
            
        }else if(emailAddress == "")
        {
            Toast.show(message: "Please Enter Work Email", controller: self)
            
        }else if(mobilePhone == "")
        {
            Toast.show(message: "Please Enter Work Phone", controller: self)
        }
        else if(!Utils.isValidEmail(email: emailAddress))
        {
            Toast.show(message: "Please Enter Valid Email", controller: self)
        }else if(password.count>0 && password.count<8)
        {
            Toast.show(message: "Password is short. minimum length is 8.", controller: self)
        }else if(mobilePhone.count<6)
        {
            Toast.show(message: "Phone is too short", controller: self)
        }else{
            var id = 0;
            for i in (0..<(ViewController.interactionResponse?.roles!.count)!)
            {
                if user_type == ViewController.interactionResponse!.roles![i].name!
                {
                    id = ViewController.interactionResponse!.roles![i].id!
                }
            }
            let user = UsersResponse(id: employee!.id!,name: full_name,first_name: full_name, last_name: last_name, email: emailAddress, office_id: office_id, role: Role(id: id,name: user_type!), parent: assigned_to, phone: mobilePhone,password: password)
            editEmployeeDelegate?.editEmployee(user: user,index: index)
        }
    }
    
    
    @objc
    func dismissPopup()
    {
        self.popup.removeFromSuperview()
        self.popup1.removeFromSuperview()
        self.popup3.removeFromSuperview()
        self.popUpBackGroundView.isHidden = true
    }
    
    
    func heightOfPopup1()
    {
        self.tableHeight.constant
            = self.popupTableView.contentSize.height
        self.popup.frame.size.height =  self.tableHeight.constant + 10
    }
    
    func heightOfPopup3()
    {
        self.tableHeight3.constant
            = self.popupTableView3.contentSize.height
        self.popup3.frame.size.height =  self.tableHeight3.constant - 10
    }
    
    func heightOfPopup2()
    {
        self.tableHeight2.constant
            = self.popupTableView1.contentSize.height
        
        self.popup1.frame.size.height =  self.tableHeight2.constant + 10
    }
    
    
    
    
    @objc
    func typesClick()
    {
        if Constants.LOGGING_ENABLED{
            print("Size is - \(popUpList.count)")}
        popupTableView.reloadData()
        heightOfPopup1()
        self.view.addSubview(popup)
        self.popUpBackGroundView.isHidden = false
        heightOfPopup1()
    }
    
    @objc
    func officesClick()
    {
        popupTableView1.reloadData()
        heightOfPopup2()
        self.view.addSubview(popup1)
        self.popUpBackGroundView.isHidden = false
        heightOfPopup2()
    }
    
    @objc
    func managerClick()
    {
        popupTableView3.reloadData()
        heightOfPopup3()
        self.view.addSubview(popup3)
        self.popUpBackGroundView.isHidden = false
        heightOfPopup3()
    }
    
    
}

extension EditEmployeeViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popupTableView
        {
            return popUpList.count
        }else if tableView == popupTableView3
        {
            return managersList.count
        }
        else
        {
            return popUpList1.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == popupTableView
        {
            let cell = popupTableView.dequeueReusableCell(withIdentifier: "popup", for: indexPath) as! PopUpEditEmployeeCell
            cell.label.text = popUpList[indexPath.row]
            return cell
        }
        else if tableView == popupTableView3
        {
            let cell = popupTableView3.dequeueReusableCell(withIdentifier: "popup1", for: indexPath) as! PopUpEditEmployeeCell2
            cell.label.text = managersList[indexPath.row].name
            return cell
        }
        else
        {
            let cell = popupTableView1.dequeueReusableCell(withIdentifier: "popup2", for: indexPath) as! PopUpEditEmployeeCell1
            cell.label.text = popUpList1[indexPath.row]
            return cell        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == popupTableView)
        {
            user_type = popUpList[indexPath.row]
            //adminLabel.text = user_type
        }
        else if tableView == popupTableView1
        {
            
            office_id = SessionUtils.getOffices()[indexPath.row].id!
            //officeLabel.text = SessionUtils.getOffices()[indexPath.row].name
            
        }else{
            
            assigned_to = managersList[indexPath.row].id!
           // managerLabel.text = managersList[indexPath.row].name!
        }
        dismissPopup()
    }
    
    
    
    
}

class PopUpEditEmployeeCell: UITableViewCell{
    
    @IBOutlet weak var label: UILabel!
    
}

class PopUpEditEmployeeCell1: UITableViewCell{
    
    @IBOutlet weak var label: UILabel!
    
    
}
class PopUpEditEmployeeCell2: UITableViewCell
{
    
    @IBOutlet weak var label: UILabel!
}

