//
//  MapViewViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 10/08/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import GoogleMaps
import GoogleMapsUtils
import Alamofire
import SwiftyJSON

class MapViewViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    var employeeName: String?
    var locationManager : CLLocationManager = CLLocationManager()
    @IBOutlet weak var endDateTextField: UITextField!
    var datePicker = UIDatePicker()
    var employee: Employee?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "\(employeeName!)"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.backItem?.title = "Back"
        
        setUpGoogleMap()
        createDatePicker()
        createEndDatePicker()
        
        getConverstation(startDate: "2020-08-22", endDate: "2020-08-22")
    }
    
    func createDatePicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(datePickerDoneClick))
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelEditing))
        
        toolbar.setItems([doneBtn,cancelBtn], animated: true)
        
        dateTextField.inputAccessoryView = toolbar
        
        dateTextField.inputView = datePicker
        
        datePicker.datePickerMode = .date
    }
    
    @objc
    func datePickerDoneClick()
    {
        let formattor =  DateFormatter();
        formattor.dateFormat = "yyyy-MM-dd"
        
        dateTextField.text = formattor.string(from: datePicker.date)
        if endDateTextField.text != ""{
            getConverstation(startDate: dateTextField.text!, endDate: endDateTextField.text!)
        }
        self.view.endEditing(true)
    }
    
    func createEndDatePicker()
    {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(endDatePickerDoneClick))
        
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelEditing))
        
        toolbar.setItems([doneBtn,cancelBtn], animated: true)
        
        endDateTextField.inputAccessoryView = toolbar
        
        endDateTextField.inputView = datePicker
        
        datePicker.datePickerMode = .date
    }
    
    @objc
    func endDatePickerDoneClick()
    {
        let formattor =  DateFormatter();
        formattor.dateFormat = "yyyy-MM-dd"
        
        endDateTextField.text = formattor.string(from: datePicker.date)
        if dateTextField.text != ""{
            getConverstation(startDate: dateTextField.text!, endDate: endDateTextField.text!)
        }
        self.view.endEditing(true)
    }
    
    @objc
    func cancelEditing()
    {
        self.view.endEditing(true)
    }
    
    
    //  Initialize Map View
    func setUpGoogleMap()
    {
        //show_marker(position: google_map.camera.target)
        let camera = GMSCameraPosition.camera(withLatitude: 41.65689673944328, longitude: -72.66650788486002, zoom: 17.0)
        mapView.camera = camera
        mapView.settings.compassButton = true
        
        mapView.isMyLocationEnabled = true;
        locationManager.delegate  = self
        locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if let nav_height = self.navigationController?.navigationBar.frame.height
        {
            let status_height = view.window!.windowScene!.statusBarManager!.statusBarFrame.size.height
            mapView.padding = UIEdgeInsets (top: nav_height+status_height, left: 0, bottom: 0, right: 0)
        }
        
        if(SessionUtils.getLatitude() == 0.0 || SessionUtils.getLongitude() == 0.0 )
        {
            if Constants.LOGGING_ENABLED{
                print("Latitude and Longitude are Null")}
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                //locationManager.location?.coordinate.latitude
                showMyLocation()
            }
            else
            {
                Toast.show(message: "Your Location Services are Off Please enable from setting", controller: self)
            }
        }
        else
        {
            let camera = GMSCameraPosition.camera(withLatitude: SessionUtils.getLatitude(), longitude: SessionUtils.getLongitude(), zoom: Float(SessionUtils.getZoom()))
            mapView.camera = camera
        }
        if Constants.LOGGING_ENABLED{
        print(GMSMapViewType.hybrid.rawValue)
        print(GMSMapViewType.normal.rawValue)
        print(GMSMapViewType.satellite.rawValue)
        print(GMSMapViewType.terrain.rawValue)
            print(SessionUtils.getMapType())}
        
        
        
        
        if SessionUtils.getMapType() == 1  || SessionUtils.getMapType() == 0 {
            mapView.mapType = GMSMapViewType.normal
        }
        else if SessionUtils.getMapType() == 2{
            mapView.mapType = GMSMapViewType.satellite
        }
        else if SessionUtils.getMapType() == 3 {
            mapView.mapType = GMSMapViewType.terrain
        }
        else if SessionUtils.getMapType() == 4 {
            mapView.mapType = GMSMapViewType.hybrid
        }
        else{
            mapView.mapType = GMSMapViewType.hybrid
        }
        
        
        
        
    }
    
    func manageDataOfLocations(listOfLocations: [MapInteraction])
    {
        for i in (0..<listOfLocations.count)
        {
            let value = "\(i+1)";
            let latitude = Double(listOfLocations[i].lat!)!
            let longitude = Double(listOfLocations[i].long!)!
            let currentLat = Double(listOfLocations[i].current_lat!)!
            let currentLong = Double(listOfLocations[i].current_long!)!
            
            let positionCurrent = CLLocationCoordinate2D(latitude: currentLat, longitude: currentLong)
            let position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            
            let markerCurrent = GMSMarker(position: positionCurrent)
            let marker = GMSMarker(position: position)
            
            let imageCurrent = Utils.customImage(diameter: 30, color: Utils.hexStringToUIColor(hex: "#FF0000"))
            let imageAddress = Utils.customImage(diameter: 30, color: Utils.hexStringToUIColor(hex: "#2196F3"))
            
            marker.icon = Utils.textToImage(drawText: value as NSString, inImage: imageAddress)
            markerCurrent.icon = Utils.textToImage(drawText: value as NSString, inImage: imageCurrent)
            
            marker.map = mapView
            markerCurrent.map = mapView
        }
    }
    
    func getConverstation(startDate: String,endDate: String)
    {
        mapView.clear()
        //present(alert!, animated: false, completion: nil)
        let parameters =  [
            "start": startDate,
            "end": endDate,
            "user_id": "\(employee!.id!)",
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/interactions";
        
        Alamofire.request(resourceString, method: .patch, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            if Constants.LOGGING_ENABLED{
                                print("Response: \(value)")}
                            //self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode([MapInteraction].self, from: jsonData!)
                            self.manageDataOfLocations(listOfLocations: res)
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 422 {
                        //self.alert?.dismiss(animated: false, completion: nil)
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        Toast.show(message: "Given Data is Invalid", controller: self)
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                        print (response_code)
                        print(response.result.value!)
                        print("Error is \(response.result.value!)")
                        }
                        //self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                //self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
            }
            
        })
    }
    
    @objc
    func showMyLocation()
    {
            
        if CLLocationManager.locationServicesEnabled() {
            let latitude = locationManager.location?.coordinate.latitude
            let longitude = locationManager.location?.coordinate.longitude
            
            if latitude == nil || longitude == nil || latitude == 0.0 || longitude == 0.0
            {
                Toast.show(message: "Enable location from settings", controller: self)
            }else
            {
                mapView.camera = GMSCameraPosition(latitude: latitude!, longitude: longitude!, zoom: 18.0)
            }
        }
        
    }
    
    
    
}
