//
//  CustomerDetailViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 01/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import MessageUI

protocol DeleteCustomerDelegate {
    func deleteCustomer(index: Int, customer: Customer,clusterIndex: Int)
}


class CustomerDetailViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    //Delegates
    var deleteCustomerDelegate: DeleteCustomerDelegate?
    var editCustomerDelegate: EditCustomerDelegatee?
    
    public var customer: Customer?
    public var selectedIndex: Int?
    public var clusterIndex: Int?
    
    //Images
    @IBOutlet weak var deletImage: UIImageView!
    @IBOutlet weak var editImage: UIImageView!
    @IBOutlet weak var emailImage: UIImageView!
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var phoneImage: UIImageView!
    
    @IBOutlet weak var ageLbl: UILabel!
    //Views
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var callView: UIView!
    
    //Customer Detail Veriables
    //Views
    @IBOutlet weak var homePhoneView: UIView!
    @IBOutlet weak var workPhoneView: UIView!
    
    //Height Constraints
    
    @IBOutlet weak var personalEmailLbl: UILabel!
    @IBOutlet weak var homePhoneHeight: NSLayoutConstraint!
    @IBOutlet weak var workPhoneHeight: NSLayoutConstraint!
    
    @IBOutlet weak var creditRatingLbl: UILabel!
    @IBOutlet weak var personalPhoneLbl: UILabel!
    //Labels
    
    @IBOutlet weak var incomeLbl: UILabel!
    @IBOutlet weak var networthLbl: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    
    var addressInformationControlller:AddressInformationViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setup Icons
        setUpIcons()
        //Setup Gesture of View
        setUpExtraGestures()
        //Setup Navigation Items
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = (customer?.firstName)! + " " + (customer?.lastName)!
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        //Setup Customer Data
        loadCustomerInformation()
        
    }
    
    func loadCustomerInformation()
    {
        nameLabel.text = (customer?.firstName)! + " " + (customer?.lastName)!
        ageLbl.text = customer?.age ?? "-"
        genderLabel.text = customer?.genderStr ?? customer?.gender
        personalEmailLbl.text = customer?.personalEmail ?? "-"
        personalPhoneLbl.text = customer?.mobilePhone ?? "-"
        incomeLbl.text = customer?.income ?? "-"
        creditRatingLbl.text = customer?.credit_rating ?? "-"
        networthLbl.text = customer?.net_worth ?? "-"
        
        
        
    }
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setUpExtraGestures()
    {
        let callGesture = UITapGestureRecognizer(target: self, action: #selector(callAction))
        callView.addGestureRecognizer(callGesture)
        
        let messageGesture = UITapGestureRecognizer(target: self, action: #selector(messageAction))
        messageView.addGestureRecognizer(messageGesture)
        
        let editGesture = UITapGestureRecognizer(target: self, action: #selector(editAction))
        editView.addGestureRecognizer(editGesture)
        
        let emailGesture = UITapGestureRecognizer(target: self, action: #selector(emailAction))
        emailView.addGestureRecognizer(emailGesture)
        
        let deleteGesture = UITapGestureRecognizer(target: self, action: #selector(deleteAction))
        deleteView.addGestureRecognizer(deleteGesture)
        
    }
    
    @objc
    func callAction()
    {
        
        if(customer!.mobilePhone != nil || customer!.mobilePhone == "" )
        {
            call(number: customer!.mobilePhone!)
        }
        else
        {
            Toast.show(message: "No Phone Numbers", controller: self)
        }
    }
    
    @objc
    func messageAction()
    {
        if Constants.LOGGING_ENABLED{
            print(customer!.mobilePhone!)}
        if(customer!.mobilePhone! != "")
        {
            UIApplication.shared.open(URL(string: "sms:\(String(describing: customer!.mobilePhone!))")!, options: [:], completionHandler: nil)
        }
        else
        {
            Toast.show(message: "No number available", controller: self)
        }
        
    }
    
    @objc
    func emailAction()
    {
        sendEmail()
    }
    
    @objc
    func editAction()
    {
        let customerViewController = UIStoryboard.init(name: "EditCustomer", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditCustomerViewController") as! EditCustomerViewController
        customerViewController.customer = self.customer
        if Constants.LOGGING_ENABLED{
        print("\(customerViewController.customer!.id!)")
        }
        customerViewController.clusterIndex = clusterIndex
        customerViewController.index = selectedIndex;
        customerViewController.editCustomerDelegate = addressInformationControlller
        self.navigationController?.pushViewController(customerViewController, animated: false);
    }
    
    @objc
    func deleteAction()
    {
        
        let refreshAlert = UIAlertController(title: "", message: "If you choose Delete then customer and address both will delete. Are you sure to delete?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
            self.deleteCustomerDelegate?.deleteCustomer(index: self.selectedIndex!, customer: self.customer!,clusterIndex: self.clusterIndex!)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: false, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }
    
    func setUpIcons()
    {
        self.phoneImage.layer.borderWidth = 1
        self.phoneImage.layer.borderColor = UIColor.white.cgColor
        self.phoneImage.layer.cornerRadius = self.phoneImage.frame.height/2
        
        self.messageImage.layer.borderWidth = 1
        self.messageImage.layer.borderColor = UIColor.white.cgColor
        self.messageImage.layer.cornerRadius = self.messageImage.frame.height/2
        
        self.emailImage.layer.borderWidth = 1
        self.emailImage.layer.borderColor = UIColor.white.cgColor
        self.emailImage.layer.cornerRadius = self.emailImage.frame.height/2
        
        self.editImage.layer.borderWidth = 1
        self.editImage.layer.borderColor = UIColor.white.cgColor
        self.editImage.layer.cornerRadius = self.editImage.frame.height/2
        
        self.deletImage.layer.borderWidth = 1
        self.deletImage.layer.borderColor = UIColor.white.cgColor
        self.deletImage.layer.cornerRadius = self.deletImage.frame.height/2
    }
    
    func call(number: String)
    {
        if let phoneCallURL:URL = URL(string: "tel:\(number)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                let alertController = UIAlertController(title: "Connect with Customer", message: "Are you sure you want to call \n\(number)?", preferredStyle: .alert)
                let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                    application.open(phoneCallURL, options:[:], completionHandler: nil)
                })
                let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
                    
                })
                alertController.addAction(yesPressed)
                alertController.addAction(noPressed)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func sendEmail() {
        if Constants.LOGGING_ENABLED{
            print("email started")}
        var email: String = ""
            email = customer!.personalEmail ?? ""
        if email == ""{
            Toast.show(message: "No Email", controller: self)
        }else{
           if MFMailComposeViewController.canSendMail() {
               let mail = MFMailComposeViewController()
               mail.mailComposeDelegate = self
               mail.setToRecipients(["\(email)"])
               mail.setMessageBody("<p>Message from Sales Pro</p>", isHTML: true)
               present(mail, animated: true)
           } else {
            if Constants.LOGGING_ENABLED{
               print("Email Not Wroking")
            }
           }
       }
    }
    
    

       func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
           controller.dismiss(animated: true)
       }
    
    
}

extension CustomerDetailViewController: EditCustomerDelegate{
    func editCustomer(customer: Customer) {
        //self.navigationController?.popViewController(animated: true)
        self.editCustomerDelegate?.editCustomer(index: selectedIndex!, customer: customer, clusterIndex: clusterIndex!)
    }
    
    
}
