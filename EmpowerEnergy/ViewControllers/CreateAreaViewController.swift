//
//  CreateAreaViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 13/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import GoogleMaps
import GoogleMapsUtils
import SwiftyJSON
import Alamofire

protocol CreateAreaDelegate
{
    func createArea(area: CreateAreaResponse,users: [UsersResponse])
}

class CreateAreaViewController: UIViewController, UITextFieldDelegate {
    
    var createAreaDelegate: CreateAreaDelegate?
    
    @IBOutlet weak var popUpBackGroundView: UIView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var popupTableView: UITableView!
    @IBOutlet var popup: UIView!
    var drawCoordinates = [CLLocationCoordinate2D]()
    var employees: [Employee] = []
    var selectedColor = ""
    var locationsList:[DLocation] = []
    var checkList: [Int] = []
    var areaUsers: [UsersResponse] = []
    
    @IBOutlet weak var areaNameEditText: TextField!
    @IBOutlet weak var employeesTableView: UITableView!
    
    @IBOutlet weak var areaColorImage: UIImageView!
    
   
    var officesList: [Office] = []
    var geography: NSString = ""
    var users: NSString = ""
    var areaName: String = ""
    var arrayr: [String: Any] = [:]
    
    var alert: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Create New Area"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        areaColorImage.backgroundColor = UIColor(hexString: selectedColor)
        
        areaNameEditText.tintColor = UIColor(hexString: selectedColor)
        
        popUpBackGroundView.isHidden = true
        popup.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        
        let dissmissPopupGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup))
        popUpBackGroundView.addGestureRecognizer(dissmissPopupGesture)
        
        let formator = DateFormatter()
        formator.dateFormat = "yyyy-MM-dd HH:mm:ss"
        for i in (0..<ViewController.interactionResponse!.users!.count)
        {
            let id = ViewController.interactionResponse!.users![i].id!
            let firstname = ViewController.interactionResponse!.users![i].first_name!
            let lastname = ViewController.interactionResponse!.users![i].last_name ?? ""
            let email = ViewController.interactionResponse!.users![i].email!
            let created = Date()
            let updated = Date()
            let type = ViewController.interactionResponse!.users![i].role!
            
            employees.append(Employee(id: id,firstName: firstname,lastName: lastname, email: email, created: created, modified: updated, type: type.name!,offices: ViewController.interactionResponse!.users![i].companies!,parent: ViewController.interactionResponse!.users![i].parent_id!,phone: ViewController.interactionResponse!.users![i].phone ?? ""))
        }
        
        for i in (0..<drawCoordinates.count)
        {
            locationsList.append(DLocation(latitude: drawCoordinates[i].latitude, longitude: drawCoordinates[i].longitude))
            
        }
        
        officesList = SessionUtils.getOffices()
        
        employeesTableView.reloadData()
        
        self.areaNameEditText.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.tableHeight.constant
            = self.popupTableView.contentSize.height
        self.popup.frame.size.height =  self.tableHeight.constant
        
    }
    
    
    @IBAction func onSaveClick(_ sender: Any) {
        self.view.endEditing(true)
        areaName = areaNameEditText.text!
        areaUsers = []
        
        if(areaName == "")
        {
            Toast.show(message: "Please enter area Name", controller: self)
            
        }
        else if(checkList.count<1)
        {
            Toast.show(message: "Please choose employee.", controller: self)
        }else
        {
            var employeesIds:[Int] = []
            do{
                let jsonData = try JSONEncoder().encode(locationsList)
                self.geography  = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)!
                
                for i in (0..<checkList.count)
                {
                    employeesIds.append(ViewController.interactionResponse!.users![checkList[i]].id!)
                    let user = ViewController.interactionResponse!.users![checkList[i]]
                    areaUsers.append(user)
                    
                }
                
                let jsonEmployeeIds = try JSONEncoder().encode(employeesIds)
                self.users = NSString(data: jsonEmployeeIds, encoding: String.Encoding.utf8.rawValue)!
                
                
                popupTableView.reloadData()
                viewWillLayoutSubviews()
                self.view.addSubview(popup)
                self.popUpBackGroundView.isHidden = false
                viewWillLayoutSubviews()
                
                
                
                
            }catch{
                if Constants.LOGGING_ENABLED{
                    print(error)}}
        }
    }
    
    func createArea(name: String,colorStr: String, geography: String, office_id: Int, users: String, created_by: Int)
    {
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "name": name,
            "created_by": created_by,
            "color": colorStr,
            "office_id": office_id,
            "users": users,
            "geography": geography,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/areas";
        
        Alamofire.request(resourceString, method: .post, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let myAreaResponse = try decoder.decode(CreateAreaResponse.self, from: jsonData!)
                            self.createAreaDelegate?.createArea(area: myAreaResponse,users: self.areaUsers)
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 401{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Employee already exists / Server Error.", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    func convertIntoJSONString(arrayObject: [Any]) -> String? {
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
            
        } catch let error as NSError {
            if Constants.LOGGING_ENABLED{
            print("Array convertIntoJSON - \(error.description)")
            }
        }
        return nil
    }
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    var selectIndex = -1;
    
    @objc
    func imageClick(tapGesture: UITapGestureRecognizer)
    {
        let imageView = tapGesture.view as! UIImageView
        let index = imageView.tag
        let indexPath = IndexPath(row: index, section: 0)
        let cell = self.employeesTableView.cellForRow(at: indexPath) as! EmployeeCell
        
        if(cell.checkBoxImage.tintColor == UIColor.systemGray2)
        {
            cell.checkBoxImage.tintColor = UIColor(named: "mainBlue")
            cell.checkBoxImage.image = UIImage(systemName: "checkmark.circle.fill")
            onEmployeeCheckClick(position: index, isChecked: true)
            
        }else
        {
            cell.checkBoxImage.tintColor = UIColor.systemGray2
            cell.checkBoxImage.image = UIImage(systemName: "circle")
            onEmployeeCheckClick(position: index, isChecked: false)
        }
        
    }
    
    
    func onEmployeeCheckClick(position: Int, isChecked: Bool)
    {
        var checkTempList: [Int] = checkList
        if isChecked
        {
            checkTempList.append(position)
        }
        else
        {
            for i in (0..<checkList.count)
            {
                if(checkList[i] == position)
                {
                    checkTempList.remove(at: i)
                }
            }
            
        }
        checkList = checkTempList
        
        for i in (0..<checkList.count)
        {
            if Constants.LOGGING_ENABLED{
                print("\(checkList[i])")}
        }
        
        
    }
    
    
    
    @objc
       func dismissPopup()
       {
           self.popup.removeFromSuperview()
           self.popUpBackGroundView.isHidden = true
       }
    
}


extension CreateAreaViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == employeesTableView)
        {
        return employees.count
        }else{
           return officesList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == employeesTableView{
        let cell = employeesTableView.dequeueReusableCell(withIdentifier: "areaemployee") as! EmployeeCell
        cell.checkBoxImage.tag = indexPath.row
        cell.checkBoxImage.isUserInteractionEnabled = true
        let imageGesture = UITapGestureRecognizer(target: self, action: #selector(imageClick(tapGesture:)))
        imageGesture.numberOfTapsRequired = 1
        cell.checkBoxImage.addGestureRecognizer(imageGesture)
        
        cell.employeeNameLabel.text = "\(employees[indexPath.row].firstName ?? "") \(employees[indexPath.row].lastName ?? "")"
        
        return cell;
        }else{
            let cell = popupTableView.dequeueReusableCell(withIdentifier: "popup", for: indexPath) as! OfficePopupCell
            cell.label.text = officesList[indexPath.row].name!
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        self.viewWillLayoutSubviews()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popupTableView
        {
            dismissPopup()
            createArea(name: areaName, colorStr: selectedColor, geography: geography as String, office_id: officesList[indexPath.row].id!, users: users as String, created_by: SessionUtils.getUserId())
        }
    }
    
    
    
    
}

class EmployeeCell: UITableViewCell
{
    
    @IBOutlet weak var checkBoxImage: UIImageView!
    @IBOutlet weak var employeeNameLabel: UILabel!
}


extension Array where Element: Encodable {
    func asArrayDictionary() throws -> [[String: Any]] {
        var data: [[String: Any]] = []
        
        for element in self {
            data.append(try element.asDictionary())
        }
        return data
    }
}

extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}

class OfficePopupCell: UITableViewCell
{
    
    @IBOutlet weak var label: UILabel!
}
