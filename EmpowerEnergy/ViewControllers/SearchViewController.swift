//
//  SearchViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 04/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SkeletonView

protocol AddressSearchDelegate {
    func addressSearchResult(address: InteractionAddressData)
}

protocol CustomerSearchDelegate
{
    func customerSearchResult(customerResult: InteractionAddressData)
}


class SearchViewController: UIViewController, UITextFieldDelegate {
    
    var addressSearchDelegate: AddressSearchDelegate?
    var customerSearchDelegate: CustomerSearchDelegate?
    
    @IBOutlet weak var customerSearchTextField: UITextField!
    @IBOutlet weak var customerview: UIView!
    @IBOutlet weak var addressSearchButton: UIView!
    @IBOutlet weak var customerTableViewheight: NSLayoutConstraint!
    @IBOutlet weak var customerTableView: UITableView!
    @IBOutlet weak var addressTableHeight: NSLayoutConstraint!
    @IBOutlet weak var addressSearchView: UIView!
    @IBOutlet weak var addressSearchTextField: UITextField!
    @IBOutlet weak var customerSearchButton: UIView!
    @IBOutlet weak var addressSearchProgressBar: UIActivityIndicatorView!
    @IBOutlet weak var addressTableView: UITableView!
    
    @IBOutlet weak var customerProgressIndicator: UIActivityIndicatorView!
    var addressResults: [InteractionAddressData] = []
    
    var customerResults: [InteractionAddressData] = []
    
    @IBOutlet weak var customerClickViewHeight: NSLayoutConstraint!
    @IBOutlet weak var customerClickView: UIView!
    
    var statusBarFrame: CGRect?
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Search Location"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.backItem?.title = "Back"
        
        //setting up
        addressSearchTextField.addTarget(self, action: #selector(addressTextFieldChange(_:)), for: .editingChanged)
        
        customerSearchTextField.addTarget(self, action: #selector(customerTextFieldChange(_:)), for: .editingChanged)
        
        let customerGesture = UITapGestureRecognizer(target: self, action: #selector(enableCustomerSearch))
        customerSearchButton.addGestureRecognizer(customerGesture)
        
        let addressGesture = UITapGestureRecognizer(target: self, action: #selector(enableAddressSearch))
        addressSearchButton.addGestureRecognizer(addressGesture)
        
        addressIndicator.isHidden = true
        customerProgressIndicator.isHidden = true
        
        if(SessionUtils.getUserType() == "Admin" || SessionUtils.getUserType() == "admin" ||  SessionUtils.getUserType() == "SuperAdmin" || SessionUtils.getUserType() == "Superadmin" || SessionUtils.getUserType() == "superadmin" )
        {
            customerClickView.isHidden = false
            customerClickViewHeight.constant = 49
        }
        else
        {
            customerClickView.isHidden = true
            customerClickViewHeight.constant = 0
        }
        
        self.addressSearchTextField.delegate = self
        self.customerSearchTextField.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    @objc func enableCustomerSearch(){
        
        if(addressIndicator.isAnimating)
        {
            Toast.show(message: "Already Searching Please Wait...", controller: self)
        }else
        {
        customerview.isHidden = false
        addressSearchView.isHidden = true
        customerTableView.reloadData()
        }
    }
    
    @objc func enableAddressSearch(){
        if(customerProgressIndicator.isAnimating)
        {
            Toast.show(message: "Already Searching Please Wait...", controller: self)
        }else
        {
        addressSearchView.isHidden = false
        customerview.isHidden = true
        addressTableView.reloadData()
        }
    }
    
    
    @IBOutlet weak var addressIndicator: UIActivityIndicatorView!
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.addressTableHeight.constant
            = self.addressTableView.contentSize.height
        
        self.customerTableViewheight.constant = self.customerTableView.contentSize.height
    }
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    var textString = ""
    var count = 0;
    var timer: Timer?
    var customerTimer: Timer?
    
    @objc func addressTextFieldChange(_ textfield: UITextField)
    {
        timer?.invalidate()
        timer = nil
        count = 0
        textString = textfield.text!
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(timerAddress
            ), userInfo: ["foo": "bar"], repeats: true)
    }
    
    @objc func customerTextFieldChange(_ textfield: UITextField)
    {
        customerTimer?.invalidate()
        customerTimer = nil
        count = 0
        textString = textfield.text!
        customerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(timerCustomer
            ), userInfo: ["foo": "bar"], repeats: true)
    }
    
    @objc func timerAddress(){
        count += 1
        if count == 5
        {
            timer!.invalidate()
            if(textString != "")
            {
                doAddressSearch()
            }
            count = 0
        }
        
    }
    
    @objc func timerCustomer(){
           count += 1
           if count == 5
           {
               customerTimer!.invalidate()
               if(textString != "")
               {
                   doCustomerSearch()
               }
               count = 0
           }
           
       }
    
    
       func hideSkeleton()
       {
           addressTableView.stopSkeletonAnimation()
           self.view.hideSkeleton()
       }
    
    func showAddressSkeletonView()
    {
        addressTableView.isSkeletonable = true
        addressTableView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .lightGray), animation: nil,  transition: .crossDissolve(0.25))
    }
    
    func doCustomerSearch()
    {
        if Constants.LOGGING_ENABLED{
            print("\(textString)")
            print("\(textString)")}
        customerSearch(query: textString)
    }
    
    func doAddressSearch()
    {
        if Constants.LOGGING_ENABLED{
        print("\(textString)")
            print("\(textString)")}
        addressSearch(query: textString)
    }
    
    //Search By Address
    func addressSearch(query: String)
    {
        addressResults = []
        addressIndicator.isHidden = false
        addressIndicator.startAnimating()
        let parameters =  [
            "query": query,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/search-address";
        
        Alamofire.request(resourceString, method: .get, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            if Constants.LOGGING_ENABLED{
                                print(value)}
                            self.addressIndicator.isHidden = true
                            self.addressIndicator.stopAnimating()
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode([InteractionAddressData].self, from: jsonData!)
                            if Constants.LOGGING_ENABLED{
                                print("Size of Address Search Results - \(res.count)")}
                            self.addressResults = res
                            //self.hideSkeleton()
                            self.addressTableView.reloadData()
                            self.viewWillLayoutSubviews()
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 401{
                        //self.hideSkeleton()
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        self.addressIndicator.isHidden = true
                        self.addressIndicator.stopAnimating()
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        //self.hideSkeleton()
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        self.addressIndicator.isHidden = true
                        self.addressIndicator.stopAnimating()
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                //self.hideSkeleton()
                self.addressIndicator.isHidden = true
                self.addressIndicator.stopAnimating()
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    //Search By Customer
    func customerSearch(query: String)
    {
        customerResults = []
        customerProgressIndicator.isHidden = false
        customerProgressIndicator.startAnimating()
        let parameters =  [
            "query": query,
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/search-owner";
        
        Alamofire.request(resourceString, method: .get, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            if Constants.LOGGING_ENABLED{
                                print(value)}
                            self.customerProgressIndicator.isHidden = true
                            self.customerProgressIndicator.stopAnimating()
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode([InteractionAddressData].self, from: jsonData!)
                            if Constants.LOGGING_ENABLED{
                                print("Size of Address Search Results - \(res.count)")}
                            self.customerResults = res
                            self.customerTableView.reloadData()
                            self.viewWillLayoutSubviews()
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 401{
                        if Constants.LOGGING_ENABLED{
                            print(response.result.value!)}
                        self.customerProgressIndicator.isHidden = true
                        self.customerProgressIndicator.stopAnimating()
                        Toast.show(message: "Unauthorized User", controller: self)
                        let viewController = UIStoryboard.init(name: "SignIn", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(viewController!, animated: false);
                        //self.present(viewController!,animated: true,completion: nil)
                        
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                        print(response.result.value!)}
                        self.customerProgressIndicator.isHidden = true
                        self.customerProgressIndicator.stopAnimating()
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                if Constants.LOGGING_ENABLED{
                print(error)}
                self.customerProgressIndicator.isHidden = true
                self.customerProgressIndicator.stopAnimating()
                if Constants.LOGGING_ENABLED{
                    print(error)}
                Toast.show(message: "No Internet Connection / Server Error", controller: self)
            }
            
        })
    }
    
    
    
    
}

extension SearchViewController : SkeletonTableViewDelegate,SkeletonTableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == addressTableView)
        {
            return addressResults.count
        }else
        {
            return customerResults.count
        }
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        if(skeletonView == addressTableView){
            return "addresscell" as ReusableCellIdentifier
        }else{
             return "customeraddresscell" as ReusableCellIdentifier
        }
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == addressTableView)
        {
            let cell = addressTableView.dequeueReusableCell(withIdentifier: "addresscell",for: indexPath) as! AddressCell
            if(addressResults.count > indexPath.row)
            {
            cell.addressLbl.text = addressResults[indexPath.row].address!
            cell.addressDetailLabel.text = "\(addressResults[indexPath.row].address!) \(addressResults[indexPath.row].city?.name ?? "") \(addressResults[indexPath.row].city?.state?.name ?? "")"
                
            }
            return cell
        }else
        {
            let cell = customerTableView.dequeueReusableCell(withIdentifier: "customeraddresscell",for: indexPath) as! CustomerSearchCell
            cell.customerNameLabel.text = "\(customerResults[indexPath.row].first_name!) \(customerResults[indexPath.row].last_name!)"
            
//            if customerResults[indexPath.row].credit_rating! == "" || customerResults[indexPath.row].credit_rating! == "Unknown" Int(customerResults[indexPath.row].net_income!) == 0
//            {
//                cell.decriptionLabel.text = "0 - 0 credit, $0K income"
//            }
//            else
//            {
                cell.decriptionLabel.text = "\(customerResults[indexPath.row].credit_rating!) credit, \(customerResults[indexPath.row].income!) income"
            //}
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == addressTableView)
        {
            
            if(indexPath.row < addressResults.count){
            addressSearchDelegate?.addressSearchResult(address: addressResults[indexPath.row])
            }
        }
        else
        {
            if( indexPath.row < customerResults.count){
            customerSearchDelegate?.customerSearchResult(customerResult: customerResults[indexPath.row])
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        self.viewWillLayoutSubviews()
    }
    
    
}


class AddressCell: UITableViewCell
{
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var addressDetailLabel: UILabel!
}

class CustomerSearchCell: UITableViewCell
{
    
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var decriptionLabel: UILabel!
    
    
}

