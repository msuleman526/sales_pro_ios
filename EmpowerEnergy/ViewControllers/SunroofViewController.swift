//
//  SunroofViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 14/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import WebKit

class SunroofViewController: UIViewController, WKNavigationDelegate{
    
    @IBOutlet weak var webView: WKWebView!
    
    var alert : UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Sunroof"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        present(alert!, animated: false, completion: nil)
        webView.navigationDelegate = self
        
        //Initailize Sunroof
        webView.load(URLRequest(url: URL(string: "https://www.google.com/get/sunroof/building/42.9586191/-76.9087279/")!))
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         self.alert?.dismiss(animated: false, completion: nil)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Toast.show(message: "Internet Issue, try again!", controller: self)
    }
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //    func webViewDidFinishLoad(_ webView: UIWebView) {
    //
    //    }
    
}
