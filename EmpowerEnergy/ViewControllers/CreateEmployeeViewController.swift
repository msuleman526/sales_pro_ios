//
//  CreateEmployeeViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 25/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit

protocol AddEmployeeUserDelegate{
    func addEmployee(user: UsersResponse)
}

class CreateEmployeeViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var lastNameTextField: UITextField!
    var addEmployeeDelegate: AddEmployeeUserDelegate?
    
    @IBOutlet weak var officeTextField: DropDown!
    @IBOutlet weak var assignTextField: DropDown!
    @IBOutlet weak var userTypeTextField: DropDown!
    
    @IBOutlet weak var tableHeight3: NSLayoutConstraint!
    @IBOutlet var popup3: UIView!
    @IBOutlet weak var popupTableView1: UITableView!
    var office_id = 0
    var assigned_to = 0
    var user_type: Role?
    @IBOutlet weak var passwordLabel: UITextField!
    @IBOutlet weak var workEmailLabel: UITextField!
    @IBOutlet weak var workPhoneLabel: UITextField!
    @IBOutlet weak var nameLabel: UITextField!
    
    var managersList: [UsersResponse] =  []
    var managersListStr: [String] =  []
    
    @IBOutlet weak var tableHeight1: NSLayoutConstraint!
    @IBOutlet var popup1: UIView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordEye: UIImageView!
    @IBOutlet weak var popUpBackGroundView: UIView!
    @IBOutlet weak var popupTableView: UITableView!
    @IBOutlet weak var popupTableView3: UITableView!
    @IBOutlet var popup: UIView!
    var userTypeList: [String] = []
    var popUpList1: [String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Create User"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        for i in (0..<(ViewController.interactionResponse?.users!.count)!)
        {
            if(ViewController.interactionResponse?.users![i].role!.name! == "Manager")
            {
                managersList.append((ViewController.interactionResponse?.users![i])!)
                managersListStr.append(("\(String(describing: ViewController.interactionResponse?.users![i].first_name ?? "")) \(ViewController.interactionResponse?.users![i].last_name ?? "")"))
            }
        }
        
        userTypeList = []
        for i in (0..<(ViewController.interactionResponse?.roles!.count)!)
        {
            if ViewController.interactionResponse?.roles![i].name == "Sales Rep"
            {
                userTypeList.append("Sales Rep")
                
            }else{
                userTypeList.append((ViewController.interactionResponse?.roles![i].name!)!)
            }
        }
        
        popUpList1 = []
        if Constants.LOGGING_ENABLED{
            print("OFFCIECOUNT - \(SessionUtils.getOffices().count)")}
        for i in (0..<(SessionUtils.getOffices().count))
        {
            popUpList1.append((SessionUtils.getOffices()[i].name)!)
        }
        
        popUpBackGroundView.isHidden = true
        popup.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        popup1.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        popup3.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2)
        
//        let dissmissPopupGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup))
//        popUpBackGroundView.addGestureRecognizer(dissmissPopupGesture)
        
//        officeLabel.isUserInteractionEnabled = true
//
//
//        let officePopupGesture = UITapGestureRecognizer(target: self, action: #selector(officesClick))
//        officeLabel.addGestureRecognizer(officePopupGesture)
//
        
        
        office_id = SessionUtils.getOffices()[0].id!
        assigned_to = managersList[0].id!
        user_type = ViewController.interactionResponse?.roles![0]
        
        officeTextField.text = SessionUtils.getOffices()[0].name!
        assignTextField.text = managersList[0].name!
        userTypeTextField.text = userTypeList[0]
        
        //heightofPopup()
        //heightOfPopup3()
        //heightOfPopup1()
        
        self.nameLabel.delegate = self
        self.passwordLabel.delegate = self
        self.workPhoneLabel.delegate = self
        
        let eyeImage = UITapGestureRecognizer(target: self, action: #selector(passwordEyeClick))
        passwordEye.isUserInteractionEnabled = true
        passwordEye.addGestureRecognizer(eyeImage)
        passwordLabel.isSecureTextEntry = true
        
        setUpDropDownList()
        
        
    }
    
    
    func setUpDropDownList()
    {
        //self.userTypeTextField.addGestureRecognizer(UITapGestureRecognizer(target: nil, action: #selector(showDropDown)))
        userTypeTextField.optionArray = userTypeList
        userTypeTextField.didSelect{(selectedText , index ,id) in
              self.userTypeTextField.text = "\(selectedText)"
              
            self.user_type = ViewController.interactionResponse?.roles![index]
        }
        
        assignTextField.optionArray = managersListStr
        assignTextField.didSelect{(selectedText , index ,id) in
              self.assignTextField.text = "\(selectedText)"
              self.assigned_to = self.managersList[index].id!
        }
        
        officeTextField.optionArray = popUpList1
        officeTextField.didSelect{(selectedText , index ,id) in
              self.officeTextField.text = "\(selectedText)"
              self.office_id = SessionUtils.getOffices()[index].id!
        }
    }
    
    @objc func showDropDown()
    {
        //self.userTypeTextField.touchAction()
    }
    
    @objc func passwordEyeClick()
    {
        if passwordEye.image == UIImage(systemName: "eye.fill")
        {
            passwordEye.image = UIImage(systemName: "eye.slash.fill")
            passwordLabel.isSecureTextEntry = false
        }else
        {
            passwordEye.image = UIImage(systemName: "eye.fill")
            passwordLabel.isSecureTextEntry = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    
    @IBAction func saveClick(_ sender: Any) {
        
        self.view.endEditing(true)
        let mobilePhone = workPhoneLabel.text!
        let emailAddress = workEmailLabel.text!
        let full_name = nameLabel.text!
        let password = passwordLabel.text!
        let last_name = lastNameTextField.text ?? ""
        
        if(full_name == "")
        {
            Toast.show(message: "Please Enter Full Name", controller: self)
            
        }else if(emailAddress == "")
        {
            Toast.show(message: "Please Enter Work Email", controller: self)
            
        }else if(mobilePhone == "")
        {
            Toast.show(message: "Please Enter Work Phone", controller: self)
        }
        else if(!Utils.isValidEmail(email: emailAddress))
        {
            Toast.show(message: "Please Enter Valid Email", controller: self)
        }else if(password.count<8)
        {
            Toast.show(message: "Password is short. minimum length is 8.", controller: self)
        }else if(mobilePhone.count<6)
        {
            Toast.show(message: "Phone is too short", controller: self)
        }else{
            if Constants.LOGGING_ENABLED{
            print(office_id)
                print(assigned_to)}
            let user = UsersResponse(name: full_name,first_name: full_name,last_name: last_name, email: emailAddress, office_id: office_id, role: user_type!, parent: assigned_to, phone: mobilePhone,password: password)
            addEmployeeDelegate?.addEmployee(user: user)
            
        }
        
        
        
    }
    
//    @objc
//    func dismissPopup()
//    {
//        self.popup.removeFromSuperview()
//        self.popup1.removeFromSuperview()
//        self.popup3.removeFromSuperview()
//        self.popUpBackGroundView.isHidden = true
//    }
//
//    func heightofPopup()
//    {
//        self.tableHeight1.constant
//            = self.popupTableView.contentSize.height
//        self.popup.frame.size.height =  self.tableHeight.constant + 20
//    }
//
//    func heightOfPopup1()
//    {
//        self.tableHeight1.constant
//            = self.popupTableView1.contentSize.height
//
//        self.popup1.frame.size.height =  self.tableHeight1.constant
//    }
//
//    func heightOfPopup3()
//    {
//        self.tableHeight3.constant
//            = self.popupTableView3.contentSize.height
//
//        self.popup3.frame.size.height =  self.tableHeight3.constant + 10
//
//    }
    
    
    
//    @objc
//    func typesClick()
//    {
//        popupTableView.reloadData()
//        heightofPopup()
//        self.view.addSubview(popup)
//        self.popUpBackGroundView.isHidden = false
//        heightofPopup()
//    }
    
//    @objc
//    func officesClick()
//    {
//        popupTableView1.reloadData()
//        heightOfPopup1()
//        self.view.addSubview(popup1)
//        self.popUpBackGroundView.isHidden = false
//        heightOfPopup1()
//    }

    
    
}

//extension CreateEmployeeViewController: UITableViewDelegate, UITableViewDataSource
//{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
////        if tableView == popupTableView
////        {
////            return popUpList.count
////        }else
////        if tableView == popupTableView3
////        {
////            return managersList.count
////        }
////        else
////        {
//            return popUpList1.count
//        //}
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
////        if tableView == popupTableView
////        {
////            let cell = popupTableView.dequeueReusableCell(withIdentifier: "popup", for: indexPath) as! PopUpCelll
////            cell.label.text = popUpList[indexPath.row]
////            return cell
////        }
////        else
////        if tableView == popupTableView3
////        {
////            let cell = popupTableView3.dequeueReusableCell(withIdentifier: "popup2", for: indexPath) as! PopUpCelll2
////            cell.label.text = managersList[indexPath.row].name
////            return cell
////        }
////        else
////        {
//           let cell = popupTableView1.dequeueReusableCell(withIdentifier: "popup1", for: indexPath) as! PopUpCelll1
//            cell.label.text = popUpList1[indexPath.row]
//            return cell        //}
//    }
//
//
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////        if(tableView == popupTableView)
////        {
////            user_type = ViewController.interactionResponse?.roles![indexPath.row]
////            adminLabel.text = user_type!.name!
////        }
////        else
//        if tableView == popupTableView1
//        {
//
//            office_id = SessionUtils.getOffices()[indexPath.row].id!
//            officeLabel.text = SessionUtils.getOffices()[indexPath.row].name
//
//        }
//        //else{
//
//         //   assigned_to = managersList[indexPath.row].id!
//       //     managerLabel.text = managersList[indexPath.row].name!
//       // }
//        dismissPopup()
//    }
//
//
//
//
//}

class PopUpCelll: UITableViewCell{
    
    @IBOutlet weak var label: UILabel!
}

class PopUpCelll1: UITableViewCell{
    
    @IBOutlet weak var label: UILabel!
    
}
class PopUpCelll2: UITableViewCell
{
    @IBOutlet weak var label: UILabel!
}

