//
//  ForgotPasswordViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 06/10/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailTextField: TextField!
    var alert : UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Forgot Password"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
    }
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    @IBAction func submitAction(_ sender: Any) {
        let email = emailTextField.text
        if(email == "")
        {
            Toast.show(message: "Please fill the email", controller: self)
        }else
        {
            present(alert!, animated: false, completion: nil)
            let parametersLogin =  [
                "email": email,
            ]
            
            let header = [
                "Accept": "text/json"
            ]
            
            let resourceString = "\(Utils.requestURL)api/auth/reset";
            
            Alamofire.request(resourceString, method: .post, parameters: parametersLogin, headers: header).responseString(completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    if let httpURLResponse = response.response{
                        let response_code = httpURLResponse.statusCode;
                        if response_code == 200{
                            self.alert?.dismiss(animated: false, completion: nil)
                            Toast.show(message: "Confirmation email sent", controller: self)
                            self.navigationController?.popViewController(animated: true)
                        }
                        else{
                             //print(response.result.value)
                            if Constants.LOGGING_ENABLED{
                                print(response.result.value!)}
                            self.alert?.dismiss(animated: false, completion: nil)
                            Toast.show(message: "No Internet Connection/ Server Issue", controller: self)
    
                            
                        }
                    }
                case .failure(let error):
                    //print(response.result.value)
                    self.alert?.dismiss(animated: false, completion: nil)
                    Toast.show(message: "\(error)", controller: self)
                }
                
            })
        }
    }
}
