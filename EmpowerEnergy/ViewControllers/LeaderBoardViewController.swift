//
//  LeaderBoardViewController.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 23/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class LeaderBoardViewController: UIViewController {
    
    
    @IBOutlet weak var sortByImage1: UIImageView!
    var selectDateFilterIndex = 0
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var popupBackgroundView: UIView!
    @IBOutlet weak var popUpBackgroudView: UIView!
    @IBOutlet weak var officeStatsTableView: UITableView!
    @IBOutlet weak var dateFilterCollectionView: UICollectionView!
    @IBOutlet weak var saleImage1: UIImageView!
    @IBOutlet weak var appCompleteImage1: UIImageView!
    @IBOutlet weak var appSetImage1: UIImageView!
    @IBOutlet weak var totalImage1: UIImageView!
    @IBOutlet weak var sortView1: UIView!
    @IBOutlet weak var officesView: UIView!
    
    @IBOutlet weak var sortByImage2: UIImageView!
    
    
    @IBOutlet weak var popupTableView: UITableView!
    @IBOutlet weak var saleImage2: UIImageView!
    @IBOutlet weak var appComImage2: UIImageView!
    @IBOutlet weak var appSetImage2: UIImageView!
    @IBOutlet weak var totalImage2: UIImageView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var repsTabView: UIView!
    @IBOutlet weak var officeBtnView: UIView!
    @IBOutlet weak var officeLbl: UILabel!
    @IBOutlet weak var officeBottomView: UIView!
    @IBOutlet weak var repsLbl: UILabel!
    @IBOutlet weak var repsBtnView: UIView!
    @IBOutlet weak var repsBtn: UIView!
    
    @IBOutlet weak var statsCollectionView: UICollectionView!
    
    var dateFilters: DateFilter = DateFilter()
    
    var alert: UIAlertController?
    
    
    var selectOfficeIndex: Int = 0
    var sortIndex: Int = 0
    
    var popupList: [Popup] = []
    
    var officeResponse: [OfficesDataResponse] = []
    var statsList: [Stats] = []
    var officeStats: [OfficeStats] = []
    var employeeStats: [EmployeeStats] = []
    
    @IBOutlet weak var employeeStatsTableView: UITableView!
    
    @IBOutlet weak var sortView: UIView!
    @IBOutlet var popup: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "blueDarkBlue")
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "LeaderBoard"
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.backItem?.title = "Back"
        
        
        alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating();
        alert!.view.addSubview(loadingIndicator)
        
        popupBackgroundView.isHidden = true
        
        
        popupList.append(Popup(index: 0, name: "Sort by Total"))
        popupList.append(Popup(index: 0, name: "Sort by App Set"))
        popupList.append(Popup(index: 0, name: "Sort by App Complete"))
        popupList.append(Popup(index: 0, name: "Sort by Sale"))
        
        mainView.isHidden = true
        
        
        
        officesView.isHidden = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(officeTabClick))
        officeBtnView.addGestureRecognizer(tapGesture)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(repsTabClick))
        repsBtn.addGestureRecognizer(tapGesture1)
        
        dateFilters.filters = []
        dateFilters.filters?.append(Today())
        dateFilters.filters?.append(Yesterday())
        dateFilters.filters?.append(ThisWeek())
        dateFilters.filters?.append(LastWeek())
        dateFilters.filters?.append(ThisMonth())
        dateFilters.filters?.append(LastMonth())
        dateFilters.filters?.append(ThisYear())
        dateFilters.filters?.append(LastYear())
        
        getConverstation(startDate: Today.getStartDate(), endDate: Today.getEndDate())
        
        setUpImages()
        
        appSetImage1.isUserInteractionEnabled = true
        appSetImage2.isUserInteractionEnabled = true
        totalImage2.isUserInteractionEnabled = true
        totalImage1.isUserInteractionEnabled = true
        appComImage2.isUserInteractionEnabled = true
        appCompleteImage1.isUserInteractionEnabled = true
        saleImage1.isUserInteractionEnabled = true
        saleImage2.isUserInteractionEnabled = true
        
        let dissmissPopupGesture = UITapGestureRecognizer(target: self, action: #selector(dismissPopup))
        popupBackgroundView.addGestureRecognizer(dissmissPopupGesture)
        
        let sortPopupGesture1 = UITapGestureRecognizer(target: self, action: #selector(sortByAppSet))
        let sortPopupGesture11 = UITapGestureRecognizer(target: self, action: #selector(sortByAppSet))
        appSetImage1.addGestureRecognizer(sortPopupGesture11)
        appSetImage2.addGestureRecognizer(sortPopupGesture1)
        
        let sortPopupGesture2 = UITapGestureRecognizer(target: self, action: #selector(sortByAppCom))
        let sortPopupGesture22 = UITapGestureRecognizer(target: self, action: #selector(sortByAppCom))
        appCompleteImage1.addGestureRecognizer(sortPopupGesture22)
        appComImage2.addGestureRecognizer(sortPopupGesture2)
        
        let sortPopupGesture3 = UITapGestureRecognizer(target: self, action: #selector(sortBySale))
        let sortPopupGesture33 = UITapGestureRecognizer(target: self, action: #selector(sortBySale))
        saleImage1.addGestureRecognizer(sortPopupGesture33)
        saleImage2.addGestureRecognizer(sortPopupGesture3)
        
        let sortPopupGesture = UITapGestureRecognizer(target: self, action: #selector(sortByTotal))
        let sortPopupGesturee = UITapGestureRecognizer(target: self, action: #selector(sortByTotal))
        totalImage1.addGestureRecognizer(sortPopupGesturee)
        totalImage2.addGestureRecognizer(sortPopupGesture)
        
        
        
        
        
    }
    
    
    @objc
    func sortViewClick()
    {
        
        viewWillLayoutSubviews()
        popupTableView.isHidden = false
        
    }
    
    @objc
    func sortByTotal(){
        
        sortIndex = 0
        manageData(oResponse: officeResponse, selOfficeIndex: selectOfficeIndex, srtIndex: sortIndex)
        let colorTotalImage = statsList[0].color!
        let iconTotalImage =  statsList[0].tagName!
        let totalImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: colorTotalImage))
        sortByImage1.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
        sortByImage2.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
        
    }
    
    @objc
    func sortByAppSet(){
        
        sortIndex = 1
        manageData(oResponse: officeResponse, selOfficeIndex: selectOfficeIndex, srtIndex: sortIndex)
        let colorTotalImage = statsList[1].color!
        let iconTotalImage =  statsList[1].tagName!
        let totalImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: colorTotalImage))
        sortByImage1.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
        sortByImage2.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
    }
    
    @objc
    func sortByAppCom(){
        
        sortIndex = 2
        manageData(oResponse: officeResponse, selOfficeIndex: selectOfficeIndex, srtIndex: sortIndex)
        let colorTotalImage = statsList[2].color!
        let iconTotalImage =  statsList[2].tagName!
        let totalImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: colorTotalImage))
        sortByImage1.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
        sortByImage2.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
        
    }
    
    @objc
    func sortBySale(){
        
        sortIndex = 3
        manageData(oResponse: officeResponse, selOfficeIndex: selectOfficeIndex, srtIndex: sortIndex)
        let colorTotalImage = statsList[3].color!
        let iconTotalImage =  statsList[3].tagName!
        let totalImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: colorTotalImage))
        sortByImage1.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
        sortByImage2.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
    }
    
    @objc
    func dismissPopup()
    {
        self.popupTableView.isHidden = true
        self.popupBackgroundView.isHidden = true
    }
    
    func setUpImages()
    {
        if statsList.count > 3
        {
            let colorTotalImage = statsList[0].color!
            let iconTotalImage =  statsList[0].tagName!
            let totalImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: colorTotalImage))
            totalImage1.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
            totalImage2.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
            sortByImage2.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
            sortByImage1.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
            
        }
        
        if statsList.count > 3
        {
            let colorAppSetImage = statsList[1].color!
            let iconAppSetImage =  statsList[1].tagName!
            let appSetImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: colorAppSetImage))
            appSetImage1.image = Utils.textToImage(drawText: iconAppSetImage as NSString, inImage: appSetImage)
            appSetImage2.image = Utils.textToImage(drawText: iconAppSetImage as NSString, inImage: appSetImage)
        }
        
        if statsList.count > 3
        {
            let colorappComImage = statsList[2].color!
            let iconAppComImage =  statsList[2].tagName!
            let appComImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: colorappComImage))
            appCompleteImage1.image = Utils.textToImage(drawText: iconAppComImage as NSString, inImage: appComImage)
            appComImage2.image = Utils.textToImage(drawText: iconAppComImage as NSString, inImage: appComImage)
        }
        
        if statsList.count > 3
        {
            let colorSaleImage = statsList[3].color!
            let iconSaleImage =  statsList[3].tagName!
            let saleImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: colorSaleImage))
            saleImage1.image = Utils.textToImage(drawText: iconSaleImage as NSString, inImage: saleImage)
            saleImage2.image = Utils.textToImage(drawText: iconSaleImage as NSString, inImage: saleImage)
        }
        
    }
    
    @objc
    func officeTabClick()
    {
        officeBottomView.backgroundColor = UIColor(named: "textColor")
        officeLbl.textColor = UIColor(named: "textColor")
        
        repsBtnView.backgroundColor = UIColor.systemGray5
        repsLbl.textColor = UIColor.systemGray3
        
        officesView.isHidden = false
        repsTabView.isHidden = true
    }
    
    @objc
    func repsTabClick()
    {
        repsBtnView.backgroundColor = UIColor(named: "textColor")
        repsLbl.textColor = UIColor(named: "textColor")
        
        officeBottomView.backgroundColor = UIColor.systemGray5
        officeLbl.textColor = UIColor.systemGray3
        
        officesView.isHidden = true
        repsTabView.isHidden = false
    }
    
    
    
    override func willMove(toParent parent: UIViewController?) {
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    func getConverstation(startDate: String, endDate :String)
    {
        present(alert!, animated: false, completion: nil)
        let parameters =  [
            "start": startDate,
            "end": endDate
            ] as [String : Any]
        
        let header = [
            "Accept": "text/json",
            "Authorization": "bearer \(SessionUtils.getToken())"
        ]
        
        let resourceString = "\(Utils.requestURL)api/stats";
        
        Alamofire.request(resourceString, method: .get, parameters: parameters, headers: header).responseJSON(completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let httpURLResponse = response.response{
                    let response_code = httpURLResponse.statusCode;
                    if response_code == 200{
                        do{
                            self.mainView.isHidden = false
                            self.alert?.dismiss(animated: false, completion: nil)
                            let json = JSON(value)
                            let str = String(describing: json);
                            let jsonData = str.data(using: .utf8)
                            let decoder = JSONDecoder();
                            let res = try decoder.decode([OfficesDataResponse].self, from: jsonData!)
                            self.officeResponse = res
                            if Constants.LOGGING_ENABLED{
                                print("Size of LeaderBoard Offices - \(self.officeResponse.count)")}
                            self.manageData(oResponse: self.officeResponse, selOfficeIndex: self.selectOfficeIndex, srtIndex: self.sortIndex)
                            
                            
                        }catch{
                            if Constants.LOGGING_ENABLED{
                                print(error)}}
                        
                        
                    }
                    else if response_code == 422 {
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "Given Data is Invalid", controller: self)
                    }
                    else{
                        if Constants.LOGGING_ENABLED{
                            print (response_code)}
                        
                        self.alert?.dismiss(animated: false, completion: nil)
                        Toast.show(message: "No Internet Connection / Server Error", controller: self)
                        
                    }
                }
            case .failure(let error):
                self.alert?.dismiss(animated: false, completion: nil)
                Toast.show(message: "\(error)", controller: self)
            }
            
        })
    }
    
    func manageData(oResponse: [OfficesDataResponse], selOfficeIndex: Int, srtIndex: Int)
    {
        if Constants.LOGGING_ENABLED{
        print("Indexx - \(selOfficeIndex)")
            print("Response is - \(oResponse)")}
        var statsTotalCount = 0
        var statsAppSetCount = 0
        var statsSalesCount = 0
        var statsAppCompleteCount = 0
        var appSetColor = ""
        var appSetName = ""
        var appSetTag = ""
        
        var saleColor = ""
        var saleTag = ""
        var saleName = ""
        
        var appComName = ""
        var appComTag = ""
        var appComColor = "";
        
        officeStats = []
        statsList = []
        
        if(oResponse.count > 0){
        //SetUp Stats Collection View
        for i in (0..<oResponse[selOfficeIndex].users!.count)
        {
            for j in (0..<oResponse[selOfficeIndex].users![i].interactions!.count)
            {
                let interaction = oResponse[selOfficeIndex].users![i].interactions![j]
                if(interaction.id == 66)
                {
                    statsAppSetCount = statsAppSetCount + interaction.log_count!
                    appSetTag = interaction.icon!
                    appSetName = interaction.name!
                    appSetColor = interaction.color!
                    
                }else if(interaction.id == 67)
                {
                    statsAppCompleteCount = statsAppCompleteCount + interaction.log_count!
                    appComTag = interaction.icon!
                    appComName = interaction.name!
                    appComColor = interaction.color!
                    
                }else if(interaction.id == 51)
                {
                    statsSalesCount = statsSalesCount + interaction.log_count!
                    saleTag = interaction.icon!
                    saleName = interaction.name!
                    saleColor = interaction.color!
                }
            }
            statsTotalCount = statsSalesCount + statsAppCompleteCount + statsAppSetCount;
            
        }
        
        statsList.append(Stats(name: "Total", count: "\(statsTotalCount)", color: "#FF5733", tagName: "T"))
        statsList.append(Stats(name: appSetName, count: "\(statsAppSetCount)", color: appSetColor, tagName: appSetTag))
        statsList.append(Stats(name: appComName, count: "\(statsAppCompleteCount)",color: appComColor, tagName: appComTag))
        statsList.append(Stats(name: saleName, count: "\(statsSalesCount)", color: saleColor, tagName: saleTag))
        
        
        //SetUp Offices Stats
        for z in (0..<oResponse.count)
        {
            var totalCount = 0
            var officeName = ""
            var appSetCount = 0
            var appComCount = 0
            var saleCount = 0
            
            for i in (0..<oResponse[z].users!.count)
            {
                officeName = oResponse[z].name!
                for j in (0..<oResponse[selOfficeIndex].users![i].interactions!.count)
                {
                    let interaction = oResponse[selOfficeIndex].users![i].interactions![j]
                    if(interaction.id == 66)
                    {
                        appSetCount = appSetCount + interaction.log_count!
                        
                    }else if(interaction.id == 67)
                    {
                        appComCount = appComCount + interaction.log_count!
                        
                    }else if(interaction.id == 51)
                    {
                        saleCount = saleCount + interaction.log_count!
                    }
                }
                totalCount = saleCount + appComCount + appSetCount;
            }
            
            officeStats.append(OfficeStats(itemNumber: z+1, employeeName: officeName, totalCount: totalCount, appSetCount: appSetCount, appCompleteCount: appComCount, saleCount: saleCount))
        }
        
        var sortedArray: [OfficeStats] = []
        if(sortIndex == 0)
        {
            sortedArray = officeStats.sorted(by: {$0.totalCount! > $1.totalCount!})
        }
        else if(sortIndex == 1)
        {
            sortedArray = officeStats.sorted(by: {$0.appSetCount! > $1.appSetCount!})
        }
        else if(sortIndex == 2)
        {
            sortedArray = officeStats.sorted(by: {$0.appCompleteCount! > $1.appCompleteCount!})
        }
        else if(sortIndex == 3)
        {
            sortedArray = officeStats.sorted(by: {$0.saleCount! > $1.saleCount!})
        }
        
        officeStats = sortedArray
        
        employeeStats = []
        //SetUp Employee Stats
        for z in (0..<oResponse.count)
        {
            
            
            for i in (0..<oResponse[z].users!.count)
            {
                var totalEmployeeCount = 0
                var employeeName = ""
                var appSetCount = 0
                var appComCount = 0
                var saleCount = 0
                for j in (0..<oResponse[selOfficeIndex].users![i].interactions!.count)
                {
                    employeeName = "\(oResponse[selOfficeIndex].users![i].first_name ?? "") \(oResponse[selOfficeIndex].users![i].last_name ?? "")"
                    let interaction = oResponse[selOfficeIndex].users![i].interactions![j]
                    if(interaction.id == 66)
                    {
                        appSetCount = appSetCount + interaction.log_count!
                        
                    }else if(interaction.id == 67)
                    {
                        appComCount = appComCount + interaction.log_count!
                        
                    }else if(interaction.id == 51)
                    {
                        saleCount = saleCount + interaction.log_count!
                    }
                }
                totalEmployeeCount = saleCount + appComCount + appSetCount;
                employeeStats.append(EmployeeStats(itemNumber: i+1, employeeName: employeeName, totalCount: totalEmployeeCount, appSetCount: appSetCount, appCompleteCount: appComCount, saleCount: saleCount))
            }
            
            
        }
        
        var sortedArrayEmployees: [EmployeeStats] = []
        if(sortIndex == 0)
        {
            sortedArrayEmployees = employeeStats.sorted(by: {$0.totalCount! > $1.totalCount!})
        }
        else if(sortIndex == 1)
        {
            sortedArrayEmployees = employeeStats.sorted(by: {$0.appSetCount! > $1.appSetCount!})
        }
        else if(sortIndex == 2)
        {
            sortedArrayEmployees = employeeStats.sorted(by: {$0.appCompleteCount! > $1.appCompleteCount!})
        }
        else if(sortIndex == 3)
        {
            sortedArrayEmployees = employeeStats.sorted(by: {$0.saleCount! > $1.saleCount!})
        }
        
        employeeStats = sortedArrayEmployees
        
        }
        
        
        setUpImages()
        statsCollectionView.reloadData()
        officeStatsTableView.reloadData()
        employeeStatsTableView.reloadData()
    }
    
    /* @objc
     func imageClick(tapGesture: UITapGestureRecognizer)
     {
     let imageView = tapGesture.view!
     let index = imageView.tag
     
     for i in (0..<dateFilters.filters!.count)
     {
     let indexPath = IndexPath(row: i, section: 0)
     print("Index PAth is - \(indexPath)")
     let cell = self.dateFilterCollectionView.cellForItem(at: indexPath) as! LeaderBoardDateFilterCell
     
     if(indexPath.row == index)
     {
     cell.dateFilterImage.backgroundColor = UIColor(named: "mainBlue")
     cell.dateFilterLabel.textColor = UIColor.white
     }else
     {
     cell.dateFilterImage.backgroundColor = UIColor.systemGray5
     cell.dateFilterLabel.textColor = UIColor.black
     }
     
     }
     
     
     }*/
    
    
    
    
    
}

extension LeaderBoardViewController:UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == dateFilterCollectionView)
        {
            return (dateFilters.filters?.count)!
        }else
        {
            return statsList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == dateFilterCollectionView)
        {
            let cell = dateFilterCollectionView.dequeueReusableCell(withReuseIdentifier: "LeaderBoardDateFilterCell", for: indexPath) as! LeaderBoardDateFilterCell
            
            if(indexPath.row == selectDateFilterIndex)
            {
                cell.dateFilterImage.backgroundColor = UIColor(named: "blueDarkBlue")
                cell.dateFilterLabel.textColor = UIColor.white
            }else
            {
                cell.dateFilterImage.backgroundColor = UIColor.systemGray5
                cell.dateFilterLabel.textColor = UIColor(named: "textcolor3")
            }
            cell.dateFilterImage.layer.cornerRadius = 13
            cell.dateFilterLabel
                .text = dateFilters.filters![indexPath.row].name
            
            
            /*cell.dateFilterImage.tag = indexPath.row
             cell.dateFilterImage.isUserInteractionEnabled = true
             let imageGesture = UITapGestureRecognizer(target: self, action: #selector(imageClick(tapGesture:)))
             imageGesture.numberOfTapsRequired = 1
             cell.dateFilterImage.addGestureRecognizer(imageGesture)*/
            return cell;
            
        }else
        {
            let cell = statsCollectionView.dequeueReusableCell(withReuseIdentifier: "LeaderBoardStatCell", for: indexPath) as! LeaderBoardStatCell
            
            
            let colorTotalImage = statsList[indexPath.row].color!
            let iconTotalImage =  statsList[indexPath.row].tagName!
            let totalImage = Utils.customImage(diameter: 28, color: Utils.hexStringToUIColor(hex: colorTotalImage))
            cell.interactionImage.image = Utils.textToImage(drawText: iconTotalImage as NSString, inImage: totalImage)
            cell.logCountLabel.text = statsList[indexPath.row].count!
            cell.nameLabel.text = statsList[indexPath.row].name!
            
            return cell
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == dateFilterCollectionView
        {
            selectDateFilterIndex = indexPath.row
            let range = Range(uncheckedBounds: (0, collectionView.numberOfSections))
            let indexSet = IndexSet(integersIn: range)
            self.dateFilterCollectionView.reloadSections(indexSet)
            if(indexPath.row == 0)
            {
                let startDate = Today.getStartDate()
                let endDate = Today.getEndDate()
                if Constants.LOGGING_ENABLED{
                print("Start Date - \(startDate)")
                    print("End Date - \(endDate)")}
                getConverstation(startDate: startDate, endDate: endDate)
            }
            else if(indexPath.row == 1)
            {
                
                let startDate = Yesterday.getStartDate()
                let endDate = Yesterday.getEndDate()
                if Constants.LOGGING_ENABLED{
                print("Start Date - \(startDate)")
                    print("End Date - \(endDate)")}
                getConverstation(startDate: startDate, endDate: endDate)
                
            }
                
            else if(indexPath.row == 2)
            {
                
                let startDate = ThisWeek.getStartDate()
                let endDate = ThisWeek.getEndDate()
                if Constants.LOGGING_ENABLED{
                print("Start Date - \(startDate)")
                print("End Date - \(endDate)")
                }
                getConverstation(startDate: startDate, endDate: endDate)
                
            }
                
            else if(indexPath.row == 3)
            {
                
                let startDate = LastWeek.getStartDate()
                let endDate = LastWeek.getEndDate()
                if Constants.LOGGING_ENABLED{
                print("Start Date - \(startDate)")
                    print("End Date - \(endDate)")}
                getConverstation(startDate: startDate, endDate: endDate)
                
            }
                
            else if(indexPath.row == 4)
            {
                
                let startDate = ThisMonth.getStartDate()
                let endDate = ThisMonth.getEndDate()
                if Constants.LOGGING_ENABLED{
                print("Start Date - \(startDate)")
                    print("End Date - \(endDate)")}
                getConverstation(startDate: startDate, endDate: endDate)
                
            }
                
            else if(indexPath.row == 5)
            {
                
                let startDate = LastMonth.getStartDate()
                let endDate = LastMonth.getEndDate()
                if Constants.LOGGING_ENABLED{
                print("Start Date - \(startDate)")
                    print("End Date - \(endDate)")}
                getConverstation(startDate: startDate, endDate: endDate)
                
            }
            
            else if(indexPath.row == 6)
            {
                
                let startDate = ThisYear.getStartDate()
                let endDate = ThisYear.getEndDate()
                if Constants.LOGGING_ENABLED{
                print("Start Date - \(startDate)")
                    print("End Date - \(endDate)")}
                getConverstation(startDate: startDate, endDate: endDate)
                
            }
            
            else if(indexPath.row == 7)
            {
                
                let startDate = LastYear.getStartDate()
                let endDate = LastYear.getEndDate()
                if Constants.LOGGING_ENABLED{
                print("Start Date - \(startDate)")
                    print("End Date - \(endDate)")}
                getConverstation(startDate: startDate, endDate: endDate)
                
            }
        }
        
        
        
    }
}

extension LeaderBoardViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == officeStatsTableView)
        {
            return officeStats.count
        }else
        {
            return employeeStats.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == officeStatsTableView){
            let cell = officeStatsTableView.dequeueReusableCell(withIdentifier: "officecell", for: indexPath) as! LeaderBoardOfficeCell
            cell.totalLabel.backgroundColor = UIColor.secondarySystemBackground
            cell.appComLabel.backgroundColor = UIColor.secondarySystemBackground
            cell.appSetLabel.backgroundColor = UIColor.secondarySystemBackground
            cell.saleLabel.backgroundColor = UIColor.secondarySystemBackground
            if(sortIndex == 0)
            {
                cell.totalLabel.backgroundColor = UIColor.systemGray5
            }else if(sortIndex == 1)
            {
                cell.appSetLabel.backgroundColor = UIColor.systemGray5
            }else if(sortIndex == 2)
            {
                cell.appComLabel.backgroundColor = UIColor.systemGray5
            }else if(sortIndex == 3)
            {
                cell.saleLabel.backgroundColor = UIColor.systemGray5
            }
            
            cell.numberLabel.text = "\(indexPath.row + 1)"
            cell.officeNameLabel.text = officeStats[indexPath.row].employeeName!
            cell.totalLabel.text = "\(String(describing: officeStats[indexPath.row].totalCount!))"
            cell.appSetLabel.text = "\(String(describing: officeStats[indexPath.row].appSetCount!))"
            cell.appComLabel.text = "\(String(describing: officeStats[indexPath.row].appCompleteCount!))"
            cell.saleLabel.text = "\(String(describing: officeStats[indexPath.row].saleCount!))"
            return cell
        }else{
            let cell = employeeStatsTableView.dequeueReusableCell(withIdentifier: "employeecell", for: indexPath) as! EmployeeStatsCell
            
            cell.totalLabel.backgroundColor = UIColor.secondarySystemBackground
            cell.appComLabel.backgroundColor = UIColor.secondarySystemBackground
            cell.appSetLabel.backgroundColor = UIColor.secondarySystemBackground
            cell.saleLabel.backgroundColor = UIColor.secondarySystemBackground
            if(sortIndex == 0)
            {
                cell.totalLabel.backgroundColor = UIColor.systemGray5
            }else if(sortIndex == 1)
            {
                cell.appSetLabel.backgroundColor = UIColor.systemGray5
            }else if(sortIndex == 2)
            {
                cell.appComLabel.backgroundColor = UIColor.systemGray5
            }else if(sortIndex == 3)
            {
                cell.saleLabel.backgroundColor = UIColor.systemGray5
            }
            
            cell.numberLabel.text = "\(indexPath.row + 1)"
            cell.employeeNameLabel.text = employeeStats[indexPath.row].employeeName!
            cell.totalLabel.text = "\(String(describing: employeeStats[indexPath.row].totalCount!))"
            cell.appSetLabel.text = "\(String(describing: employeeStats[indexPath.row].appSetCount!))"
            cell.appComLabel.text = "\(String(describing: employeeStats[indexPath.row].appCompleteCount!))"
            cell.saleLabel.text = "\(String(describing: employeeStats[indexPath.row].saleCount!))"
            
            return cell
        }
        
    }
    
    
    
}

class LeaderBoardDateFilterCell: UICollectionViewCell
{
    
    @IBOutlet weak var dateFilterImage: UIView!
    
    
    
    @IBOutlet weak var dateFilterLabel: UILabel!
}

class LeaderBoardStatCell: UICollectionViewCell
{
    
    @IBOutlet weak var logCountLabel: UILabel!
    @IBOutlet weak var interactionImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
}

class LeaderBoardOfficeCell: UITableViewCell{
    
    @IBOutlet weak var officeNameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var saleLabel: UILabel!
    @IBOutlet weak var appComLabel: UILabel!
    @IBOutlet weak var appSetLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
}

class PopupCell: UITableViewCell
{
    
    @IBOutlet weak var label: UILabel!
}

class EmployeeStatsCell: UITableViewCell
{
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var appSetLabel: UILabel!
    @IBOutlet weak var appComLabel: UILabel!
    @IBOutlet weak var saleLabel: UILabel!
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
}
