//
//  File.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 18/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveKit

class UnderlinedTextField: UITextField {

    @IBOutlet weak var underlineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        underlineView.backgroundColor = UIColor.lightGray
        
        // Highlight/unhighlight the underlined view when it's being edited.
        reactive.controlEvents(.editingDidBegin).map { _ in
            return UIColor.green
        }.bind(to: underlineView.reactive.backgroundColor).dispose(in: reactive.bag)
        
        reactive.controlEvents(.editingDidEnd).map { _ in
            return UIColor.lightGray
        }.bind(to: underlineView.reactive.backgroundColor).dispose(in: reactive.bag)
    }
}
