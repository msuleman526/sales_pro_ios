///
//  Delete.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 13/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class DeleteMarker: GMSMarker {
    
    var drawPolygon:GMSPolygon
    var markers:[GMSMarker]?
    
    init(location:CLLocationCoordinate2D,polygon:GMSPolygon) {
        
        
        self.drawPolygon = polygon
        super.init()
        super.position = location
        
    }
    
}
