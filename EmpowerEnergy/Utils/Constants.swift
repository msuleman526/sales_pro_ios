//
//  Contants.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 19/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class Constants {
    
    public static var SESSION_EMAIL:String = "SESSION_EMAIL"
    public static var SESSION_NAME:String = "SESSION_NAME"
    public static var SESSION_TOKEN:String = "SESSION_TOKEN"
    public static var SESSION_ID:String = "SESSION_ID"
    public static var MAP_TYPE:String = "MAP_TYPE"
    public static var SESSION_TYPE:String = "SESSION_TYPE"
    public static var LOGGING_ENABLED:Bool = true
}
