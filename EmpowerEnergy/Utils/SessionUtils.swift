//
//  SessionUtils.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 19/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class SessionUtils{
    
    static func setLoginData(userResponse: UsersResponse)
    {
        let preferences = UserDefaults.standard;
        preferences.set(userResponse.email, forKey: Constants.SESSION_EMAIL)
        preferences.set(userResponse.id, forKey: Constants.SESSION_ID)
        preferences.set(userResponse.api_token, forKey: Constants.SESSION_TOKEN)
        preferences.set(userResponse.name, forKey: Constants.SESSION_NAME)
        preferences.set(userResponse.role!.name!, forKey: Constants.SESSION_TYPE)
        
        var offices: [OfficeModel] = []
        for i in (0..<userResponse.companies!.count)
        {
            let created = userResponse.companies![i].created ?? Date()
            let modified = userResponse.companies![i].modified ?? Date()
            offices.append(OfficeModel(creeated: created, id: userResponse.companies![i].id!, modified: modified, name: userResponse.companies![i].name!, index: userResponse.companies![i].index ?? 0))
        }
        do{
            let jsonData = try JSONEncoder().encode(offices)
            let json  = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
            preferences.set(json!, forKey: "offices")
        }catch{print(error)}
        preferences.synchronize()
        
    }
    
    static func setCurrentLatLong(latitude: Double, longitude: Double,zoom: Double)
    {
        let preferences = UserDefaults.standard;
        preferences.set(latitude, forKey: "latitude")
        preferences.set(longitude, forKey: "longitude")
        preferences.set(zoom, forKey: "zoom")
        preferences.synchronize()
    }
    
    static func setMapType(value: Int)
    {
        let preferences = UserDefaults.standard;
        preferences.set(value, forKey: Constants.MAP_TYPE)
        preferences.synchronize()
    }
    
    static func logout()
    {
        let preferences = UserDefaults.standard;
        preferences.set("", forKey: Constants.SESSION_EMAIL)
        preferences.set(0, forKey: Constants.SESSION_ID)
        preferences.set("", forKey: Constants.SESSION_TOKEN)
        preferences.set("", forKey: Constants.SESSION_NAME)
        preferences.set("", forKey: Constants.SESSION_TYPE)
        preferences.set(0.0, forKey: "latitude")
        preferences.set(0.0, forKey: "longitude")
        preferences.set(0.0, forKey: "zoom")
        preferences.synchronize()
        
    }
    
    static func getEmail() -> String{
        let preferences = UserDefaults.standard;
        if(preferences.object(forKey: Constants.SESSION_EMAIL)) == nil{
            return "";
        }else
        {
            return preferences.object(forKey: Constants.SESSION_EMAIL) as! String;
        }
    }
    
    static func getOffices() -> [Office]{
        let preferences = UserDefaults.standard;
        if(preferences.object(forKey: "offices")) == nil{
            return [];
        }else
        {
            var offices: [Office] = []
            do{
                print("Hello")
                let object = preferences.object(forKey: "offices") as! String;
                let jsonData = object.data(using: .utf8)
                let decoder = JSONDecoder();
                let res = try decoder.decode([OfficeModel].self, from: jsonData!)
                for i in (0..<res.count)
                {
                    offices.append(Office(creeated: res[i].created, id: res[i].id!, modified: res[i].modified!, name: res[i].name!, index: res[i].index!))
                }
                print("Hello")
            
            }catch{print(error)}
             return offices
        }
    }
    
    static func getLatitude() -> Double{
        let preferences = UserDefaults.standard;
        if(preferences.object(forKey: "latitude")) == nil{
            return 0.0;
        }else
        {
            return preferences.object(forKey: "latitude") as! Double;
        }
    }
    
    static func getLongitude() -> Double{
        let preferences = UserDefaults.standard;
        if(preferences.object(forKey: "longitude")) == nil{
            return 0.0;
        }else
        {
            return preferences.object(forKey: "longitude") as! Double;
        }
    }
    
    static func getZoom() -> Double{
        let preferences = UserDefaults.standard;
        if(preferences.object(forKey: "zoom")) == nil{
            return 0.0;
        }else
        {
            return preferences.object(forKey: "zoom") as! Double;
        }
    }
    
    
    static func getUserType() -> String{
        let preferences = UserDefaults.standard;
        if(preferences.object(forKey: Constants.SESSION_TYPE)) == nil{
            return "";
        }else
        {
            return preferences.object(forKey: Constants.SESSION_TYPE) as! String;
        }
    }
    
    
    
    
    static func getMapType() -> Int{
        let preferences = UserDefaults.standard;
        if preferences.object(forKey: Constants.MAP_TYPE) == nil{
            return 4;
        }else
        {
            return preferences.object(forKey: Constants.MAP_TYPE) as! Int;
        }
    }
    
    static func getToken() -> String{
        let preferences = UserDefaults.standard;
        
        if preferences.object(forKey: Constants.SESSION_TOKEN) == nil || preferences.object(forKey: Constants.SESSION_TOKEN) as! String == "" {
            return "kjsqsj";
        }else
        {
            return preferences.object(forKey: Constants.SESSION_TOKEN) as! String;
        }
    }
    
    static func getName() -> String{
        let preferences = UserDefaults.standard;
        if(preferences.object(forKey: Constants.SESSION_NAME)) == nil{
            return "";
        }else
        {
            return preferences.object(forKey: Constants.SESSION_NAME) as! String;
        }
    }
    
    static func getUserId() -> Int{
        let preferences = UserDefaults.standard;
        if(preferences.object(forKey: Constants.SESSION_ID)) == nil{
            return 0;
        }else
        {
            return preferences.object(forKey: Constants.SESSION_ID) as! Int;
        }
    }
    
    private static func archivePeople(people : [Office]) -> NSData {
        var data = NSData()
        do{
            data =  try NSKeyedArchiver.archivedData(withRootObject: people as NSArray, requiringSecureCoding: true) as NSData
        }catch(let error){
            print(error)
        }
        return data
    }
    
}
