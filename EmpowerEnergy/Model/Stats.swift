//
//  Stats.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 24/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation


class Stats
{
    var name: String?
    var count: String?
    var color: String?
    var tagName: String?
    
    init(name: String, count: String, color: String, tagName: String)
    {
        self.name = name;
        self.count = count;
        self.color = color
        self.tagName = tagName
    }
}
