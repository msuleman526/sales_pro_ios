//
//  Area.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 02/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class Area: Decodable
{
    var color: String?
    var coordinates: [Location]?
    var created: Date?
    var id: CLong?
    var modified: Date?
    var name: String?
    var employees: [Employee]?
    var total_addresses: Int?
    
    init(id: Int,name: String,color: String,created: Date,modified: Date,coordinates: [Location],employees: [Employee])
    {
        self.id = id
        self.name = name
        self.color = color
        self.created = created
        self.modified = modified
        self.coordinates = coordinates
        self.employees = employees
       
    }
    
    init(id: Int,name: String,color: String,created: Date,modified: Date,coordinates: [Location],employees: [Employee],total_addresses: Int)
    {
        self.id = id
        self.name = name
        self.color = color
        self.created = created
        self.modified = modified
        self.coordinates = coordinates
        self.employees = employees
        self.total_addresses = total_addresses
    }
    
    init(id: Int,name: String,color: String,created: Date,modified: Date,coordinates: [Location])
    {
        self.id = id
        self.name = name
        self.color = color
        self.created = created
        self.modified = modified
        self.coordinates = coordinates
    }
}
