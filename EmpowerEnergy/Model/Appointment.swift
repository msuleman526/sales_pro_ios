//
//  Appointment.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 21/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class Appointment: Decodable
{
    var active: Bool?
    var created: Date?
    var customer: Customer?
    var end: Date?
    var held: Bool?
    var id: CLong?
    var location: String?
    var modified: Date?
    var start: Date?
    var title: String?
    var content: String?
    var uuid: String?
    var appointment_at: Date?
    var user: UsersResponse
    var address: InteractionAddressData?
    
    init(created: Date,end: Date,held: Bool,id: CLong, modified: Date, start: Date, title: String, content: String, user: UsersResponse, address: InteractionAddressData) {
        self.created = created
        self.end = end
        self.id = id
        self.held = held
        self.modified = modified
        self.start = start
        self.title = title
        self.content = content
        self.user = user
        self.address = address
    }
    
    init(created: Date,end: Date,held: Bool,id: CLong, modified: Date, start: Date, title: String, content: String, user: UsersResponse, address: InteractionAddressData, active: Bool) {
        self.created = created
        self.end = end
        self.id = id
        self.held = held
        self.modified = modified
        self.start = start
        self.title = title
        self.content = content
        self.user = user
        self.address = address
        self.active = active
    }
}
