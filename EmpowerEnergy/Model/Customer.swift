//
//  Customer.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class Customer: Decodable
{
    var birthday: String?;
    var birthdayDate: Date?
    var dataFields: [CustomerDataField]?
    var demographic: Demographic?
    var firstName: String?;
    var gender: String?;
    var genderStr: String?;
    var homePhone: String?
    var id: Int?
    var lastName: String?
    var mobilePhone: String?
    var personalEmail: String?
    var resident: Bool?
    var workEmail: String?
    var income: String?
    var workPhone: String?
    var address_id: Int?
    var net_worth: String?
    var age: String?
    var credit_rating: String?
    var addressData: InteractionAddressData?
    
    init() {
        
    }
    
    init(id:Int,firstName:String,lastName:String,mobilePhone: String,homePhone:String,workPhone:String,personalEmail:String,workEmail:String,resident:Bool,genderStr:String,birthday:String,demographic:Demographic,list: [CustomerDataField],address_id:Int,addressData:InteractionAddressData) {
        
        self.id = id;
        self.firstName = firstName
        self.lastName = lastName
        self.mobilePhone = mobilePhone
        self.homePhone = homePhone
        self.workPhone = workPhone
        self.personalEmail = personalEmail
        self.workEmail = workEmail
        self.resident = resident
        self.genderStr = genderStr
        self.birthday = birthday
        self.demographic = demographic
        self.dataFields = list
        self.address_id = address_id
        self.addressData = addressData
        
        
    }
    
    init(id:Int,address_id:Int) {
           
           self.id = id;
           self.address_id = address_id
           
           
       }
    
    
    init(id:Int,firstName:String,lastName:String,mobilePhone: String,homePhone:String,workPhone:String,personalEmail:String,workEmail:String,resident:Bool,genderStr:String,birthday:String,address_id:Int,addressData:InteractionAddressData) {
        
        self.id = id;
        self.firstName = firstName
        self.lastName = lastName
        self.mobilePhone = mobilePhone
        self.homePhone = homePhone
        self.workPhone = workPhone
        self.personalEmail = personalEmail
        self.workEmail = workEmail
        self.resident = resident
        self.genderStr = genderStr
        self.birthday = birthday
        self.address_id = address_id
        self.addressData = addressData
        
        
    }
    
    init(id:Int,firstName:String,lastName:String,mobilePhone: String,homePhone:String,workPhone:String,personalEmail:String,workEmail:String,resident:Bool,genderStr:String,birthday:String,demographic:Demographic,list: [CustomerDataField],address_id:Int) {
           
           self.id = id;
           self.firstName = firstName
           self.lastName = lastName
           self.mobilePhone = mobilePhone
           self.homePhone = homePhone
           self.workPhone = workPhone
           self.personalEmail = personalEmail
           self.workEmail = workEmail
           self.resident = resident
           self.genderStr = genderStr
           self.birthday = birthday
           self.demographic = demographic
           self.dataFields = list
           self.address_id = address_id
           
           
       }
    
    init(id:Int,firstName:String,lastName:String,mobilePhone: String,personalEmail:String,resident:Bool,age: String, net_worth: String,credit_rating: String,genderStr:String,income:String,address_id:Int) {
        
        self.id = id;
        self.firstName = firstName
        self.lastName = lastName
        self.mobilePhone = mobilePhone
        self.personalEmail = personalEmail
        self.income = income
        self.resident = resident
        self.genderStr = genderStr
        self.address_id = address_id
        self.age = age;
        self.net_worth = net_worth
        self.credit_rating = credit_rating
        
        
    }
    
}
