//
//  CustomerData.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class CustomerData: Decodable
{
    var id: Int?
    var address_id: Int?
    var first_name: String?
    var last_name: String?
    var birth_day: String?
    var gender: String?
    var home_phone: String?
    var mobile_phone: String?
    var personal_email: String?
    var work_email: String?
    var resident: Bool?
    var net_income: Int?
    var net_worth: Int?
    var credit_rating: Int?
}
