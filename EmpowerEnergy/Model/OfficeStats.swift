//
//  OfficeStats.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 24/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation


class OfficeStats
{
    var itemNumber: Int?
    var employeeName: String?
    var totalCount: Int?
    var appSetCount: Int?
    var appCompleteCount: Int?
    var saleCount: Int?
    
    init(itemNumber: Int, employeeName: String,totalCount: Int, appSetCount: Int, appCompleteCount: Int,saleCount: Int)
    {
        self.itemNumber = itemNumber
        self.employeeName = employeeName
        self.totalCount = totalCount
        self.appCompleteCount = appCompleteCount
        self.appSetCount = appSetCount
        self.saleCount = saleCount
    }
}
