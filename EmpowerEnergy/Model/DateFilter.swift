//
//  DateFilter.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 12/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class DateFilter
{
    public var TODAY:DateFilter?;
    public var filters: [DateFilter]?
    
    public var name: String?
    
    init(name: String) {
        self.name = name;
    }
    
    init() {
        
    }
    
    
    
}

class Today: DateFilter
{
    
    override init() {
        super.init(name: "Today")
    }
    
    static func getEndDate() -> String
    {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
    
    static func getStartDate() -> String
    {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
        
    }
}

class Yesterday: DateFilter
{
    
    override init() {
        super.init(name: "Yesterday")
    }
    
    static func getEndDate() -> String
    {
        let date = Date().dayBefore
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
    
    static func getStartDate() -> String
    {
        
        let date = Date().dayBefore
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
        
    }
}

class ThisWeek: DateFilter
{
    
    override init() {
        super.init(name: "This Week")
    }
    
    static func getEndDate() -> String
    {
        let date = Date().endOfWeek
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date!)
        return result
    }
    
    static func getStartDate() -> String
    {
        
        let date = Date().startOfWeek
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date!)
        return result
        
    }
}

class LastWeek: DateFilter
{
    
    override init() {
        super.init(name: "Last Week")
    }
    
    static func getEndDate() -> String
    {
        let date = Date().endOfWeekLast
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date!)
        return result
    }
    
    static func getStartDate() -> String
    {
        
        let date = Date().startOfWeekLast
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date!)
        return result
        
    }
}

class ThisMonth: DateFilter
{
    
    override init() {
        super.init(name: "This Month")
    }
    
    static func getEndDate() -> String
    {
        let date = Date().endOfMonth
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
    
    static func getStartDate() -> String
    {
        
        let date = Date().startOfMonth
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
        
    }
}

class LastMonth: DateFilter
{
    
    override init() {
        super.init(name: "Last Month")
    }
    
    static func getEndDate() -> String
    {
        let date = Date().endOfMonthLast
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
    
    static func getStartDate() -> String
    {
        
        let date = Date().startOfMonthLast
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
        
    }
}

class ThisYear: DateFilter
{
    
    override init() {
        super.init(name: "This Year")
    }
    
    static func getEndDate() -> String
    {
        let date = Date().endOfYear
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
    
    static func getStartDate() -> String
    {
        
        let date = Date().startOfYear
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
        
    }
}

class LastYear: DateFilter
{
    
    override init() {
        super.init(name: "Last Year")
    }
    
   static func getEndDate() -> String
    {
        let date = Date().endOfYearLast
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
    
    static func getStartDate() -> String
    {
        
        let date = Date().startOfYearLast
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
        
    }
}

class Custom: DateFilter
{
    
    override init() {
        super.init(name: "Custom")
    }
    
    func getEndDate() -> String
    {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
    }
    
    func getStartDate() -> String
    {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        return result
        
    }
    
}

extension Date {
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    
    var startOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 1, to: sunday)
    }
    
    var endOfWeek: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    var startOfWeekLast: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: -6, to: sunday)
    }
    
    var endOfWeekLast: Date? {
        let gregorian = Calendar(identifier: .gregorian)
        guard let sunday = gregorian.date(from: gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return gregorian.date(byAdding: .day, value: 0, to: sunday)
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var startOfMonth: Date {
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month], from: self)
        
        return  calendar.date(from: components)!
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfMonth)!
    }
    
    func isMonday() -> Bool {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.weekday], from: self)
        return components.weekday == 2
    }
    
    var endOfMonthLast: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfMonthLast)!
    }
    
    var startOfMonthLast: Date {
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month], from: self)
        let date = calendar.date(from: components)!
        return  calendar.date(byAdding: .month, value: -1, to: date)!
    }
    
    
    var startOfYear: Date {
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .year], from: self)
        let date = calendar.date(from: components)!
        return  calendar.date(byAdding: .month, value: 0, to: date)!
    }
    
    var startOfYearLast: Date {
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .year], from: self)
        let date = calendar.date(from: components)!
        return  calendar.date(byAdding: .month, value: -12, to: date)!
    }
    
    var endOfYear: Date {
           var components = DateComponents()
           components.year = 1
           components.second = -1
           return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfYear)!
       }
    
    var endOfYearLast: Date {
        var components = DateComponents()
        components.year = 1
        components.second = -1
        return Calendar(identifier: .gregorian).date(byAdding: components, to: startOfYearLast)!
    }
}
