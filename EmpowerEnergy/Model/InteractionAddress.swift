//
//  InteractionAddress.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class InteractionAddress: Decodable{
    
    var address: Address?
    var interaction: Interaction?
    var customers: [Customer]?
    
    init(address:Address,interaction: Interaction) {
        self.address = address
        self.interaction = interaction
    }
    
    
    init(address:Address,interaction: Interaction,customers: [Customer]) {
        self.address = address
        self.interaction = interaction
        self.customers = customers
    }
    
    
    init(address:Address) {
        self.address = address
        
    }
}
