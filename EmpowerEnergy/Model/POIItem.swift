//
//  POIItem.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 23/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation
import GoogleMapsUtils

class POIItem: NSObject, GMUClusterItem {
  var position: CLLocationCoordinate2D
  var name: String!

  init(position: CLLocationCoordinate2D, name: String) {
    self.position = position
    self.name = name
  }
}
