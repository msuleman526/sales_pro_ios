//
//  History.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 28/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class History: Decodable
{
    var id: CLong?
    static var ALL: Int = 0
    static var NOTES: Int = 1
    static var APPOINTMENTS: Int = 2
    static var STATUSES: Int = 3
    var note_id: Int?
    var note: String?
    var created: Date?
    var customer: Customer?
    var interaction: Interaction?
    var modified: Date?
    var user: UsersResponse?
    var content: String?
    var title: String?
    var appointment_at: Date?
    var appointment_end_at: Date?
    var snoozeTime: String?
    var description: String?
    var held: Bool?
    
    init(id: Int,customer: Customer,created: Date,modified: Date)
    {
        self.id = id
        self.customer = customer
        self.created = created
        self.modified = modified
        
    }
    
    init(id: Int,content: String,appointment_at: Date, appointment_end_at: Date,held: Bool,title: String,created: Date,updated: Date)
    {
        self.content = content
        self.appointment_at = appointment_at
        self.appointment_end_at = appointment_end_at
        self.held = held
        self.title = title
        self.created = created
        self.modified = updated
    }
    
    init(id: CLong, content: String, appointment_at: Date,appointment_end_at: Date,held: Bool,title: String,
        created: Date,updated: Date,user: UsersResponse)
    {
        self.id = id
        self.content = content
        self.appointment_end_at = appointment_end_at
        self.appointment_at = appointment_at
        self.held = held
        self.title = title
        self.created = created
        self.modified = updated
        self.user = user
    }
    
    init(id: CLong,interaction:Interaction,created: Date,modified: Date)
    {
        self.id = id
        self.interaction = interaction
        self.created = created
        self.modified = modified
    }
    
    init(id: CLong,interaction:Interaction,created: Date,modified: Date,user: UsersResponse)
       {
           self.id = id
           self.interaction = interaction
           self.created = created
           self.modified = modified
        self.user = user
       }
    
    init(id: CLong,note: String, user: UsersResponse, note_id: Int,created: Date,modified: Date)
    {
        self.id = id
        self.note = note
        self.note_id = note_id
        self.created = created
        self.modified = modified
    }
    
    init(id: CLong, note: String, user: UsersResponse,created: Date,modified:Date,note_id:Int)
    {
        self.id = id
        self.note = note
        self.note_id = note_id
        self.user = user
        self.created = created
        self.modified = modified
    }
    
}
