//
//  Employee.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 18/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class Employee: Decodable
{
    var created: Date?
    var email: String?
    var firstName: String?
    var lastName: String?
    var id: CLong?
    var type: String?
    var password: String?
    var location: Location?
    var modified: Date?
    var phone: String?
    var picture: String?
    var parent: Int = 0
    var offices: [Office]?
    
    init(id: CLong, firstName: String,lastName: String,email: String,created: Date, modified: Date, type: String,offices: [Office],parent: Int,phone: String)
    {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.created = created
        self.modified = modified
        self.parent = parent
        self.type = type
        self.offices = offices
        self.phone = phone
    }
}
