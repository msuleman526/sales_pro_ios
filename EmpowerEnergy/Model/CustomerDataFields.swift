//
//  CustomerDataFields.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class CustomerDataField : Decodable
{
    var id: CLong?;
    var name: String?
    var type: String?
    var value: String?
}
