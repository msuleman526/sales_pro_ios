//
//  2DLocation.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 23/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class DLocation: Encodable
{
    var latitude:Double?
    var longitude:Double?
    
    init(latitude: Double,longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
}
