//
//  InteractionAddressDatra.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class InteractionAddressData: Decodable
{
    var id: Int?
    var city_id: Int?
    var latitude: String?
    var longitude: String?
    var email: String?
    var first_name: String?
    var last_name: String?
    var gender: String?
    var income: String?
    var address: String?
    var credit_rating: String?
    var age: Int?
    var net_worth: String?
    var unit: String?
    var phone: String?
    var district: String?
    var region: String?
    var zipcode: String?
    var city: City?
    var current_interaction: Interaction?
    
    
}


class InteractionAddressDataForAddress: Decodable
{
    
    var id: Int?
      var city_id: Int?
      var latitude: String?
      var longitude: String?
      var email: String?
      var first_name: String?
      var last_name: String?
      var gender: String?
      var income: String?
      var address: String?
      var credit_rating: String?
      var age: Int?
      var net_worth: String?
      var unit: String?
      var phone: String?
      var district: String?
      var region: String?
      var zipcode: String?
      var city: City?
      var current_interaction: Interaction?
    
}


