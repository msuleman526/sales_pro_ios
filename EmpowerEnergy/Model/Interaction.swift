//
//  Interaction.swift
//  energysolutions
//
//  Created by Salman Sherin on 18/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class Interaction: Decodable
{
    
    var color:String?;
    var icon:String?;
    var id:CLong?;
    var name:String?;
    var ordinal:Int?;
    var parent:Int?;
    
    var log_count:Int?;
    var parentId:CLong?
    var children:[Interaction]?;
    var type:String?;
    var user:UsersResponse?;
    var created_at:String?;
    var updated_at:String?;
    
    init(id: CLong, type:String, name: String, icon: String, color: String, ordinal: Int) {
        self.id = id
        self.type = type
        self.name = name
        self.icon = icon
        self.color = color
        self.ordinal = ordinal
    }
    
    init(id: CLong, name: String, icon: String, color: String, children: [Interaction],parent: Interaction) {
        self.id = id
        self.name = name
        self.icon = icon
        self.color = color
        self.children = children
        
    }
    
    init(id: CLong, name: String, icon: String, color: String, children: [Interaction]) {
        self.id = id
        self.name = name
        self.icon = icon
        self.color = color
        self.children = children
    }
    
}
