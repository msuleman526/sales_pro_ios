//
//  Popup.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 24/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class Popup
{
    var index: Int?
    var name: String?
    
    init(index: Int, name: String)
    {
        self.index = index
        self.name = name
    }
}
