//
//  File.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class City: Decodable{
    
    var name:String?
    var state: State?
    
}

class State: Decodable
{
    var name:String?
    var country: Country?
}
