//
//  AreaClusterItem.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 14/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation
import GoogleMaps
import GoogleMapsUtils

class AreaClusterItem: NSObject, GMUClusterItem{
    var area: Area?
    var index: Int?;
    var position: CLLocationCoordinate2D
    var marker: UIImage
    
    init(area: Area, index: Int,position: CLLocationCoordinate2D,marker: UIImage)
    {
        self.area = area
        self.index = index
        self.marker = marker
        self.position = position
        
        
    }
    
    func geographicMidpoint(betweenCoordinates coordinates: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D {
        
        guard coordinates.count > 1 else {
            return coordinates.first ?? // return the only coordinate
                CLLocationCoordinate2D(latitude: 0, longitude: 0) // return null island if no coordinates were given
        }
        
        var x = Double(0)
        var y = Double(0)
        var z = Double(0)
        
        for coordinate in coordinates {
            let lat = coordinate.latitude * .pi / 180.0
            let lon = coordinate.longitude * .pi / 180.0
            x += cos(lat) * cos(lon)
            y += cos(lat) * sin(lon)
            z += sin(lat)
        }
        
        x /= Double(coordinates.count)
        y /= Double(coordinates.count)
        z /= Double(coordinates.count)
        
        let lon = atan2(y, x)
        let hyp = sqrt(x * x + y * y)
        let lat = atan2(z, hyp)
        
        return CLLocationCoordinate2D(latitude: lat * 180.0 / .pi, longitude: lon * 180.0 / .pi)
    }
    
}


