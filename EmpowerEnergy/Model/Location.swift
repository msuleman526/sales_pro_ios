//
//  Location.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class Location: Decodable{
    
    var latitude:Double?;
    var longitude:Double?;
    
    init(latitude: Double,longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }

}
