//
//  File.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 12/05/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class CustomerSearchData: Decodable
{
     var id: Int?
     var address_id: Int?
     var first_name: String?
     var last_name: String?
     var birthday: String?
     var gender: String?
     var home_phone: String?
     var mobile_phone: String?
     var personal_email: String?
     var work_email: String?
     var work_phone: String?
     var resident: Bool?
     var net_income: Int?
     var net_worth: Int?
     var credit_rating: Int?
     var address: InteractionAddressData?
    
}
