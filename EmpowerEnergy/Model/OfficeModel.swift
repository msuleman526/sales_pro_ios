//
//  OfficeModel.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 25/06/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation


class OfficeModel: Encodable, Decodable
{
    var created:Date?;
    var id:CLong?;
    var modified:Date?;
    var name:String?;
    var index:Int?;
    
    init(creeated: Date?,id: CLong, modified: Date, name: String,index: Int) {
        self.created = creeated
        self.id = id
        self.modified = modified
        self.name = name
        self.index = index
    }
}
