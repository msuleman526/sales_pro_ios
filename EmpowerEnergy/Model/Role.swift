//
//  Role.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/08/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation


class Role: Decodable
{
    var created_at: String?
    var description: String?
    var id: Int?
    var name: String?
    var slug: String?
    var updated_at: String?
    
    init(name: String)
    {
        self.name = name
    }
    
    init(id: Int,name: String)
    {
        self.id = id
        self.name = name
    }
}
