//
//  Address.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class Address: Decodable{
    
    var addressLine:String?
    var city: String?
    var country: String?
    var id: CLong?
    var location: Location?
    var number: String?
    var postal: String?
    var region: String?
    var street: String?
    var unit: String?
    
    init()
    {
        
    }
    
    init(id: CLong, number: String, street: String, unit: String, city: String,region: String,postal:String, country: String, location: Location, addressLine: String) {
       
        self.id = id
        self.number = number
        self.street = street
        self.unit = unit
        self.city = city
        self.region = region
        self.postal = postal
        self.country = country
        self.location = location
        self.addressLine = addressLine
        
    }
    
    init(id: CLong, number: String, street: String, unit: String, city: String,region: String,postal:String, country: String, location: Location) {
       
        self.id = id
        self.number = number
        self.street = street
        self.unit = unit
        self.city = city
        self.region = region
        self.postal = postal
        self.country = country
        self.location = location
        
    }
    
    init(location: Location) {
        self.location = location
    }
    
    func getStreetAddress() -> String {
        return "\(street!)";
    }
}
