//
//  Office.swift
//  energysolutions
//
//  Created by Salman Sherin on 18/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

struct Office:Codable{
     var created:Date?;
     var id:CLong?;
     var modified:Date?;
     var name:String?;
     var index:Int?;
    
    init(creeated: Date?,id: CLong, modified: Date, name: String,index: Int) {
        self.created = creeated
        self.id = id
        self.modified = modified
        self.name = name
        self.index = index
    }
    
}
