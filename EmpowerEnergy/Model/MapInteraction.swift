//
//  MapInteraction.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/08/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation

class MapInteraction: Decodable{
    
    var address_id: Int?
    var created_at: String?
    var current_lat: String?
    var current_long: String?
    var id: Int?
    var interaction_id: Int?
    var lat: String?
    var long: String?
    var note: String?
    var updated_at: String?
    var user_id: Int?
    
    
    
}
