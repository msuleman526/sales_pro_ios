//
//  AddressInteractionClusterItem.swift
//  EmpowerEnergy
//
//  Created by Salman Sherin on 22/04/2020.
//  Copyright © 2020 Salman Sherin. All rights reserved.
//

import Foundation
import GoogleMapsUtils

class AddressInteractionClusterItem: NSObject, GMUClusterItem{
    var addressInteraction: InteractionAddress?
    var index: Int?;
    var position: CLLocationCoordinate2D
    var name: String!
    var image: UIImage?
    
    init(addressInteraction: InteractionAddress, index: Int,position: CLLocationCoordinate2D,name: String,image: UIImage) {
        self.addressInteraction = addressInteraction
        self.index = index
        self.position = position
        self.name = name;
        self.image = image
    }
    
}
